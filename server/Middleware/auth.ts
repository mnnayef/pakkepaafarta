
const jwt= require('jsonwebtoken');
const config=require('config');

/**
 *
 * @param req
 * @param res
 * @param next
 * @function auth used to check the authentication key when server receive request from the client (user)
 */
function auth (req, res, next){
    const token = req.header('x-auth-token');
    // Check for token
    if (!token){
        console.log("No token")

        res.status(401).send({ error: 'No token, authorization denied'});
        return
    }
    try {
        // Verify token
        const decoded = jwt.verify(token, config.get('jwt.jwt_secret'));
        // Add user from payload
        req.user = decoded;
        next();
    } catch (e) {
        res.status(400).send({ error: 'Token is not valid'});
    }
}

module.exports= auth;
