DROP TABLE IF EXISTS kvittering;
DROP TABLE IF EXISTS melding;
DROP TABLE IF EXISTS chat;
DROP TABLE IF EXISTS reiserute;
DROP TABLE IF EXISTS oppdrag;
DROP TABLE IF EXISTS bruker;



CREATE TABLE IF NOT EXISTS bruker(
    bruker_id INT NOT NULL Auto_increment,
    navn VARCHAR(45) NOT NULL,
    email VARCHAR (45) NOT NULL UNIQUE,
    mobile INT NOT NULL UNIQUE ,
    hash varchar (45) NOT NULL,
    salt VARCHAR (256) Not NULL,
    vurdering DOUBLE,
    bilde LONGBLOB NULL,
    primary key (bruker_id)
)
Engine = InnoDB;


CREATE TABLE IF NOt exists oppdrag(
    oppdrag_id int not null auto_increment,
    fra_by varchar (120),
    til_by varchar (120),
    tidspunkt DATE,
    bruker_eier int,
    varebil tinyint,
    sykkel_personbil tinyint,
    beskrivelse TEXT,
    bilde LONGBLOB NULL,
    pris int,
    fra_adresse text,
    til_adresse text,
    status varchar (45),
    opdatert DateTime,
    primary key(oppdrag_id),
    foreign key(bruker_eier) references bruker(bruker_id)
    on Delete set NULL
    on Update CASCADE
)
ENGINE = InnoDB;


CREATE TABLE IF NOt exists reiserute(
    reiserute_id int not null auto_increment,
    fra_by Text,
    til_by Text,
    tidspunkt DateTime,
    bruker_pendler int,
    varebil tinyint,
    sykkel_personbil tinyint,
    pris int,
    informasjon Text,
    status varchar (45),
    stopp_by1 text,
    stopp_by2 text,
    stopp_by3 text,
    primary key(reiserute_id),
    foreign key(bruker_pendler) references bruker(bruker_id)
    on Delete set NULL
    on Update CASCADE
    )
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS chat(
    chat_id INT NOT NULL AUTO_INCREMENT,
    oppdr_id int,
    rute_id int,
    bruker_1 int,
    bruker_2 int,
    status tinyint(4),
    bruker_updated_status int(11),
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (chat_id),
    FOREIGN KEY(oppdr_id) REFERENCES oppdrag(oppdrag_id),
    FOREIGN KEY(bruker_1) REFERENCES bruker(bruker_id),
    FOREIGN KEY(bruker_2) REFERENCES bruker(bruker_id),
    FOREIGN KEY(rute_id) REFERENCES reiserute(reiserute_id)
    on Delete set NULL
    on Update CASCADE
    )
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS melding(
    mld_id INT NOT NULL AUTO_INCREMENT,
    chat_nr int,
    sender_id int,
    mld text NOT null,
    status tinyint(4),
    sendt_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (mld_id),
    FOREIGN KEY(chat_nr) REFERENCES chat(chat_id),
    FOREIGN KEY(sender_id) REFERENCES bruker(bruker_id)
    on Delete set NULL
    on Update CASCADE
    )
ENGINE = InnoDB;


CREATE TABLE IF not exists kvittering(
    kvittering_id INT NOT NULL AUTO_INCREMENT,
    bruker_pendler int,
    bruker_eier int,
    pris int,
    oppdrag_id int,
    reiserute_id int,
    tidspunkt DateTime,
    primary key(kvittering_id),
    foreign key(bruker_eier) references bruker(bruker_id),
    foreign key(bruker_pendler) references bruker(bruker_id),
    foreign key(oppdrag_id) references oppdrag(oppdrag_id),
    foreign key(reiserute_id) references reiserute(reiserute_id)
    on Delete set NULL
    on Update CASCADE
)
ENGINE = InnoDB;