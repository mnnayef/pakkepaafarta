/*
 * reiseruteDao.test.ts Tests to interact with route (reiserute).
 * @this run used in beforeAll to get connection, and to create db tables and the data
 */
import reiseruteDao from "../dao/reiseruteDao";
require('dotenv').config();

var mysql = require("mysql");
var fs = require("fs");

function run(filename, pool, done) {
    let sql =  fs.readFileSync(filename, "utf8");
    pool.getConnection((err, connection) => {
        if (err) {
            console.log(err)
            console.log("runsqlfile: error connecting");
            done();
        } else {
            console.log("runsqlfile: connected");
            connection.query(sql, (err, rows) => {
                connection.release();
                if (err) {
                    console.log(err);
                    done();
                } else {
                    console.log("runsqlfile: run ok");
                    done();
                }
            });
        }
    });
}

let poolConfig = {
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "root",
    database: "pakkepaafarta",
    debug: false,
    multipleStatements: true
};
var conPool = mysql.createPool(poolConfig);

const dao = new reiseruteDao(conPool);

beforeAll(done => {
    run("src/tests/createDB.sql", conPool, () => {
        run("src/tests/createTestData.sql", conPool, done);
    });
});

afterAll(() => {
    conPool.end();
});

test("Get all reiserute", done => {
    dao.getAllReiserute((status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    });
});

test("Get reiserute by its id", done => {
    dao.getReiserute(1, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBe(1);
        expect(data[0].pris).toBe(500);
        done();
    });
});

test("Get reiserute from to", done => {
    dao.getReiseruteFromTo("Skien","Oslo", (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(1);
        expect(data[0].stopp_by2).toBe("Moss");
        done();
    });
});

test("Add reiserute", done => {
    let reiserute = {
        reiserute_id: -1,
        fra_by: "Lillehammer",
        til_by: "Alta",
        tidspunkt: "2020-10-20 15:00:00",
        bruker_pendler: 2,
        varebil: 0,
        sykkel_personbil: 1,
        pris: 200,
        informasjon: "string",
        stopp_by1:null,
        stopp_by2 : "Oslo",
        stopp_by3 : null,

    };

    dao.addReiserute(reiserute, (status, data) => {
        expect(status).toBe(200);
        expect(data.insertId).toBeGreaterThanOrEqual(3);
        expect(data.affectedRows).toBe(1);
        done();
    });
});

test("Update reiserute", done => {
    let reiserute = {
        reiserute_id: 1,
        fra_by: "Lillehammer",
        til_by: "Alta",
        tidspunkt: "2021-12-15 15:00:00",
        bruker_pendler: 1,
        varebil: 1,
        sykkel_personbil: 0,
        pris: 5,
        informasjon: "prisen diskuteres ikke",
        stopp_by1:'Meldal',
        stopp_by2 : "Stryn",
        stopp_by3 : null,
    };
    // Actual change
    dao.updateReiserute(1, reiserute, (status, data) => {
        expect(status).toEqual(200);
        expect(data.affectedRows).toBe(1);
        expect(data.changedRows).toBe(1);
        done();
        // No change
        dao.updateReiserute(1, reiserute, (status, data) => {
            expect(status).toEqual(200);
            expect(data.affectedRows).toBe(1);
            expect(data.changedRows).toBe(0);
            done();
        });
    });
});

test("Delete reiserute", done => {
    dao.deleteReiserute(1, (status, data) => {
        expect(status).toEqual(200);
        expect(data.affectedRows).toBe(1);
        done();
    });
});
