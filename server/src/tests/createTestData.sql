INSERT INTO bruker VALUES( default,'moha', 'moha@moha.com', 47190342, 'dfghjklfghj', 'fghjkfghjkh', 8, null);
INSERT INTO bruker VALUES( default,'mahmoud', 'mahmoud@mahmoud.com', 47185000, 'qwertyuty', 'cbnmnnm', 10, null);
INSERT INTO bruker VALUES( default,'Hans Hansen', 'hans@hansen.com', 47190340, 'dfghjkfghjkfghjk', 'zxcvbnmxcvb', 10, null);

INSERT INTO oppdrag VALUES (default , 'Oslo' ,'Trondheim' ,'2020-12-14 15:00:00', 1 , 0 , 1 , 'en liten pakke', null, 120  , 'Trondheim', 'Oslo','gyldig', default );
INSERT INTO oppdrag VALUES (default , 'Oslo' ,'Bergen'    ,'2021-12-15 15:00:00', 2 , 1 , 0 , 'en stor pakke' , null , 1000, 'Oslo'     , 'Bergen','gyldig', default  );
INSERT INTO oppdrag VALUES (default , 'Skien','Drammen'   ,'2021-12-15 15:00:00', 2 , 1 , 0 , 'en sofa og et bord, bordet inneholder glass' , null , 1000 , 'Skyer' , 'Drammen','gyldig', default);
INSERT INTO oppdrag VALUES (default , 'Oslo','Kristiansand'   ,'2021-12-15 15:00:00', 2 , 1 , 0 , 'en sofa og et bord, bordet inneholder glass' , null , 1000 , 'OSLO' , 'Kristiansand','gyldig', default);

INSERT INTO reiserute VALUES (default , 'Skien', 'Oslo', '2021-12-14 15:00:00', 3, 0, 1, 500, 'kan diskutere prisen','gyldig', null , 'Moss', null );
INSERT INTO reiserute VALUES (default, 'Trondheim', 'Bergen', '2021-12-15 15:00:00', 1, 1, 0, 600, 'prisen diskuteres ikke ','gyldig', 'Meldal', 'Stryn', null );

INSERT INTO chat VALUES (default, 1 , null , 1, 2,0,0, default );
INSERT INTO chat VALUES (default, null , 2, 1, 3,0,0, default );

INSERT INTO melding VALUES (default ,1, 1, 'hei jeg kan bringe pakken din',0, default );
INSERT INTO melding VALUES (default, 1, 2, 'hei!! supert. den ligger ute foran døra',0, default);
INSERT INTO melding VALUES (default, 1, 1, 'nice da leverer jeg den i morgen',0, default);

INSERT INTO melding VALUES (default , 2, 3, 'hei jeg ønsker å flytte en sofa fra meldal til bergen, kan du gjøre det?',0, default );
INSERT INTO melding VALUES (default, 2, 1, 'hei!! ja, det kan jeg gjøre',0, default);
INSERT INTO melding VALUES (default, 2, 3, 'nice takk',0, default);

INSERT INTO kvittering VALUES (default , 1, 2, 600, 1 , null, default );
INSERT INTO kvittering VALUES (default, 3, 1, 600,  null , 1, default );
