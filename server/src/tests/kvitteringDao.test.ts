/*
 * kvitteringDao.test.ts Tests to interact with kvittering.
 * @this run used in beforeAll to get connection, and to create db tables and the data
 */
import kvitteringDao  from "../dao/kivtteringDao";
require('dotenv').config();

var mysql = require("mysql");
var fs = require("fs");

function run(filename, pool, done) {
    let sql = fs.readFileSync(filename, "utf8");
    pool.getConnection((err, connection) => {
        if (err) {
            console.log(err)
            console.log("runsqlfile: error connecting");
            done();
        } else {
            console.log("runsqlfile: connected");
            connection.query(sql, (err, rows) => {
                connection.release();
                if (err) {
                    console.log(err);
                    done();
                } else {
                    console.log("runsqlfile: run ok");
                    done();
                }
            });
        }
    });
}


let poolConfig = {
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "root",
    database: "pakkepaafarta",
    debug: false,
    multipleStatements: true
};

var conPool = mysql.createPool(poolConfig);

const dao = new kvitteringDao(conPool);

beforeAll(done => {
    run("src/tests/createDB.sql", conPool, () => {
        run("src/tests/createTestData.sql", conPool, done);
    });
});

afterAll(() => {
    conPool.end();
});

test("Get all kvittering by user id ", done => {
    dao.getAllKvitteringForBruker(1,(status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBe(2);
        done();
    });
});

test("Get kvittering by its id", done => {
    dao.getKvittering(1, (status, data) => {
        expect(status).toBe(200);
        expect(data[0].pris).toBe(600);
        done();
    });
});

test("Get kvittering by both users id", done => {
    dao.getKvitteringPendlerEier(3,1, (status, data) => {
        expect(status).toBe(200);
        expect(data[0].kvittering_id).toBeGreaterThanOrEqual(2);
        done();
    });
});

test("Add kvittering", done => {
    let kvittering = {
        kvittering_id: -1,
        bruker_pendler: 1,
        bruker_eier: 2,
        pris: 0,
        oppdrag_id:2,
        reiserute_id: null,
    };

    dao.addKvittering(kvittering, (status, data) => {
        expect(status).toBe(200);
        expect(data.affectedRows).toBe(1);
        done();
    });
});

test("Delete kvittering", done => {
    dao.deleteKvittering(2, (status, data) => {
        expect(status).toEqual(200);
        expect(data.affectedRows).toBe(1);
        done();
    });
});
