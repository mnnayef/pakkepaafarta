/*
 * oppdragDao.test.ts Tests to interact with commission (oppdrag).
 */
import oppdragDao, {oppdrag} from "../dao/oppdragDao";
require('dotenv').config();

var mysql = require("mysql");
var fs = require("fs");

function run(filename, pool, done) {
    let sql = fs.readFileSync(filename, "utf8");
    pool.getConnection((err, connection) => {
        if (err) {
            console.log(err)
            console.log("runsqlfile: error connecting");
            done();
        } else {
            console.log("runsqlfile: connected");
            connection.query(sql, (err, rows) => {
                connection.release();
                if (err) {
                    console.log(err);
                    done();
                } else {
                    console.log("runsqlfile: run ok");
                    done();
                }
            });
        }
    });
}


let poolConfig = {
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "root",
    database: "pakkepaafarta",
    debug: false,
    multipleStatements: true
};

var conPool = mysql.createPool(poolConfig);

const dao = new oppdragDao(conPool);

beforeAll(done => {
    run("src/tests/createDB.sql", conPool, () => {
        run("src/tests/createTestData.sql", conPool, done);
    });
});

afterAll(() => {
    conPool.end();
});

test("Get all oppdrag", done => {
    dao.getAllOppdrag((status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThan(2);
        done();
    });
});

test("Get oppdrag by id", done => {
    dao.getOppdrag(2, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBe(1);
        expect(data[0].fra_by).toBe("Oslo");
        done();
    });
});

test("Get oppdrag from to", done => {
    dao.getOppdragFromTo("Oslo","Bergen", (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(1);
        expect(data[0].fra_by).toBe("Oslo");
        done();
    });
});

test("Add oppdrag", done => {
    let oppdrag = {
        oppdrag_id: -1,
        fra_by: "Trondheim",
        til_by: "Kristiansand",
        tidspunkt: "2020-10-20 15:00:00",
        bruker_eier: 2,
        varebil: 0,
        sykkel_personbil: 1,
        beskrivelse: "string",
        bilde:null,
        pris: 200,
        fra_adresse : "frode rinnans vei 111",
        til_adresse : "Halvdan svartes vei 10",
        status:"gyldig"
    };

    dao.addOppdrag(oppdrag, (status, data) => {
        expect(status).toBe(200);
        expect(data.insertId).toBeGreaterThanOrEqual(5);
        expect(data.affectedRows).toBe(1);
        done();
    });
});

test("Update oppdrag", done => {
    let oppdrag = {
        oppdrag_id: -1,
        fra_by: "Trondheim",
        til_by: "Kristiansand",
        tidspunkt: "2020-10-20",
        bruker_eier: 2,
        varebil: 0,
        sykkel_personbil: 1,
        beskrivelse: "string",
        bilde:null,
        pris: 100,
        fra_adresse : "frode rinnans vei 111",
        til_adresse : "Halvdan svartes vei 10",
        status:"gyldig"
    };
    let oppdragId;
    dao.getLastOppdrag("Trondheim","Kristiansand" , (status, data) => {
        oppdragId=data[0].oppdrag_id;
        // Actual change
        dao.updateOppdrag(oppdragId, oppdrag, (status, data) => {
            expect(status).toBe(200);
            expect(data.affectedRows).toBe(1);
            expect(data.changedRows).toBe(1);
            done();
            // No change
            dao.updateOppdrag(oppdragId, oppdrag, (status, data) => {
                expect(status).toBe(200);
                expect(data.affectedRows).toBe(1);
                expect(data.changedRows).toBe(0);
                done();
            });
        });
    });

});

test("Get last oppdrag", done => {
    let oppdragId;
    dao.getLastOppdrag("Trondheim","Kristiansand", (status, data) => {
        oppdragId=data[0].oppdrag_id;
        dao.getOppdrag(oppdragId, (status, data) => {
            expect(status).toBe(200);
            expect(data[0].pris).toBe(100);
            done();
        });
    });
});


test("Delete oppdrag", done => {
    let oppdragId;
    dao.getLastOppdrag("Oslo","Kristiansand", (status, data) => {
        oppdragId=data[0].oppdrag_id;
        dao.deleteOppdrag(oppdragId, (status, data) => {
            expect(status).toBe(200);
            expect(data.affectedRows).toBe(1);
            done();
        });
    });

});
