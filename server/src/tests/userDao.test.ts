/*
 * userDao.test.ts Tests to interact with users (bruker).
 * @this run used in beforeAll to get connection, and to create db tables and the data
 */
import userDao from "../dao/brukerDao";
require('dotenv').config();

var mysql = require("mysql");
var fs = require("fs");

function run(filename, pool, done) {
  let sql = fs.readFileSync(filename, "utf8");
  pool.getConnection((err, connection) => {
    if (err) {
      console.log(err)
      console.log("runsqlfile: error connecting");
      done();
    } else {
      console.log("runsqlfile: connected");
      connection.query(sql, (err, rows) => {
        connection.release();
        if (err) {
          console.log(err);
          done();
        } else {
          console.log("runsqlfile: run ok");
          done();
        }
      });
    }
  });
}

let poolConfig = {
  connectionLimit: 1,
  host: "mysql",
  user: "root",
  password: "root",
  database: "pakkepaafarta",
  debug: false,
  multipleStatements: true
};

var conPool = mysql.createPool(poolConfig);

const dao = new userDao(conPool);

beforeAll(done => {
  run("src/tests/createDB.sql", conPool, () => {
    run("src/tests/createTestData.sql", conPool, done);
  });
});

afterAll(() => {
  conPool.end();
});

test("Get all users", done => {
   dao.getAllUsers((status, data) => {
    expect(status).toBe(200);
    expect(data.length).toBeGreaterThan(2);
    done();
  });
});

test("Get user by id", done => {
  dao.getUser(3, (status, data) => {
    expect(status).toBe(200);
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hans Hansen");
    done();
  });
});

test("Get user by email", done => {
  dao.getUserByEmail("mahmoud@mahmoud.com", (status, data) => {
    expect(status).toBe(200);
    expect(data.length).toBe(1);
    expect(data[0].mobile).toBe(47185000);
    done();
  });
});

test("Get hash of user", done => {
  dao.getHashOfUser("moha@moha.com", (status, data) => {
    let user = data[0];
    expect(status).toBe(200);
    expect(data.length).toBe(1);
    expect(user.hash).toBe("dfghjklfghj");
    expect(user.salt).toBe("fghjkfghjkh");
    done();
  });
});

test("Add user", done => {
  let bruker = {
    brukerId: -1,
    navn: "Test Testesen",
    email: "test@testesen.com",
    mobile: 1881,
    hash: "wgwrgwdgfqe",
    salt: "efnbvwwrtuj",
    vurdering:10,
    bilde: new Buffer("")
  };

  dao.addUser(bruker, (status, data) => {
    expect(status).toBe(200);
    expect(data.insertId).toBeGreaterThanOrEqual(4);
    expect(data.affectedRows).toBe(1);
    done();
  });
});

test("Update user", done => {
  let bruker = {
    brukerId: -1,
    navn: "Test Testesen Nyt navn",
    email: "testUpdated@testesen.com",
    mobile: 1881,
    hash: "wgwrgwdgfqe",
    salt: "efnbvwwrtuj",
    vurdering:10,
    bilde: new Buffer("")
  };
  let brukerId;
  dao.getUserByEmail("test@testesen.com", (status, data) => {
    brukerId=data[0].bruker_id;
    // Actual change
    dao.updateUser(brukerId, bruker, (status, data) => {
      expect(status).toBe(200);
      expect(data.affectedRows).toBe(1);
      expect(data.changedRows).toBe(1);
      done();
      // No change
      dao.updateUser(brukerId, bruker, (status, data) => {
        expect(status).toBe(200);
        expect(data.affectedRows).toBe(1);
        expect(data.changedRows).toBe(0);
        done();
      });
    });
  });

});

test("Get updated user", done => {
  let brukerId;
  dao.getUserByEmail("moha@moha.com", (status, data) => {
    brukerId=data[0].bruker_id;
    dao.getUser(brukerId, (status, data) => {
      expect(status).toBe(200);
      expect(data[0].navn).toBe("moha");
      done();
    });
  });

});

test("Reset password of user", done => {
  let bruker = {
    brukerId: null,
    navn: null,
    email: null,
    mobile: null,
    hash: "wgwrgwdgfqe",
    salt: "efnbvwwrtuj",
    vurdering:null,
    bilde:null
  };
  dao.resetPassword("testUpdated@testesen.com", bruker, (status, data) => {
    expect(status).toBe(200);
    expect(data.affectedRows).toBe(1);
    done();
  });
});

test("Delete user", done => {
  let brukerId;
  dao.getUserByEmail("testUpdated@testesen.com", (status, data) => {
    brukerId=data[0].bruker_id;
    dao.deleteUser(brukerId, (status, data) => {
      expect(status).toBe(200);
      expect(data.affectedRows).toBe(1);
      done();
    });
  });
});
