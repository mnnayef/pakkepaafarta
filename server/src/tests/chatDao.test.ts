
import chatDao  from "../dao/chatDao";
require('dotenv').config();

var mysql = require("mysql");
var fs = require("fs");

/*
 * chatDao.test.ts Tests to interact with chat.
 * @function run used in beforeAll to get connection, and to create db tables and the data
 */
function run(filename, pool, done) {
    let sql = fs.readFileSync(filename, "utf8");
    pool.getConnection((err, connection) => {
        if (err) {
            console.log(err)
            console.log("runsqlfile: error connecting");
            done();
        } else {
            console.log("runsqlfile: connected");
            connection.query(sql, (err, rows) => {
                connection.release();
                if (err) {
                    console.log(err);
                    done();
                } else {
                    console.log("runsqlfile: run ok");
                    done();
                }
            });
        }
    });
}


let poolConfig = {
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "root",
    database: "pakkepaafarta",
    debug: false,
    multipleStatements: true
};

var conPool = mysql.createPool(poolConfig);

const dao = new chatDao(conPool);

beforeAll(done => {
    run("src/tests/createDB.sql", conPool, () => {
        run("src/tests/createTestData.sql", conPool, done);
    });
});

afterAll(() => {
    conPool.end();
});

test("Get all chats", done => {
    dao.getAllChat((status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    });
});

test("Get chat by its id", done => {
    dao.getAllChatByUser(1, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    });
});

test("Get chat by oppdrag and user id", done => {
    dao.getChatForBruker(1,2, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    });
});

test("Get getSpecificChatOppdrag", done => {
    dao.getSpecificChatOppdrag(1,null,1,2, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    });
});

test("Get chat by reiserute id", done => {
    dao.getChatByReiserute(2, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    });
});

test("Add chat and get maxId", done => {
    let chat = {
        chat_id: -1,
        oppdr_id: 3,
        rute_id: null,
        bruker_1: 2,
        bruker_2: 1,
        status: 0,
        bruker_updated_status: 0,
    };

    dao.addChat(chat, (status, data) => {
        expect(status).toBe(200);
        expect(data.affectedRows).toBe(1);
        done();
    });
});

test("Get max chat id", done => {
    dao.getMaxChatId((status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    });
});

test("Delete chat", done => {
    dao.deleteChat(3,2,1, (status, data) => {
        expect(status).toBe(200);
        expect(data.affectedRows).toBe(1);
        done();
    });
});

