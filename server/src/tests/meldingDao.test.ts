/*
 * meldingDao.test.ts Tests to interact with message (melding).
 */
import meldingDao  from "../dao/meldingDao";
require('dotenv').config();

var mysql = require("mysql");
var fs = require("fs");

function run(filename, pool, done) {
    let sql = fs.readFileSync(filename, "utf8");
    pool.getConnection((err, connection) => {
        if (err) {
            console.log(err)
            console.log("runsqlfile: error connecting");
            done();
        } else {
            console.log("runsqlfile: connected");
            connection.query(sql, (err, rows) => {
                connection.release();
                if (err) {
                    console.log(err);
                    done();
                } else {
                    console.log("runsqlfile: run ok");
                    done();
                }
            });
        }
    });
}


let poolConfig = {
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "root",
    database: "pakkepaafarta",
    debug: false,
    multipleStatements: true
};

var conPool = mysql.createPool(poolConfig);

const dao = new meldingDao(conPool);

beforeAll(done => {
    run("src/tests/createDB.sql", conPool, () => {
        run("src/tests/createTestData.sql", conPool, done);
    });
});

afterAll(() => {
    conPool.end();
});

test("Add melding to chat", done => {
    let melding = {
        mld_id: -1,
        chat_nr: 1,
        sender_id:2,
        mld: "Hei fra test meldingDao",
    };

    dao.addMelding(melding, (status, data) => {
        expect(status).toBe(200);
        expect(data.affectedRows).toBe(1);
        done();
    });
});

test("Get all melding by chat id", done => {
    dao.getMeldingByChatId(1,(status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(3);
        done();
    });
});

test("Get all melding by user id", done => {
    dao.getAllMeldinger(1, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBeGreaterThanOrEqual(3);
        done();
    });
});

test("Get all melding by user and chat id", done => {
    dao.getMeldingByUser(1,1, (status, data) => {
        expect(status).toBe(200);
        expect(data.length).toBe(2);
        done();
    });
});

test("Get max melding id", done => {
    dao.getMaxMelding((status, data) => {
        expect(status).toBe(200);
        expect(data.length).toEqual(1);
        done();
    });
});

test("Delete melding", done => {
    dao.deleteMelding(2, (status, data) => {
        expect(status).toBe(200);
        expect(data.affectedRows).toEqual(1);
        done();
    });
});

