/*
 * contains all routes to interact with users
 * such as sign up, login, get information, delete account...
 * It connects between brukerDao and brukerService
 */
import express from "express";
require('dotenv').config();
import   brukerDao ,{clearSensitiveData } from "../dao/brukerDao";
import { pool } from "../dao/database";
const auth=require('../../Middleware/auth');
const config=require('config');
const jwt = require('jsonwebtoken');
import { compareHash, hash } from "../hashing";


const router = express.Router();
const dao = new brukerDao(pool);
let bruker;

//get all users
router.get("/bruker/" , (request, response) => {
    dao.getAllUsers((status, data) => {
        status == 500 ? response.status(500) : response.send(clearSensitiveData(data));
    });
});

// get user to login
router.post("/bruker/login", (req, res) => {
    dao.getHashOfUser(req.body.email, (status, data) => {
        bruker = data[0];
        if (typeof bruker != "undefined") {
            if (compareHash(bruker.hash, req.body.password, bruker.salt)) {
                let token = jwt.sign(
                    { id: bruker.bruker_id },
                    config.get('jwt.jwt_secret'), {
                    expiresIn: config.get('jwt.expires_in')  // a month
                });
                res.json({ jwt: token });
                res.status(200);
            } else {
                res.status(204);
                res.json({ error: "Not authorized" });
            }
        } else {
            res.status(204);
            res.json({ error: "brukeren finnes ikke" });
        }
    });
});

//add a new user
router.post("/bruker/register", (req, res) => {
    let data = hash(req.body.password);
    req.body.hash = data.hash;
    req.body.salt = data.salt;
    dao.getUserByEmail(req.body.email, (status, data) => {
        bruker = data[0];
        if (typeof bruker === "undefined") {
            dao.addUser(req.body, (status, data) => {
                if(status!==500){
                    if (status !== 401) {
                        let token = jwt.sign(
                            { id: req.body.bruker_id },
                            config.get('jwt.jwt_secret'), {
                                expiresIn: config.get('jwt.expires_in') //a month
                            });

                        res.json({ jwt: token});
                    } else {
                        res.status(401).send({ error: "Could not add user" });
                    }
                }else{
                    res.status(500).send({error: "This phone number already has a user."})
                }
            });
        } else {
            res.status(409).send({ error: "boo:(" });
        }
    });
});

// Get a single user given its id
router.get("/bruker/:id", async (request, response) => {
    console.log(parseInt(request.params.id))
    dao.getUser(parseInt(request.params.id), (status, data) => {
        status == 500 ? response.status(500) : response.send(clearSensitiveData(data));
    });
});

// Get user info without image
router.get("/bruker/no-image/:id", async (request, response) => {
    dao.getUserNoImage(parseInt(request.params.id), (status, data) => {
        status == 500 ? response.status(500) : response.send(clearSensitiveData(data));
    });
});

//Get user by email
router.get("/bruker/:email", async (request, response) => {
    dao.getUserByEMail(request.params.email, (status, data) => {
        let user=data[0];
        if (typeof user != "undefined") {
            response.status(200);
            response.send(clearSensitiveData(data))
        }else{
            response.status(401);
            response.json({ error: "brukeren finnes ikke" });
        }
    });
});

// Update singular user given id
router.put("/bruker/:id", auth, async (request, response) => {
    dao.getUserByEmail(request.body.email, (status, data) => {
        let user = data[0];
        if(typeof user === "undefined" || user.bruker_id == request.params.id){
            dao.updateUser(parseInt(request.params.id), request.body, data => {
                response.status(200);
                response.send(clearSensitiveData(data));
            });
        }else if(user.brukerId == request.params.id){
            dao.updateUser(parseInt(request.params.id), request.body, (status, data) => {
                response.status(200);
                response.send(clearSensitiveData(data));
            });
        }else{
            response.sendStatus(409);
        }
    });
});

router.get("/bruker/email/info/:email", async (request, response) => {
    dao.getUserByEmail(request.params.email, (status, data) => {
        let user=data[0];
        if (typeof user != "undefined") {
            response.status(200);
            response.send(clearSensitiveData(data))
        }else{
            response.json({ error: "brukeren finnes ikke" });
        }
    });
});

//Change password
router.put("/bruker/change_password/:email", auth, async (request, response) => {
    dao.changePassword(request.params.email, request.body, (status, data) => {
        status == 500 ? response.status(500) : response.send(clearSensitiveData(data));
    });
});

//Reset password
router.put("/bruker/reset_password/:email",  async (request, response) => {
    dao.resetPassword(request.params.email, request.body, (status, data) => {
        status == 500 ? response.status(500) : response.send(clearSensitiveData(data));
    });
});

// Change picture
router.put("/bruker/change_picture/:id", auth, async (request, response) => {
    dao.changePicture(parseInt(request.params.id), request.body, (status, data) => {
        status == 500 ? response.status(500) : response.send(clearSensitiveData(data));
    });
});

// Delete user given id
router.delete("/bruker/:id", auth, async (request, response) => {
    dao.deleteUser(parseInt(request.params.id), (status, data) => {
        status == 500 ? response.status(500) : response.send(clearSensitiveData(data));
    });
});

module.exports = router;