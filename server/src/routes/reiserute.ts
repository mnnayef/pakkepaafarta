/*
 * Routes to interact with reiserute (route)
 * such as create, get, update...
 */

import express from "express";
require('dotenv').config();
import   reiseruteDao from "../dao/reiseruteDao";
import { pool } from "../dao/database";
const auth=require('../../Middleware/auth');
const config=require('config');
const router = express.Router();
const dao = new reiseruteDao(pool);


//Get route fra til
router.get("/reiserute/:fra/:til" , (request, response) => {

    dao.getReiseruteFromTo(request.params.fra,request.params.til ,(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

// Get a singular route given its id
router.get("/reiserute/:id" , (request, response) => {
    dao.getReiserute(parseInt(request.params.id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

router.get("/bruker/reiserute/:bruker_pendler" , (request, response) => {
    dao.getReiseruteForUser(parseInt(request.params.bruker_pendler),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Update route
router.put("/reiserute/update/", auth, (req, response)=>{
    dao.updateReiserute(parseInt(req.body.reiserute_id), req.body, (status, data)=>{
        response.status(200);
        response.send(data);
   })
});

// Update the status of a singular route
router.put("/reiserute/update/:id/:status", auth, (request, response)=>{
    if(request.params.status=="gyldig"||request.params.status=="ugyldig"){
        dao.updateStatus(parseInt(request.params.id), request.params.status, (status, data)=>{
            response.status(200);
            response.send(data);
        })
    }else  response.status(410).send({ error: "status er ikke definert"});
});

//Delete route
router.delete("/reiserute/:id", auth, async (request, response) => {
    dao.deleteReiserute(parseInt(request.params.id), (status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Add route
router.post("/reiserute/", auth, (req, res) => {
    dao.addReiserute(req.body, (status, data) => {
        if(status!==500){
            if (status !== 401) {
                    res.status(200);
                    res.send(data);
            } else {
                res.status(401);
                res.json({ error: "Kunne ikke legge til reiserute, brukeren er ikke autentisert" });
            }
        }else{
            res.status(500)
            res.json({error: "Kunne ikke legge til reiserute"})
        }
    });
});

module.exports = router;