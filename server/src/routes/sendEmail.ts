/*
 * Routes to interact with sendMail to send reset password for user
 */
import express from "express";
const router = express.Router();

var path       = require("path");
var bodyParser = require("body-parser");
var nodemailer = require("nodemailer");
var email      = process.env.gmail_email.toString();
var password   = process.env.gmail_password.toString();
var subject    = "Genrert passord fra Pakke på farta"

router.use(bodyParser.json());

var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: email,
        pass: password
    }
});

// Send the mail with a generated password
router.post("/send_email/", (req, res) => {
    var emailinfo = {
        to: req.body.email,
        from: email,
        subject: subject,
        text: req.body.message
    };
    smtpTransport.sendMail(emailinfo, function(err) {
        if (!err) {
            console.log("!err i sendmail.ts", emailinfo)
            return res.json({
                message: "Passordet er nå sendt vennligst sjekk e-posten din"
            });
        } else {
            console.log(res.json(err))
            return res.json(err);
        }
    });
});

module.exports = router;
