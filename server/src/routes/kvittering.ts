/*
 * Routes to interact with kvittering
 * such as create, get, update...
 */

import express, {response} from "express";
require('dotenv').config();
import   kvitteringDao from "../dao/kivtteringDao";
import { pool } from "../dao/database";
const auth=require('../../Middleware/auth');
const config=require('config');
const router = express.Router();
const dao = new kvitteringDao(pool);


//Get all kvittering
router.get("/kvittering/:id" , auth, (request, response) => {
    dao.getKvittering(parseInt(request.params.id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get a singular kvittering for user given its id
router.get("/kvittering/bruker/:bruker" , (request, response) => {
    dao.getAllKvitteringForBruker(parseInt(request.params.bruker),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get all kvittering between two users
router.get("/kvittering/:bruker_eier/:bruker_pendler" , (request, response) => {
    dao.getKvitteringPendlerEier(parseInt(request.params.bruker_eier),parseInt(request.params.bruker_pendler),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//update kvittering
router.put("/kvittering/", auth, (request, response)=>{
    dao.updateKvittering(request.body.kvittering_id, request.body, (status, data)=>{
        response.status(200);
        response.send(data);
    })
});

//Delete kvittering
router.delete("/kvittering/:id", auth, async (request, response) => {
    dao.deleteKvittering(parseInt(request.params.id), (status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Create kvittering
router.post("/kvittering/", auth, (req, res) => {
    dao.addKvittering(req.body, (status, data) => {
        if(status!==500){
            if (status !== 401) {
                res.status(200);
                res.send(data);
            } else {
                res.status(401);
                res.json({ error: "Kunne ikke legge til kvittering, brukeren er ikke autentisert" });
            }
        }else{
            res.status(500)
            res.json({error: "Kunne ikke legge til kvittering"})
        }
    });
});

module.exports = router;


