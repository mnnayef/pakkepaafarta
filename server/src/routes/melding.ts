/*
 * Routes to interact with message (melding)
 * such as create, get, update...
 */

const auth=require('../../Middleware/auth');
require('dotenv').config();
import   meldingDao from "../dao/meldingDao";
import { pool } from "../dao/database";
import express from "express"
const config=require('config');
const router = express.Router();
const dao = new meldingDao(pool);

//Get max message id
router.get("/melding/" , auth,(request, response) => {
    dao.getMaxMelding((status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get all message given message id
router.get("/melding/:chat_nr" , auth,(request, response) => {
    dao.getMeldingByChatId(parseInt(request.params.chat_nr),(status, data) => {
        status == 500 ? response.status(500) : response.send(data).setMaxListeners(10);
    });
});

// Add melding
router.post("/melding/", auth, (req, res) => {
    dao.addMelding(req.body, (status, data) => {
        if(status!==500){
            if (status !== 401) {
                res.status(200);
                res.send(data);
            } else {
                res.status(401);
                res.json({ error: "Kunne ikke legge til message, brukeren er ikke autentisert" });
            }
        }else{
            res.status(500)
            res.json({error: "500: Kunne ikke legge til message"})
        }
    });
});

// Update status to a message ( has been seen)
router.put("/chat/:melding_id/:status", auth, (req, res) => {
    dao.updateStatus(parseInt(req.params.melding), parseInt(req.params.status),(status, data) => {
    });
});

// Delete a message given its id
router.delete("/melding/:mld_id" , auth,(request, response) => {
    dao.deleteMelding(parseInt(request.params.mld_id),(status, data) => {
            status == 500 ? response.status(500) : response.send(data);
        });
});

module.exports = router;
