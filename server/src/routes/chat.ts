/*
 * contains all routes to interact with chat
 * such as create, get...
 * It connects between brukerDao and brukerService
 */
const auth=require('../../Middleware/auth');
require('dotenv').config();
import   chatDao from "../dao/chatDao";
import { pool } from "../dao/database";
import express from "express"

const config=require('config');

const router = express.Router();
const dao = new chatDao(pool);

//Retunerer alle chat
router.get("/chat/" , auth,(request, response) => {
    dao.getAllChat((status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

router.get("/chat/samtale/:chat_id" , auth,(request, response) => {
    dao.getChat(parseInt(request.params.chat_id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});


//Get all chats given user id
router.get("/chat/:brukerID" , auth,(request, response) => {
    dao.getAllChatByUser(parseInt(request.params.brukerID),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get a singular chat given oppdrag id and user id
router.get("/chat/:oppdrag_id/:bruker_id" , auth,(request, response) => {
    dao.getChatForBruker(parseInt(request.params.oppdrag_id),parseInt(request.params.bruker_id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get a singular chat given its  ( oppdrag id, bruker1 id and bruker2 id )
router.get("/chat/specific/:oppdrag_id/:reiseute_id/:bruker_1/:bruker_2" , auth,(request, response) => {
    dao.getSpecificChatOppdrag(parseInt(request.params.oppdrag_id), parseInt(request.params.reiseute_id),
        parseInt(request.params.bruker_1), parseInt(request.params.bruker_2),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get all chat given oppdrag id
router.get("/chat/oppdrags/all/:oppdrag_id" ,auth, (request, response) => {
    dao.getChatByOppdragId(parseInt(request.params.oppdrag_id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get all chat given oppdrag id
router.get("/chat/reiserute/all/:rute_id" ,auth, (request, response) => {
    dao.getChatByReiserute(parseInt(request.params.rute_id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});
//Get max chat id
router.get("/maxchatId" , auth,(request, response) => {
    dao.getMaxChatId((status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

router.get("/chat/unread/messages/:bruker_id" , auth,(request, response) => {
    dao.getUnreadMessages(parseInt(request.params.bruker_id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

// Create chat
router.post("/chat/", auth, (req, res) => {
    dao.addChat(req.body, (status, data) => {
        if(status!==500){
            if (status !== 401) {
                res.status(200);
                res.send(data);
            } else {
                res.status(401);
                res.json({ error: "Kunne ikke legge til chat, brukeren er ikke autentisert" });
            }
        }else{
            res.status(500)
            res.json({error: "500: Kunne ikke legge til chat"})
        }

    });
});

router.put("/chat/:chat_id/:bruker/:status", auth, (req, res) => {
    dao.updateStatus(parseInt(req.params.chat_id),  parseInt(req.params.bruker), parseInt(req.params.status),(status, data) => {
    });
});

// Delete specific chat given its ( oppdrag id, bruker1 id and bruker2 id )
router.delete("/chat/:oppdrag_id/:bruker_1/:bruker_2" , auth,(request, response) => {
    dao.deleteChat(parseInt(request.params.oppdrag_id),parseInt(request.params.bruker_1),
        parseInt(request.params.bruker_2),(status, data) => {
            status == 500 ? response.status(500) : response.send(data);
        });
});

module.exports = router;
