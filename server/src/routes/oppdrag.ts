/*
 * Routes to interact with commission (oppdrag)
 * such as create, get, update...
 */

import express from "express";
require('dotenv').config();
import   oppdragDao from "../dao/oppdragDao";
import { pool } from "../dao/database";
const auth=require('../../Middleware/auth');
const config=require('config');
const router = express.Router();
const dao = new oppdragDao(pool);

//Get all commission
router.get("/oppdrag" , (request, response) => {
    dao.getAllOppdrag((status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});
//Get all commissions to list on map
router.get("/oppdrag/oppdragOnMap" , (request, response) => {
    dao.listOppdragOnMap((status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get a singular commission given oppdrag id
router.get("/oppdrag/:id" , (request, response) => {
    dao.getOppdrag(parseInt(request.params.id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Get a singular commission given user id
router.get("/oppdrag/bruker/:id" , (request, response) => {
    dao.getUserOppdrag(parseInt(request.params.id),(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

// Get a singular commission given pick up and destination address
router.get("/oppdrag/:fra/:til" , (request, response) => {
    dao.getOppdragFromTo(request.params.fra,request.params.til ,(status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Update commission given its id
router.put("/oppdrag/update/:id", auth, (request, response)=>{
    dao.updateOppdrag(parseInt(request.params.id), request.body, (status, data)=>{
        response.status(200);
        response.send(data);
    })
});

// Update status to a singular commission given its id
router.put("/oppdrag/update/:id/:status", auth, (request, response)=>{
    if(request.params.status==="gyldig"||request.params.status==="ugyldig"){
        dao.updateStatus(parseInt(request.params.id), request.params.status, (status, data)=>{
            response.status(200);
            response.send(data);
        })
    }else{
        response.status(410).send({ error: "status er ikke definert"});
    }
});

//Delete commission
router.delete("/oppdrag/:id", auth, async (request, response) => {
    dao.deleteOppdrag(parseInt(request.params.id), (status, data) => {
        status == 500 ? response.status(500) : response.send(data);
    });
});

//Add commission
router.post("/oppdrag/", auth, (req, res) => {
    dao.addOppdrag(req.body, (status, data) => {
        if(status!==500){
            if (status !== 401) {
                res.status(200);
                res.send(data);
            } else {
                res.status(401);
                res.json({ error: "Kunne ikke legge til oppdrag, brukeren er ikke autentisert" });
            }
        }else{
            res.status(500)
            res.json({error: "Kunne ikke legge til oppdrag"})
        }

    });
});

module.exports = router;
