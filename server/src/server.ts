/*
 * @this server is the main root of our server-side,
 * and it contains all routes used to communicate with the client-side
 */
const express = require('express');
const server = express();
const cors = require('cors');
const body_parser = require('body-parser');


//server.use(body_parser.json());
//server.use(body_parser.urlencoded({extended: false}));
server.use(body_parser.json({ limit: '50mb' }));
server.use(cors());
server.use(function(req, res, next) {
 res.header('Access-Control-Allow-Origin', 'http://localhost:9000');
 res.header(
     'Access-Control-Allow-Headers',
     'Origin, X-Requested-With, Content-Type, Accept'
 );
 next();
});


server.use('/v0', require('./routes/bruker'));
server.use('/v0', require('./routes/reiserute'));
server.use('/v0', require('./routes/oppdrag'));
server.use('/v0', require('./routes/kvittering'));
server.use('/v0', require('./routes/sendEmail'));
server.use('/v0', require('./routes/chat'));
server.use('/v0', require('./routes/melding'));




 const port = 9000;

server.listen(port, () => console.log('Server started on port:', port));

