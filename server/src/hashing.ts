

const sjcl = require('sjcl')

/**
 *
 * @param data
 * @function hash used to create a hash and salt of a password for users,
 * sjcl is the stanford Javascript Crypto Library
 */
export function hash(data: string) {
    var salt = sjcl.random.randomWords(32);
    var saltstring = sjcl.codec.base64.fromBits(salt);
    return {
        hash: sjcl.codec.base64.fromBits(sjcl.hash.sha256.hash(data + saltstring)),
        salt: sjcl.codec.base64.fromBits(salt)
    };
}

/**
 * @this compareHash compareHash used to check password when users login
 * @param hash of password for users
 * @param data the password
 * @param salt of password for users
 */
export function compareHash(hash: string, data: string, salt: string) {
    var newHash = sjcl.codec.base64.fromBits(sjcl.hash.sha256.hash(data + salt));
    return hash == newHash;
}