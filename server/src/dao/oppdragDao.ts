
const daoParentOppdrag = require("./dao");

export interface oppdrag {
    oppdrag_id: number;
    fra_by: string;
    til_by: string;
    tidspunkt: string;
    bruker_eier: number;
    varebil: number;
    sykkel_personbil: number;
    beskrivelse: string;
    fra_adresse: string;
    til_adresse: string;
    pris: number;
    bilde: Buffer;
}

/**
 * @daoClass oppdragDao is used to do everything has to do
 * with oppdrag(commission) in db such get,add,update, delete...
 */
export default class oppdragDao extends daoParentOppdrag {
    constructor(pool) {
        super(pool);
    }


    getAllOppdrag(callback) {
        super.query(
            "SELECT oppdrag_id, fra_by, til_by, tidspunkt, bruker_eier, vareBil, sykkel_personbil, beskrivelse, fra_adresse, til_adresse, status, bilde, pris FROM oppdrag",
            [],
            callback
        );
    }
    /**
     * @function listOppdragOnMap() list all commissions without unnecessary data such pictures, used to render them on mapScreen
     * @param callback response depends on data stored in the database
     */
    listOppdragOnMap(callback) {
        super.query(
            "SELECT oppdrag_id, fra_by, til_by,  bruker_eier,  fra_adresse, til_adresse, status,  pris FROM oppdrag",
            [],
            callback
        );
    }
    /**
     * @function getOppdrag()
     * @param oppdrag_id Identifies the commission of interest in database
     * @param callback results from database
     */
    getOppdrag(oppdrag_id:number ,callback){
        super.query(
            "Select oppdrag_id, fra_by, til_by, tidspunkt, bruker_eier, vareBil, sykkel_personbil, beskrivelse, fra_adresse, til_adresse, status, pris, bilde FROM oppdrag Where oppdrag_id=?",
            [oppdrag_id],
            callback
        );
    }
    /**
     *
     * @param bruker_eier Identifies the user of interest in database
     * @param callback
     * @return all commissions that exists, null otherwise
     */
    getUserOppdrag(bruker_eier:number , callback){
        super.query(
            "Select oppdrag_id, fra_by, til_by, tidspunkt, bruker_eier, vareBil, sykkel_personbil, beskrivelse, fra_adresse, til_adresse, status, pris, bilde FROM  oppdrag Where bruker_eier = ? ORDER BY oppdrag_id DESC ",
            [bruker_eier],
            callback
        );
    }
    /**
     * @function getOppdragFromTo() gets a single commission based on,
     * @param from the pick up address
     * @param to the destination
     * @param callback
     */
    getOppdragFromTo(from: string, to: string, callback) {
        super.query(
            "SELECT oppdrag_id, fra_by, til_by, tidspunkt, bruker_eier, varebil, sykkel_personbil, beskrivelse, fra_adresse, til_adresse, status,pris, bilde FROM oppdrag " +
            "Where fra_by=? And til_by=? And (tidspunkt >= CURDATE())",
            [from, to],
            callback
        );
    }
    getLastOppdrag(from: string, to: string, callback){
        super.query(
            "SELECT MAX(oppdrag_id) as oppdrag_id FROM oppdrag Where fra_by=? And til_by=? ",
            [from, to],
            callback
        );
    }
    addOppdrag(data: oppdrag, callback) {

        super.query(
            "INSERT INTO oppdrag VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, default )",
            [
                data.fra_by,
                data.til_by,
                data.tidspunkt,
                data.bruker_eier,
                data.varebil,
                data.sykkel_personbil,
                data.beskrivelse,
                data.bilde,
                data.pris,
                data.fra_adresse,
                data.til_adresse,
                "gyldig"
            ],
            callback
        );
    }

    updateOppdrag(oppdrag_id: number, data: oppdrag, callback) {
        let oppdrag;
        this.getOppdrag(oppdrag_id, (status, oldData) => {
            oppdrag = oldData[0];

            if (typeof data.fra_by == "undefined") {
                data.fra_by = oppdrag.fra_by;
            }
            if (typeof data.til_by == "undefined") {
                data.til_by = oppdrag.til_by;
            }
            if (typeof data.tidspunkt == "undefined") {
                data.tidspunkt = oppdrag.tidspunkt;
            }
            if (typeof data.sykkel_personbil == "undefined") {
                data.sykkel_personbil = oppdrag.sykkel_personbil;
            }
            if (typeof data.beskrivelse == "undefined") {
                data.beskrivelse = oppdrag.beskrivelse;
            }
            if (typeof data.varebil == "undefined") {
                data.varebil = oppdrag.varebil;
            }
            if (typeof data.bilde == "undefined") {
                data.bilde = oppdrag.bilde;
            }
            if (typeof data.pris == "undefined") {
                data.pris = oppdrag.pris;
            }
            if (typeof data.fra_adresse == "undefined") {
                data.fra_adresse = oppdrag.fra_adresse;
            }
            if (typeof data.til_adresse == "undefined") {
                data.til_adresse = oppdrag.til_adresse;
            }
            super.query(
                "UPDATE oppdrag SET fra_by = ?, til_by = ?, tidspunkt = ?, varebil = ?, sykkel_personbil=? , beskrivelse=?, bilde=?, pris=?, fra_adresse=?, til_adresse=?, status=? WHERE oppdrag_id = ?",
                [data.fra_by, data.til_by, data.tidspunkt, data.varebil, data.sykkel_personbil, data.beskrivelse, data.bilde, data.pris, data.fra_adresse, data.til_adresse, "gyldig", oppdrag_id],
                callback
            );
        });
    }
    /**
     * @function updateStatus() update the commission such as invalid when it is agreed to be picked up
     * @param oppdrag_id Identifies the commission of interest in database
     * @param status of the given commission
     * @param callback
     */
    updateStatus(oppdrag_id: number, status:string, callback){
        super.query("update oppdrag set status=? Where oppdrag_id=?",[status,oppdrag_id], callback)
    }

    deleteOppdrag(oppdrag_id: number, callback) {
        super.query("DELETE FROM oppdrag WHERE oppdrag_id=?", [oppdrag_id], callback);
    }
}
