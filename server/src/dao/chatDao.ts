
const daoParentChat = require("./dao");

export interface Chat {
    chat_id: number
    bruker_1: number;
    bruker_2: number;
    status: number;
    bruker_updated_status: number;
    oppdr_id: number;
    rute_id: number
}

/**
 * @daoClass chatDao is used to do everything has to do
 * with chat functionality in database such create a new chat between two users,
 * list all chats for user...
 */
export default class chatDao extends daoParentChat {
    constructor(pool) {
        super(pool);
    }
    /**
     * @function getAllChat() is used to gets all chat from database
     * @param callback returns response
     * @return list of chat if exists, null otherwise
     */
    getAllChat(callback) {
        super.query(
            "Select  chat_id,oppdr_id, rute_id ,bruker_1, bruker_2, status, bruker_updated_status,created_at FROM chat ORDER BY created_at DESC",
            [],
            callback
        );
    }
    /**
     * @function getUser() is used to get specific user given its id
     * @param brukerId Identifies the user of interest in database
     * @param callback
     * @return user if exist,null otherwise
     */
    getChat(chatId:number,callback) {
        super.query(
            "Select  chat_id,oppdr_id, rute_id ,bruker_1, bruker_2, status, bruker_updated_status,created_at FROM chat where chat_id=? ORDER BY created_at DESC",
            [chatId],
            callback
        );
    }
    /**
     * @function getAllChatByUser() used to in myChat screen to a user
     * @param bruker_id Identifies the user of interest in database
     * @param callback return data if exist, null otherwise
     */
    getAllChatByUser(bruker_id:number, callback) {
        super.query(
            "Select chat_id,oppdr_id, rute_id ,bruker_1, bruker_2, status, bruker_updated_status,created_at FROM chat Where bruker_1=? OR bruker_2=? ORDER BY created_at DESC",
            [bruker_id,bruker_id],
            callback
        );
    }
    /**
     * @function() getChatForBruker() used to get a specific chat connected to a given oppdrag for a user,
     * @param oppdr_id Identifies the oppdrag of interest in database
     * @param bruker_id Identifies the user of interest in database
     * @param callback
     */
    getChatForBruker(oppdr_id:number,bruker_id:number,  callback) {
        super.query(
            "Select chat_id,oppdr_id, rute_id , bruker_1, bruker_2, status, bruker_updated_status,created_at FROM chat Where oppdr_id=? AND (bruker_1=? or bruker_2=?) ORDER BY created_at DESC",
            [oppdr_id,bruker_id,bruker_id],
            callback
        );
    }
    /**
     * @function getSpecificChatOppdrag() used to render chatBox for the users
     * @param oppdr_id Identifies the oppdrag that is connected to the chat in database
     * @param reiserute_id Identifies the route that is connected to the chat in database
     * @param bruker_1 Identifies the 1. user of interest in database
     * @param bruker_2 Identifies the 2. user of interest in database
     * @param callback
     */
    getSpecificChatOppdrag(oppdr_id:number, reiserute_id: number,bruker_1:number, bruker_2: number, callback) {
        super.query(
            "Select chat_id,oppdr_id,  rute_id ,bruker_1, bruker_2, status, bruker_updated_status,created_at FROM chat Where  (oppdr_id=? OR rute_id= ?) "+
            "and ((bruker_1=? and bruker_2=?) or (bruker_1=? and bruker_2=?) ) ORDER BY created_at DESC",
            [oppdr_id, reiserute_id, bruker_1, bruker_2, bruker_2, bruker_1],
            callback
        );
    }

    getChatByOppdragId(oppdr_id:number, callback) {
        super.query(
            "Select  chat_id,oppdr_id , rute_id ,bruker_1, bruker_2, status, bruker_updated_status,created_at FROM chat Where oppdr_id=? ORDER BY created_at DESC",
            [oppdr_id],
            callback
        );
    }

    getChatByReiserute(rute_id:number, callback) {
        super.query(
            "Select  chat_id,rute_id , rute_id ,bruker_1, bruker_2, status, bruker_updated_status,created_at FROM chat Where rute_id=? ORDER BY created_at DESC",
            [rute_id],
            callback
        );
    }
    /**
     * @function getUnreadMessages() used to get chats that contain Unread messages,
     * twhich allows us to notify the user about them
     * @param bruker_id Identifies the user of interest in database
     * @param callback
     */
    getUnreadMessages(bruker_id, callback) {
        super.query(
            "Select  chat_id FROM chat Where (bruker_1=? OR bruker_2=?) And status=0 And bruker_updated_status<>?",
            [bruker_id, bruker_id,bruker_id],
            callback
        );
    }

    /**
     * @function getMaxChatId used to navigate to the newest created message
     * @param callback
     */
    getMaxChatId(callback) {
        super.query(
            "Select max(chat_id) as chat_id FROM chat",
            [],
            callback
        );
    }

    /**
     * @function updateStatus() used to set message as readied
     * @param chat_id Identifies the chat of interest in database
     * @param bruker_updated_status Identifies the user that reading the message
     * @param status
     * @param callback
     */
    updateStatus(chat_id:number,bruker_updated_status:number, status:number, callback) {
        super.query(
            "UPDATE chat SET bruker_updated_status=?, status=? where chat_id=?",
            [bruker_updated_status, status ,chat_id],
            callback
        );
    }

    /**
     * @function addChat() create chat in database
     * @param data information needed to create the chat
     * @param callback
     */
    addChat (data:Chat, callback) {
        super.query(
            "INSERT IGNORE INTO chat VALUES( default , ?, ?, ?, ?, ?, ?,default )",
            [
                data.oppdr_id,
                data.rute_id,
                data.bruker_1,
                data.bruker_2,
                0,
                0
            ],
            callback
        );
    }

    deleteChat(oppdr_id:number,bruker_1:number, bruker_2: number, callback) {
        super.query(
            "Delete FROM chat Where oppdr_id=? and bruker_1=? and bruker_2=? ORDER BY created_at DESC",
            [oppdr_id, bruker_1, bruker_2],
            callback
        );
    }
}

