
const daoParentUser = require("./dao");
import { compareHash, hash } from "../hashing";

export interface bruker {
  brukerId: number;
  navn: string;
  mobile: number;
  email: string;
  hash: string;
  salt: string;
  vurdering: number;
  bilde: Buffer;
}

/**
 *
 * @param data
 * @function clearSensitiveData is a method to delete hash and salt from the objects which are sent to the front-end.
 */
export function clearSensitiveData(data) {
  if (!data) return;
  if (data["hash"]) delete data["hash"];
  if (data["salt"]) delete data["salt"];

  return data;
}

/**
 * @daoClass userDao is used to do everything has to do
 * with User in db such get,add,update, delete...
 */
export default class userDao extends daoParentUser {
  constructor(pool) {
    super(pool);
  }

  /**
   * @function getAllUsers is used to gets all users
   * @param callback returns response
   * @return list of users if exists, null otherwise
   */
  getAllUsers(callback) {
    super.query(
      "SELECT bruker_id, navn, email, mobile, vurdering, bilde FROM bruker",
      [],
      callback
    );
  }
  /**
   * @function getUser() is used to get specific user given its id
   * @param brukerId Identifies the user of interest in database
   * @param callback
   * @return user if exist,null otherwise
   */
  getUser(brukerId:number ,callback){
    super.query(
        "SELECT navn, email, mobile, vurdering, bilde  FROM bruker Where bruker_id=?",
        [brukerId],
        callback
    );
  }
  /**
   @function getUserNoImage is used to get user without its pic to render it in several screen
   such chatList, reiserute to render it faster
   * @param brukerId Identifies the user of interest in database
   * @param callback returns response
   * @return user if exist,null otherwise
   */
  getUserNoImage(brukerId:number ,callback){
    super.query(
        "SELECT navn, email, mobile, vurdering FROM bruker Where bruker_id=?",
        [brukerId],
        callback
    );
  }
  /**
   * @function getUserByEmail() used in forgot password screen
   * @param email Identifies the user of interest in database
   * @param callback
   * @return user if exist,null otherwise
   */
  getUserByEmail(email: string, callback) {
    super.query(
        "SELECT bruker_id, navn, mobile, vurdering, bilde FROM bruker WHERE email = ?",
        [email],
        callback
    );
  }
  /**
   * @function getHashOfUser() used to implement functions such login
   * @param email Identifies the user of interest in database
   * @param callback
   * @return the hash of password to the user if exist,null otherwise
   */
  getHashOfUser(email: string, callback) {
    super.query(
        "SELECT hash, salt, bruker_id FROM bruker WHERE email = ?",
        [email],
        callback
    );
  }
  /**
   * @function addUser() used to register a user in database
   * @param data is user´s information that trying to sign up (name, mobile...)
   * @param callback
   * @return response we handle it in front-end, depends if user already exists,
   * some data is missing...
   */
  addUser(data, callback) {
    super.query(
        "INSERT INTO bruker VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?)",
        [
          data.navn,
          data.email,
          data.mobile,
          data.hash,
          data.salt,
          10,
          data.bilde,
        ],
        callback
    );
  }
  /**
   * @function updateUser() used to update information to a user
   * @param brukerId Identifies the user of interest in database
   * @param data object of type user to be updated
   * @param callback
   */
  updateUser(brukerId: number, data: bruker, callback) {
    let user;
    this.getUser(brukerId, (status, olddata) => {
      user = olddata[0];
      if (typeof data.navn == undefined) {
        data.navn = user.navn;
      }
      if (typeof data.email == undefined) {
        data.email = user.email;
      }
      if (typeof data.vurdering == undefined) {
        data.vurdering = user.vurdering;
      }
      if (typeof data.mobile == undefined) {
        data.mobile = user.mobile;
      }
      if (typeof data.bilde == undefined) {
        data.bilde = user.bilde;
      }
      super.query(
          "UPDATE bruker SET  email = ?, mobile = ?, bilde=? WHERE bruker_id = ?",
          [ data.email, data.mobile, data.bilde, brukerId],
          callback
      );
    });
  }
  /**
   * @function resetPassword() used to set a new password
   * @param email Identifies the user of interest in database
   * @param data used to store hash,salt of password to the user
   * @param callback
   */
  resetPassword(email: string, data, callback) {
    let newData = hash(data.password);
    let newHash = newData.hash;
    let newSalt = newData.salt;
    super.query(
        "UPDATE bruker SET  hash = ?, salt = ? WHERE email = ?",
        [newHash, newSalt, email],
        callback
    );
  }
  /**
   * @function changePassword() used to let the user change his password
   * @param email Identifies the user of interest in database
   * @param data used to store and compare hash,salt of password to the user
   * @param callback
   */
  changePassword(email: string, data, callback) {
    let user;
    this.getHashOfUser(email, (status, olddata) => {
      user = olddata[0];
      let oldHash = user.hash;
      let oldSalt = user.salt;
      let okPassword = compareHash(oldHash, data.oldPassword, oldSalt);
      if (okPassword) {
        let newData = hash(data.newPassword);
        let newHash = newData.hash;
        let newSalt = newData.salt;
        super.query(
            "UPDATE user SET  hash = ?, salt = ? WHERE email = ?",
            [newHash, newSalt, email],
            callback
        );
      } else {
        callback.sendStatus(403); //Forbidden
      }
    });
  }
  /**
   * @function changePicture used in profileScreen to change a pic of the user
   * @param brukerId Identifies the user of interest in database
   * @param data is the new picture
   * @param callback
   */
  changePicture(brukerId: number, data, callback) {
    super.query(
        "UPDATE bruker SET  bilde = ? WHERE bruker_id = ?",
        [data.bilde, brukerId],
        callback
    );
  }
  /**
   * @function deleteUser() allows user to delete his account from our database
   * @param brukerId Identifies the user of interest in database
   * @param callback
   */
  deleteUser(brukerId: number, callback) {
    super.query("DELETE FROM bruker WHERE bruker_id=?", [brukerId], callback);
  }
}


