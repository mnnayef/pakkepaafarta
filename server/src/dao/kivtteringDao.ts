

const daoParentKvittering = require("./dao");

export interface kvittering {
    kvittering_id: number;
    bruker_pendler: number;
    bruker_eier: number;
    pris: number;
    oppdrag_id: number;
    reiserute_id: number;
}

/**
 * @daoClass kvitteringDao (receipt) is used to do everything has to do
 * with kvittering in db such get,add,update, delete...
 * Kvittering/receipt is used to summarize / confirm agreement between two users
 */
export default class kvitteringDao extends daoParentKvittering {
    constructor(pool) {
        super(pool);
    }

    /**
     *
     * @param kvittering_id Identifies the receipt of interest in database
     * @param callback
     */
    getKvittering(kvittering_id:number ,callback){
        super.query(
            "Select kvittering_id, bruker_pendler, bruker_eier, pris, oppdrag_id, reiserute_id, tidspunkt FROM kvittering Where kvittering_id=?",
            [kvittering_id],
            callback
        );
    }
    /**
     * @param bruker_eier Identifies the owner of the package in database
     * @param bruker_pendler Identifies the user who picks up the package
     * @param callback
     */
    getKvitteringPendlerEier(bruker_eier:number, bruker_pendler:number, callback){
        super.query(
            "Select kvittering_id, bruker_pendler, bruker_eier, pris, oppdrag_id, reiserute_id, tidspunkt FROM kvittering Where (bruker_pendler=? AND bruker_eier=?) OR (bruker_pendler=? AND bruker_eier=?)",
            [bruker_pendler, bruker_eier, bruker_eier, bruker_pendler],
            callback
        );
    }
    /**
     * @function getAllKvitteringForBruker() lists all receipts to the user
     * @param bruker_id Identifies the user of interest in database
     * @param callback
     */
    getAllKvitteringForBruker(bruker_id:number, callback){
        super.query(
            "Select kvittering_id, bruker_pendler, bruker_eier, pris, oppdrag_id, reiserute_id, tidspunkt FROM kvittering Where bruker_eier=? OR bruker_pendler=?",
            [bruker_id, bruker_id],
            callback
        );
    }

    addKvittering(data:kvittering, callback){
        super.query(
            "INSERT INTO kvittering VALUES( DEFAULT, ?, ?, ?, ?, ?, DEFAULT)",
            [
                data.bruker_pendler,
                data.bruker_eier,
                data.pris,
                data.oppdrag_id,
                data.reiserute_id,
            ],
            callback
        );
    }

    updateKvittering(kvittering_id: number, data: kvittering, callback) {
        let kvittering;
        this.getKvittering(kvittering_id, (status, oldData) => {
            kvittering = oldData[0];
            if (typeof data.bruker_eier == "undefined") {
                data.bruker_eier = kvittering.bruker_eier;
            }
            if (typeof data.bruker_pendler == "undefined") {
                data.bruker_pendler= kvittering.bruker_pendler;
            }
            if (typeof data.pris == "undefined") {
                data.pris = kvittering.pris;
            }
            if (typeof data.oppdrag_id == "undefined") {
                data.oppdrag_id = kvittering.oppdrag_id;
            }
            if (typeof data.reiserute_id == "undefined") {
                data.reiserute_id = kvittering.reiserute_id;
            }
            super.query(
                "UPDATE kvittering SET bruker_eier=?, bruker_pendler=?, pris=?, oppdrag_id=?, reiserute_id=? WHERE kvittering_id = ?",
                [data.bruker_eier, data.bruker_pendler, data.pris, data.oppdrag_id, data.reiserute_id, kvittering_id],
                callback
            );
        });

    }

    deleteKvittering(kvittering_id: number, callback) {
        super.query("DELETE FROM kvittering WHERE kvittering_id=?", [kvittering_id], callback);
    }
}