
const daoParentMelding = require("./dao");

export interface MeldingDao {
    sender_id: number;
    chat_nr: number;
    mld: string;
}

/**
 * @daoClass meldingDao is used to do everything has to do
 * with messages in db such get,add,update, delete...
 */
export default class meldingDao extends daoParentMelding {
    constructor(pool) {
        super(pool);
    }

    getAllMeldinger(sender_id:number, callback) {
        super.query(
            "Select mld_id,chat_nr,sender_id, mld, status,sendt_at FROM melding Where sender_id=? ORDER BY sendt_at DESC",
            [sender_id],
            callback
        );
    }


    getMeldingByUser(chat_nr:number,sender_id:number, callback) {
        super.query(
            "Select mld_id,chat_nr,sender_id, mld, status,sendt_at FROM melding Where chat_nr=? and sender_id=? ORDER BY sendt_at DESC",
            [chat_nr, sender_id],
            callback
        );
    }
    /**
     * @function getMeldingByChatId() used in chatScreen to list all messages of a given chatId
     * @param chat_nr Identifies the chat of interest in database
     * @param callback
     */
    getMeldingByChatId(chat_nr:number, callback) {
        super.query(
            "Select  mld_id,chat_nr,sender_id, mld, status,sendt_at FROM melding Where chat_nr=? ORDER BY sendt_at DESC",
            [chat_nr],
            callback
        );
    }

    getMaxMelding(callback) {
        super.query(
            "Select max(mld_id) as mld_id FROM melding",
            [],
            callback
        );
    }
    /**
     *@function addMelding() used to store all sent messages in database
     * @param data the message object itself to be stored in database
     * @param callback
     */
    addMelding (data:MeldingDao, callback) {
        super.query(
            "INSERT IGNORE INTO melding VALUES( default , ?, ?, ?, 0, default )",
            [
                data.chat_nr,
                data.sender_id,
                data.mld
            ],
            callback
        );
    }
    /**
     * @function updateStatus() to show status of message inside the chatBox
     * @param melding_id Identifies the message of interest in database
     * @param status set as read/ been seen
     * @param callback
     */
    updateStatus(melding_id:number,status:number, callback) {
        super.query(
            "UPDATE melding SET status=? where melding_id=?",
            [status ,melding_id],
            callback
        );
    }


    deleteMelding(mld_id:number, callback) {
        super.query(
            "Delete FROM melding Where mld_id=?",
            [mld_id],
            callback
        );
    }
}

