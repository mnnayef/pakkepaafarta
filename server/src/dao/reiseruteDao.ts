

const daoParentReiserute = require("./dao");

export interface reiserute {
    reiserute_id: number;
    fra_by: string;
    til_by: string;
    tidspunkt: string;
    bruker_pendler: number;
    varebil: number;
    sykkel_personbil: number;
    pris:number;
    informasjon: string;
    stopp_by1 : string;
    stopp_by2 : string;
    stopp_by3 : string;
}

/**
 * @daoClass reiseruteDao is used to do everything has to do
 * with reiserute (route) in db such get,add,update, delete...
 */
export default class reiseruteDao extends daoParentReiserute {
    constructor(pool) {
        super(pool);
    }

    getAllReiserute(callback) {
        super.query(
            "SELECT reiserute_id, fra_by, til_by, tidspunkt, bruker_pendler, stopp_by1, stopp_by2, stopp_by3, varebil, sykkel_personbil, pris, informasjon, status FROM reiserute",
            [],
            callback
        );
    }

    getReiserute(reiserute_id:number ,callback){
        super.query(
            "Select reiserute_id, fra_by, til_by, stopp_by1, stopp_by2, stopp_by3, tidspunkt, bruker_pendler, varebil, sykkel_personbil, informasjon, pris, status FROM reiserute Where reiserute_id=?",
            [reiserute_id],
            callback
        );
    }

    /**
     * @function getReiseruteForUser() used to list all routes to the user
     * @param bruker_id Identifies the route of interest in database
     * @param callback
     */
    getReiseruteForUser(bruker_id:number ,callback){
        super.query(
            "Select reiserute_id, fra_by, til_by, stopp_by1, stopp_by2, stopp_by3, tidspunkt, bruker_pendler, varebil, sykkel_personbil, informasjon, pris, status FROM reiserute Where bruker_pendler=?",
            [bruker_id],
            callback
        );
    }

    /**
     * @function getReiseruteFromTo() gets a list of routes based on,
     * @param from the pick up address
     * @param to the destination
     * from and to can be the stop points on the way to the destination
     * @param callback
     */
    getReiseruteFromTo(from: string, to: string, callback) {
        super.query(
            "SELECT reiserute_id, fra_by, til_by, stopp_by1, stopp_by2, stopp_by3, tidspunkt, bruker_pendler, varebil, sykkel_personbil, informasjon, pris, status FROM reiserute " +
            "Where (tidspunkt >= CURDATE()) " +
            " And( (fra_by=? And stopp_by1=?) or (fra_by=? And stopp_by2=?) or (fra_by=? And stopp_by3=?) or (fra_by=? And til_by=?)" +
            "or (stopp_by1=? And stopp_by2=?) or (stopp_by1=? And stopp_by2=?) or (stopp_by1=? And til_by=?)" +
            "or (stopp_by2=? and stopp_by3=?) or (stopp_by2=? and til_by=?)" +
            "or (stopp_by3=? and til_by=?))",
            [from, to, from, to, from, to, from, to, from, to, from, to, from, to, from, to, from, to, from, to],
            callback
        );
    }

    addReiserute(data: reiserute, callback) {
        super.query(
            "INSERT INTO reiserute VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?)",
            [
                data.fra_by,
                data.til_by,
                data.tidspunkt,
                data.bruker_pendler,
                data.varebil,
                data.sykkel_personbil,
                data.pris,
                data.informasjon,
                "gyldig",
                data.stopp_by1,
                data.stopp_by2,
                data.stopp_by3
            ],
            callback
        );
    }

    /**
     * @function updateReiserute() allows user to edit an existing route
     * @param reiserute_id Identifies the route of interest in database
     * @param data the route object itself to be updated
     * @param callback
     */
    updateReiserute(reiserute_id: number, data: reiserute, callback) {
        let reiserute;
        this.getReiserute(data.reiserute_id, (status, oldData) => {
            reiserute = oldData[0];
            if (typeof data.fra_by == "undefined") {
                data.fra_by = reiserute.fra_by;
            }
            if (typeof data.til_by == "undefined") {
                data.til_by = reiserute.til_by;
            }
            if (typeof data.tidspunkt == "undefined") {
                data.tidspunkt = reiserute.tidspunkt;
            }
            if (typeof data.sykkel_personbil == "undefined") {
                data.sykkel_personbil = reiserute.sykkel_personbil;
            }
            if (typeof data.sykkel_personbil == "undefined") {
                data.pris = reiserute.pris;
            }
            if (typeof data.informasjon == "undefined") {
                data.informasjon = reiserute.informasjon;
            }
            if (typeof data.varebil == "undefined") {
                data.varebil = reiserute.vareBil;
            }
            if (typeof data.stopp_by1 == "undefined") {
                data.stopp_by1 = reiserute.stopp_by1;
            }
            if (typeof data.stopp_by2== "undefined") {
                data.stopp_by2 = reiserute.stopp_by2;
            }
            if (typeof data.stopp_by3 == "undefined") {
                data.stopp_by3 = reiserute.stopp_by3;
            }
            super.query(
                "UPDATE reiserute SET fra_by = ?, til_by = ?, tidspunkt = ?, bruker_pendler = ?, varebil = ?, sykkel_personbil=?, pris=? , informasjon=?, status=?, stopp_by1=?, stopp_by2=?, stopp_by3=? WHERE reiserute_id = ?",
                [data.fra_by, data.til_by, data.tidspunkt, data.bruker_pendler, data.varebil, data.sykkel_personbil, data.pris, data.informasjon, "gyldig",data.stopp_by1, data.stopp_by2, data.stopp_by3, reiserute_id],
                callback
            );
        });
    }

    updateStatus(reiserute_id: number, status:string, callback){
        super.query("update reiserute set status=? Where reiserute_id=?",[status,reiserute_id], callback)
    }

    deleteReiserute(reiserute_id: number, callback) {
        super.query("DELETE FROM reiserute WHERE reiserute_id=?", [reiserute_id], callback);
    }
}
