export type RootStackParamList = {
    Welcome: undefined;
    Login: undefined;
};

export type BottomTabParamList = {
    Login: undefined;
    SignUp: undefined;
};



export type TabLoginParamList = {
    LoginScreen: undefined;
};

export type TabSignUpParamList = {
    SignUpScreen: undefined;
};
