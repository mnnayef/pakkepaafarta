
import React, { useEffect, useState} from 'react';
import {
    Alert,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
} from 'react-native';
import NavbarHeader from "../navigation/NavbarHeader";
import {oppdragService} from "../services/oppdragService"
import MapView, { Marker} from 'react-native-maps';
import * as Location from 'expo-location';
import { useIsFocused} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as SecureStore from "expo-secure-store";
import jwt_decode from 'jwt-decode';


declare type OppdragInfo= {
    latitude: number,
    longitude: number,
    til_by:"trondheim"
    tilAdresse:"",
    oppdrag_id: 0,
    pris:55,
    id:number
}

/**
 *
 * @param props
 * @constructor
 * @method FindOppdrag page used to show all registered commissions on the map,
 * so that other users can see them and possibly pick up packages that suit their route
 *
 * - When marker clicked on, it pops up a small card (Callout),
 *  with briefly summarized details of the selected item (oppdrag)
 * - When the card clicked we navigate the user to the details screen of this commission (item)
 *
 * In this class we use:
 * - MapView, Marker from react-native-maps
 * - Location from expo-location to get coordinates of addresses
 */

const FindOppdrag = (props: any) => {

    const {navigate} = props.navigation;
    const [loading, setLoading] = useState(true);
    const isFocused = useIsFocused();
    const [oppdragList, setOppdragList] = useState<OppdragInfo[]>([]);
    const [loadingUpdate, setLoadingUpdate] = useState(true);

    let count=0;



    useEffect(() => {
        handleFetch().then(()=>{
        }).catch(error=>console.log(error))
    }, [props, isFocused]);

    /**
     * @this handleFetch runs each time the component is opened or the props are changed,
     * used to get all commissions
     */
    const handleFetch = async () => {
        beforeDone(0,50);
        Location.requestPermissionsAsync();
        setLoadingUpdate(true);
        await SecureStore.getItemAsync('x-auth-token').then(result => {
            // User not logged in ==> get all oppdrag
            if (!result) {
                oppdragService.getAllOppdragWithoutPic().then(async data => {
                    if (data[0] === undefined){
                        Alert.alert("Netverk feil", "Vennligst sjekk internet kobling");
                        setLoading(false);
                        navigate("Home");
                        return;
                    }

                    if(data ){
                        setLoading(true);
                        for(let i=0;i<data.length;i++){
                            if(data[i].status=="gyldig"){
                                await Location.geocodeAsync(data[i].fra_adresse).then(obj=>{
                                    if(obj[0]!==undefined){
                                        oppdragList[i].longitude=obj[0].longitude;
                                        oppdragList[i].latitude=obj[0].latitude;
                                        oppdragList[i].tilAdresse= data[i].til_adresse;
                                        oppdragList[i].oppdrag_id= data[i].oppdrag_id;
                                        oppdragList[i].pris= data[i].pris;
                                        oppdragList[i].til_by= data[i].til_by;
                                        oppdragList[i].id=i
                                    }
                                    count++;
                                    if (count >data.length - 1) done();
                                }).catch(error=>console.log(error))
                            }
                        }
                        setLoading(false)
                    }
                });
            }
            // User logged inn ==> do not show the commissions that the user own
            else {
                let token=jwt_decode(result);
                oppdragService.getAllOppdragWithoutPic().then(async data => {
                    if (data[0] === undefined){
                        Alert.alert("Netverk feil", "Vennligst sjekk internet kobling");
                        setLoading(false);
                        navigate("Home");
                        return;
                    }
                    if (data ) {
                        setLoading(true);
                        for(let i=0;i<data.length;i++){
                            if(data[i].bruker_eier !== token.id &&data[i].status=="gyldig") {
                                await Location.geocodeAsync(data[i].fra_adresse).then(obj=>{
                                    if(obj[0]!==undefined){
                                        oppdragList[i].longitude=obj[0].longitude;
                                        oppdragList[i].latitude=obj[0].latitude;
                                        oppdragList[i].tilAdresse= data[i].til_adresse;
                                        oppdragList[i].oppdrag_id= data[i].oppdrag_id;
                                        oppdragList[i].pris= data[i].pris;
                                        oppdragList[i].til_by= data[i].til_by;
                                        oppdragList[i].id=i

                                    }
                                    count++;
                                    if (count >data.length - 1) done();

                                }).catch(error=>console.log(error))
                            } else {
                                count++;
                            }
                        }
                        setLoading(false)
                    }

                });
            }
        });
    };


    function done(){

        if(oppdragList[count-1].longitude!=10.111){
            setLoading(false);
        }
    }
    function beforeDone( start:number, epoch:number){
        for(let i=start;i<epoch;i++){
            oppdragList[i]= {longitude:63.111,latitude:10.421906,tilAdresse:"",til_by:"trondheim",oppdrag_id:0,pris:55, id:i};
        }
    }



    function Greeting() {
        if (loading) {
            return (
                <Spinner
                    visible={loading}
                    textContent={'Loading...'}
                    textStyle={mapStyles.spinnerTextStyle}
                />
            )
        }
        return (
            <SafeAreaView style={mapStyles.container}>
                <NavbarHeader />

                <MapView
                    style={mapStyles.mapView}
                    initialRegion={{
                        latitude: 63.446827,
                        longitude: 10.421906,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                >
                    {oppdragList.map(marker => {
                        return(
                            <Marker
                                key={marker.id}
                                coordinate={{latitude: marker.latitude, longitude: marker.longitude}}
                                title={"Til: "+marker.til_by +"/ "+ marker.tilAdresse}
                                description={"pris: "+ marker.pris+", klikk for mer detaljer"}
                                onCalloutPress={ () => navigate('OppdragDetails',{OppdragID:marker.oppdrag_id})}
                            />
                        )})}
                </MapView>

            </SafeAreaView>
        )

    }

    return (
        <Greeting/>
    );
};

const mapStyles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        margin:5
    },

    mapView:{
        flex:3.5,
        flexDirection: "column",
        alignItems: "center",
    },
    spinnerTextStyle: {
        color: '#FFF',
    },

});

export default FindOppdrag;

