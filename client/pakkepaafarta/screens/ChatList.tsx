
import React, {useCallback, useState} from "react";
import {
    Platform,
    ScrollView,
    StatusBar,
    StyleSheet,
    View
} from 'react-native';
import NavbarHeader from "../navigation/NavbarHeader";
import jwt_decode from 'jwt-decode';
import * as SecureStore from "expo-secure-store";
import {chatOppdragService, ChatOppdrag} from "../services/chatOppdragService";
import {useIsFocused, useNavigation, useFocusEffect} from "@react-navigation/native";
import Spinner from 'react-native-loading-spinner-overlay';
import {ListItem} from 'react-native-elements'
import TouchableScale from 'react-native-touchable-scale';
import { MaterialCommunityIcons} from "@expo/vector-icons";
import {brukerService} from "../services/brukerService";
import DataNotFound from "../componentes/dataNotFound"; // https://github.com/kohver/react-native-touchable-scale


/**
 *
 * @param route
 * @constructor
 * @scree ChatList Page used to list all chats to the user
 */
const ChatList =({route}) => {
    const navigation = useNavigation();
    const [chatList, setChatList] = useState<ChatOppdrag[]>([]);
    const [users, setUsers] = useState<string[]>([]);
    const [loading, setLoading] = useState(true);
    const [toki, setToki] = useState("");
    const isFocused = useIsFocused();
    let interval;

    /**
     * @this useFocusEffect used to run get chats on focus,
     * and clean it up when the screen becomes unfocused
     */
    useFocusEffect(
        useCallback(() => {

            fetchSamtaler()
            return () => {
                clearInterval(interval)
            };
        }, [])
    )


    /**
     * @this fetchSamtaler used to get all chat given user id
     * which we get from the token saved in SecureStore
     */
    const fetchSamtaler = async () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate("Login")
            return
        }
        let token = jwt_decode(authToken)
        setToki(authToken)
        interval = setInterval(() => {
            chatOppdragService.getChatByUserId(parseInt(token.id), authToken).then(async data => {
                if (data[0] === undefined){
                    setChatList([])
                    setLoading(false)
                    return;
                }
                if(data){
                    setLoading(true)
                    if(data[0].bruker_1!==token.id && data[0].bruker_2!==token.id){
                        setChatList([])
                    }

                    for(let i=0;i<data.length;i++){
                        if(data[i]!==undefined){
                            if(chatList[i]===undefined) {
                                chatList[i]=data[i];
                            }
                            if(users[i]===undefined) {
                                if(data[i].bruker_1===token.id){
                                    brukerService.getUserInfoById(data[i].bruker_2).then(async data =>users[i]=data[0].navn)
                                }else{
                                    brukerService.getUserInfoById(data[i].bruker_1).then(async data =>users[i]=data[0].navn)
                                }
                            }
                            chatList[i]=data[i];
                        }
                    }
                    setLoading(false)
                }
            });
        },2000)
    };


    function RenderChatList(){
        if (loading) {
            return (
                <Spinner
                    visible={loading}
                    textContent={'Loading...'}
                    textStyle={ChatListStyle.spinnerTextStyle}
                />
            )
        }
        // The user has no chat
        if( chatList.length<1) {
            return <DataNotFound/>
        }
        return (
            <ScrollView style={ChatListStyle.container}>
                <View style={ChatListStyle.marginHeader}>
                    <NavbarHeader/>
                </View>

                {
                    chatList.map((item, i) => (
                        <ListItem
                            onPress={ () =>{
                                navigation.navigate('Chat',{chatId:item.chat_id})
                                chatOppdragService.updateStatus(item.chat_id,item.bruker_1,1,toki)
                            } }
                            Component={TouchableScale}
                            key={i}
                            bottomDivider={true}
                            style={ChatListStyle.itemStyling}>

                            <ListItem.Content style={{flex:1, flexDirection : "row" }}>
                                <ListItem.Title style={{width:"90%", height:40, justifyContent:"flex-end", alignItems:"flex-end"}}>Samtale med {users[i]}</ListItem.Title>
                                {(item.status==0&&item.bruker_updated_status!==jwt_decode(toki).id)
                                    ?<ListItem.Content style={{ width:"10%", justifyContent:"flex-end", alignItems:"flex-end"}}><MaterialCommunityIcons name="chat-alert" size={20} color="red" /></ListItem.Content>
                                    :null
                                }

                            </ListItem.Content>
                        </ListItem>
                    ))
                }
            </ScrollView>
        )
    }

    return(
        <RenderChatList/>
    )
}


const ChatListStyle = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        paddingBottom: 50
    },
    marginHeader:{
        marginBottom:10
    },
    textINput:{
        margin:40
    },
    spinnerTextStyle: {
        color: '#FFF',
    },
    itemStyling:{
        paddingTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    }
})

export default ChatList