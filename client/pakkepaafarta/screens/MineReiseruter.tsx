
import React, {useCallback, useEffect, useState} from "react";
import {
    Platform,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View
} from 'react-native';
import NavbarHeader from "../navigation/NavbarHeader";
import jwt_decode from 'jwt-decode';
import * as SecureStore from "expo-secure-store";
import {Reiserute, reiseruteService} from "../services/reiseruteService";
import {useIsFocused, useNavigation} from "@react-navigation/native";
import Spinner from 'react-native-loading-spinner-overlay';
import {ListItem, Avatar} from 'react-native-elements'
import TouchableScale from 'react-native-touchable-scale';
import DataNotFound from "../componentes/dataNotFound";


/**
 *
 * @param props
 * @constructor
 * @screen MineReiseruter used to show list of all routes the user has ,
 * We use ListItem, Avatar from react-native-elements to render this list of routes
 *
 * The user cal also see a full info about a route by clicking on one of the items

 */
const MineReiseruter =(props) => {
    const isFocused = useIsFocused();
    const navigation = useNavigation();
    const [reiseruteList, setReiseruteList] = useState<Reiserute[]>([]);
    const [loading, setLoading] = useState(true);


    useEffect(() => {

        getUserOppdrag().then(()=>{
        }).catch(error=>console.log(error))
    }, [props, isFocused]);


    const getUserOppdrag = useCallback(async () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        let token = jwt_decode(authToken);

        reiseruteService.getReiseruteForUser(parseInt(token.id)).then(async data => {


            if (data[0] === undefined){
                setReiseruteList([]);
                setLoading(false);
                return(
                    <Text>Du har ingen reiseruter</Text>
                )
            }
            if(data){
                if(data[0].bruker_pendler!==token.id){
                    setReiseruteList([])
                }
                setReiseruteList(data);
                for(let i=0;i<data.length;i++){
                    if(data[0]!==undefined){
                        reiseruteList[i]=data[i];
                    }
                }
                setLoading(false);
            }

        });
    },[]);


    function RenderReiseruteList(){
        if (loading) {
            return (
                <Spinner
                    //visibility of Overlay Loading Spinner
                    visible={loading}
                    //Text with the Spinner
                    textContent={'Loading...'}
                    //Text style of the Spinner Text
                    textStyle={mineOppdragStyles.spinnerTextStyle}
                />
            )
        }
        if( reiseruteList.length<1) {
            return <DataNotFound/>
        }
        return (
            <ScrollView style={mineOppdragStyles.container}>
                <View style={mineOppdragStyles.marginHeader}>
                    <NavbarHeader/>
                </View>

                {
                    reiseruteList.map((item, i) => (
                        <ListItem
                            // sending oppdrag id parameter with navigate to render details screen
                            onPress={ () => navigation.navigate('MinReiseruteDetails',{minReiseruteId:item.reiserute_id})}
                            Component={TouchableScale}
                            key={i}
                            bottomDivider={true}
                            style={mineOppdragStyles.itemStyling}>
                            <Avatar source={{uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg'}} size="large"/>
                            <ListItem.Content >
                                <ListItem.Title >{item.fra_by} {"--------------->"} {item.til_by}</ListItem.Title>
                                <ListItem.Subtitle style={{color:"#5fd4c0"}}><Text style={{color:"#000000"}}>Pris:</Text>  {item.pris}  ,-KR</ListItem.Subtitle>
                                {(item.stopp_by1!==""||item.stopp_by2!=""|| item.stopp_by3!="")
                                    ?<ListItem.Subtitle style={{color:"#5fd4c0"}}><Text style={{color:"#000000"}}>Mellombyer:</Text> {item.stopp_by1}, {item.stopp_by2}, {item.stopp_by3}.</ListItem.Subtitle>
                                    :null
                                }
                                <ListItem.Subtitle style={{color:"#5fd4c0"}}><Text style={{color:"#000000"}}>Dato:</Text> {item.tidspunkt.split("T")[0]}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>
                    ))
                }
            </ScrollView>
        )
    }

    return(
        <RenderReiseruteList/>
    )
};


const mineOppdragStyles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        paddingBottom: 50
    },
    marginHeader:{
        marginBottom:10
    },
    textINput:{
        margin:40
    },
    spinnerTextStyle: {
        color: '#FFF',
    },
    itemStyling:{
        paddingTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    }
});

export default MineReiseruter;