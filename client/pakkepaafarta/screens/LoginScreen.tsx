
import React, {useEffect, useState} from 'react';
import {
    Alert,
    Button,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import NavbarHeader from "../navigation/NavbarHeader";
import styles from '../componentes/MyStyles'
import { brukerService} from "../services/brukerService";
import { useNavigation} from '@react-navigation/native';

import useCachedResources from "../hooks/useCachedResources";

/**
 * @param props
 * @constructor
 * @screen LogInScreen page used to let the user login with signed up mail and password,
 * - Login function is required to be able to use functions eg
 * (add commission (oppdrag)/route (reiserute), chat with users
 */
export default function LogInScreen(props) {
    const navigation = useNavigation();
    const [emailInput, setEmailInput] = useState("");
    const [passwordInput, setPasswordInput] = useState("");
    const [redirect, setRedirect] = useState(false);
    const isFocused = useCachedResources();

    useEffect(() => {
        // Subscribe for the focus Listener
        const unsubscribe = navigation.addListener('focus', () => {
            setEmailInput("");
            setPasswordInput("");
        });
        return () => {
            // Unsubscribe for the focus Listener
            unsubscribe;
        };
    }, [props]);

    // Used to display error on empty input when submitting
    const [submit, setSubmit] = useState(false);

    // Check inputs and try to log in with the given username and password
    const login = async () => {

        // User tries to submit/login, will activate error checks in inputs
        setSubmit(true);

        // Check if inputs are empty
        if (emailInput.trim() === "" || passwordInput.trim() === "") {
            Alert.alert("Ett eller flere felter er tomme");
            return;
        }


        let res = await brukerService.login(emailInput, passwordInput);
        if(!res) Alert.alert("Netverk feil", "sjekk internett tilkoblingen");

         if (res instanceof Error) {
            Alert.alert("Noe feil skjedde", "sjekk internett tilkoblingen");
        }

         // Email/tlf or password is wrong (not authenticated)
         else if (res && res.status === 204) {
            Alert.alert("Email eller passord er feil, prøv igjen");
        }


        // Email and password match, log user in
        else {
             props.navigation.navigate("Profile",{
                 itemId:emailInput,
             });

             setRedirect(true)
        }
    };

    return (
        <SafeAreaView style={mystyle.container}>
            <NavbarHeader />
            <View >
                <Text
                    style={[styles.title,{
                    marginTop:50
                    }]}>
                    E-mail
                </Text>

                <View
                    style={styles.action}>
                    <TextInput
                        value={emailInput}
                        textContentType={"emailAddress"}
                        accessibilityLabel={"E-post adresse"}
                        placeholder="   Din e-postadresse.."
                        style={styles.textInput}
                        autoCompleteType={"email"}
                        onChangeText={text => setEmailInput(text)}
                    />
                </View>

                <Text style={[styles.title,{
                    marginTop:20
                }]}>Password</Text>

                <View style={[styles.action,{paddingBottom:15}]}>
                    <TextInput
                        value={passwordInput}
                        secureTextEntry
                        textContentType={"password"}
                        placeholder="   Skriv inn ditt password.."
                        style={styles.textInput}
                        onChangeText={text => setPasswordInput(text)}
                    />
                </View>

                <View  >
                    <Button
                        accessibilityLabel="Logg inn knapp"
                        onPress={() => login()}
                        title="Login"
                        color={"#3ecba7"}
                    />
                </View>



                    <View style={mystyle.linkContainer}>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Registrer')}
                            style={mystyle.link}>
                            <Text
                                style={{ color:"red", fontSize:15, left:20}} >
                                Har ikke bruker? registrer
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => navigation.navigate('ForgetPassword')}
                            style={mystyle.link}>
                            <Text
                                style={{ color:"blue", fontSize:15,marginTop:20, left:50}} >
                                Glemt passord!
                            </Text>
                        </TouchableOpacity>

                    </View>



            </View>
        </SafeAreaView>
    );
}

const mystyle = StyleSheet.create({
    container:{
        flex:0.75,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        margin:5
    },

    buttonsContainer:{
        flex:0.7,
        backgroundColor: "red",
        flexDirection: "column",
        alignItems: "center"
    },
    buttonStyle:{
        width:240,
        elevation: 4,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#3ecba7",
        top:20
    },
    ButtonText:{
        fontSize: 22,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    textInput: {
        flex:1,
        marginTop:2,
        paddingBottom:15,
        color:'gray'
    },
    action: {
        flexDirection:'row',
        borderBottomWidth:1,
        borderBottomColor:'#f2f2f2'
    },
    linkContainer:{
        marginTop: 20,
        flexDirection: "column",
        alignItems: "center",
        alignContent:"center"

    },
    link:{
        width:240,
        top:20,

    }
});
