
import { Image, Platform, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useCallback, useState} from "react";
import {
    AntDesign,
    FontAwesome,
    FontAwesome5,
    Ionicons,
    MaterialCommunityIcons,
} from "@expo/vector-icons";
import {useFocusEffect, useNavigation} from "@react-navigation/native";
import * as SecureStore from "expo-secure-store";
import jwt_decode from 'jwt-decode';
import {chatOppdragService} from "../services/chatOppdragService";

/**
 *
 * @param props
 * @constructor
 * @screen MenuScreen page is a customized menu navigator to the main functions in our application,
 * eg ( My side "profile or login screen" , Add commission...etc)
 */
const MenuScreen = (props) => {
    const navigation = useNavigation();
    const {navigate} = props.navigation;
    const [user_id, setUser_id] = useState(0);
    const [checkMessage, setCheckMessage] = useState(false);
    const [token, setToken] = useState("");
    let interval;


    useFocusEffect(
        useCallback(() => {
            // Do something when the screen is focused
            checkMessages();
            return () => {
                clearInterval(interval)
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, [])
    );

    const checkMessages = async () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            return
        }
        let toki = jwt_decode(authToken);
        setToken(authToken);
        setUser_id(toki.id);
        interval= setInterval(() => {
            chatOppdragService.getUnreadMessages(toki.id,authToken).then(data => {
                if( data[0] === undefined) {
                    setCheckMessage(false);
                    return
                }else{
                    setCheckMessage(true);
                }
            })
        }, 1000);  //runs every 100 m second
    };

    const checkLoggin = async (route:string) => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate("Login");
            return;
        }
        navigation.navigate(route);
    };


    return (
        <SafeAreaView style={menuStyle.container}>
            <View  style={menuStyle.navbarView}>

                <TouchableOpacity
                    onPress={() => navigate('Home')}
                >
                    <Image style={{left:13}} source={require("../assets/images/logo2.png")}/>
                </TouchableOpacity>


                <FontAwesome
                    onPress={() => checkLoggin("Login")}
                    style={{right:-70, paddingTop:6}} name="user-circle" size={32} color="black"
                />

                <Ionicons
                    style={{left:-17}}
                    onPress={() => navigation.goBack()}
                    name="md-arrow-round-forward" size={45} color="black" />

            </View>
            <View style={menuStyle.menuTitle}>
                <Text style={menuStyle.titleText}>Menu </Text>
            </View>

            <View style={menuStyle.linkContainer}>
                <TouchableOpacity
                    style={menuStyle.linkView}
                    onPress={() => navigate('Home')}
                >
                    <Text style={menuStyle.linkStyling}>
                        HovedSide
                    </Text>
                    <AntDesign name="home" size={30} />
                </TouchableOpacity>

                <TouchableOpacity
                    style={menuStyle.linkView}
                    onPress={() => checkLoggin("Profile")}
                >
                    <Text style={menuStyle.linkStyling}>
                        Min side
                    </Text>
                    <AntDesign name="smileo" size={30}  />
                </TouchableOpacity>

                <TouchableOpacity
                    style={menuStyle.linkView}
                    onPress={() => checkLoggin("ChatList")}
                >
                    <Text style={menuStyle.linkStyling}>
                        Smataler
                    </Text>
                    {checkMessage
                        ?<MaterialCommunityIcons name="chat-alert" size={20} color="red" />
                        :<AntDesign name="wechat" size={30}  />

                    }

                </TouchableOpacity>

                <TouchableOpacity
                    style={menuStyle.linkView}
                    onPress={() => checkLoggin('AddCommission')}
                >
                    <Text style={menuStyle.linkStyling}>
                        Legg til oppdrag
                    </Text>
                    <MaterialCommunityIcons name="package-up" size={30}/>
                </TouchableOpacity>

                <TouchableOpacity
                    style={menuStyle.linkView}
                    onPress={() => checkLoggin('AddTripRoute')}
                >
                    <Text style={menuStyle.linkStyling}>
                        Legg til reiserute
                    </Text>
                    <FontAwesome5 name="route" size={30} color="black" />
                </TouchableOpacity>



            </View>
        </SafeAreaView>
    )
};

const menuStyle = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    menuTitle:{

        flex: 0.1,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor: "#e8ede9"
    },
    titleText:{
        fontSize:30,
        color: "#0c0d0c",
        flexDirection:"row",

        marginTop: 3,
    },
    linkContainer:{
        flex:1,
        backgroundColor:"#e8ede9"
    },
    linkView:{
        height: 40,
        margin: 10,
        marginHorizontal:30,
        overflow: "hidden",
        flexDirection : "row",
        justifyContent: "space-between",
    },
    linkStyling:{
        fontSize:20,
        color: "#0c0d0c"
    },
    navbarView:{
        borderStyle:'dashed',
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor:"#e8ede9"
    }

});
export default MenuScreen;