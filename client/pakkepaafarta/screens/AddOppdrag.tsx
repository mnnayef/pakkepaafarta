

import * as React from 'react';
import {
    StyleSheet,
    View,
    Text,
    SafeAreaView,
    Platform,
    StatusBar,
    TouchableOpacity,
    ScrollView, TextInput, Alert, Button, Image
} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import Swiper from 'react-native-swiper';
import NavbarHeader from "../navigation/NavbarHeader";
import myStyles from "../componentes/MyStyles";
import {useIsFocused, useNavigation} from "@react-navigation/native";
import {useCallback, useEffect, useState} from "react";
import * as SecureStore from "expo-secure-store";
const citiesData = require('./no.json');
import {Oppdrag, oppdragService} from '../services/oppdragService'
import jwt_decode from 'jwt-decode';
import DropDownPicker from "react-native-dropdown-picker";
import DateTimePicker from "@react-native-community/datetimepicker";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import * as FileSystem from "expo-file-system";
import * as Location from "expo-location";

const PAGES = ['Side 1', 'Side 2', 'Side 3','Side 4'];

const firstIndicatorStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 4,
    separatorFinishedColor: '#0dce9c',
    separatorUnFinishedColor: '#a4d4a5',
    stepIndicatorFinishedColor: '#0dce9c',
    stepIndicatorUnFinishedColor: '#a4d4a5',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 15,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
    labelColor: '#666666',
    labelSize: 12,
    currentStepLabelColor: '#0dce9c',
};

/**
 *
 * @param props
 * @constructor
 * @screen AddOppdrag Page for adding a new commission (oppdrag)
 *
 * It contains a stepper with different forms/pages and validations.
 * Each page is unique with different info needed to create a commission.
 *
 * - Page 1 (From and to City /address )
 * - Page 2 (Transportation mean and suggested price to be paid from the owner)
 * - Page 3 (Transport date, and more info about the package " Picture and text")
 * - Page 4 (brief summary of all pages before upload)
 */
export default function AddOppdrag(props:{userData?:any; logIn?:Function}) {
    const [currentPage, setCurrentPage] = React.useState<number>(0);
    const [loading, setLoading] = useState(false);
    const navigation = useNavigation();
    const [showDato, setShowDato] = useState(false);
    const[month,setMonth]=useState(0);
    const[dato,setDato]=useState(new Date());
    const isFocused = useIsFocused();
    const MaxPicture=221857;

    //Oppdrag informasjon
    const [fra_by, setFra_by] = useState('');
    const [til_by, setTil_by] = useState("");
    const [fra_adresse, setFra_adresse] = useState("");
    const [til_adresse, setTil_adresse] = useState("");
    const [tidspunkt, setTidspunkt] = useState("");
    const [varebil, setVarebil] = useState(0);
    const [bruker_eier, setBruker_eier] = useState(0);
    const [sykkel_personbil, setSykkel_personbil] = useState(0);
    const [pris, setPris] = useState("");
    const [beskrivelse,setbeskrivelse] = useState('');
    const [warningText, setWarningText] = useState("");
    const [bilde, setBilde] = useState("");
    const [token, setToken] = useState("");



    var items = [
        {
            id: 1,
            name: 'Oslo',
        }
    ]
    const fillArrays = () => {
        for(let i=0; i<citiesData.length;i++){
            let city= {} as typeof items[0];
            city.id=i ;
            city.name= citiesData[i].city;
            items[i]=city;
        }
        return items;
    }
    fillArrays()

    useEffect(() => {
        setLoading(true)
        handleFetch().then(()=>{}).catch(error=>console.log(error))
    }, []);


    /**
     * Method to check if user logged in before getting access to this page,
     * if not navigate the user to login screen
     */
    const handleFetch = useCallback(async () => {
        const authToken= await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate('Login')
            setLoading(false)
            return
        }
        setToken(authToken)
        let toki=jwt_decode(authToken)
        setBruker_eier(toki.id)

    },[]);


    //Stepper
    const onStepPress = (position: number) => {
        setCurrentPage(position);
    };

    /**
     * @function isCityInNorway A method to check weather the city is in norway or not
     * @param city: Identifies the city of interest
     */

    function isCityInNorway(city:string){
        for(let i=0;i<items.length;i++){
            if(city.toUpperCase().trim().normalize()===items[i].name.toString().toUpperCase().normalize()) {
                return true
            }
        }
        return false
    }

    /**
     * A method to check if the address is valid in Norway
     * @param address: Identifies the city of interest
     */
    async function isAddressInNorway(address: string) {
        await Location.geocodeAsync(address).then(obj => {
            if (obj[0] !== undefined) {
                return true
            }else {
                return false
            }
        }).catch(error => console.log(error))
    }

    /**
     * Method to pick the date
     * @param event
     * @param selectedDate Gets the selected date from the user
     */
    const onChangeDate = (event, selectedDate) => {
        setShowDato(Platform.OS === 'ios');
        setMonth(selectedDate.getMonth()+1)
        setTidspunkt(selectedDate.getFullYear().toString()+"-"+addZero(selectedDate.getMonth()+1)+"-"+addZero(selectedDate.getDate().toString()))
    };

    //When returned time is only one digit it helps to get the right format to store to the database
    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    /**
     * @this registerOppdrag Attempts to add a new commission
     */
    const registerOppdrag= ()=>{

        if (!isCityInNorway(fra_by.trim()) ||
            !isCityInNorway(til_by.trim()) ){
            Alert.alert("Feil i informasjon","Vennligst velg en by i Norge");
            return;
        }
        if (!isAddressInNorway(fra_adresse.trim()) ||
            !isAddressInNorway(til_adresse.trim()) ){
            Alert.alert("Feil i informasjon","Vennligst skriv inn en rikitg aresse i Norge");
            return;
        }

        if(fra_by.trim()==""||til_by==""){
            Alert.alert("Feil i informasjon","Vennligst velg avreiseby og ankomstby")
            return;
        }

        if(tidspunkt.trim()==""){
            Alert.alert("Feil i informasjon","Vennligst velg datoen du ønsker å sende pakken i")
            return;
        }
        if(pris.trim()==""){
            Alert.alert("Feil i informasjon", "Vennligst foreslå en pris")
            return;
        }
        if(sykkel_personbil==0&&varebil==0){
            Alert.alert("Feil i informasjon","Vennligst velg en transportmiddel")
            return;
        }
        const oppdragObject:Oppdrag={
            bruker_eier:bruker_eier,
            fra_by:fra_by,
            tidspunkt:tidspunkt,
            beskrivelse:beskrivelse,
            oppdrag_id:-1,
            sykkel_personbil:sykkel_personbil,
            til_by:til_by,
            varebil:varebil,
            fra_adresse:fra_adresse,
            bilde: bilde,
            pris: parseInt(pris),
            til_adresse: til_adresse
        }
        oppdragService.addOppdrag(oppdragObject,token).then(res=>{
            if(res.affectedRows>0){
                Alert.alert("Oppraget er registrert")
                handleFetch().then(()=>{
                    navigation.navigate("MineOppdrag")
                }).catch(error=>console.log(error))

            }else Alert.alert("En feil oppsto.."," vennligst ta kontakt med oss.")

        })

    }

    /**
     * Method to get a picture of the package to view in oppdragDetails
     */
    const pickFromGallery=async()=>{
        const{granted} =await Permissions.askAsync(Permissions.CAMERA_ROLL)
        if(granted){
            let data = await ImagePicker.launchImageLibraryAsync({
                mediaTypes:ImagePicker.MediaTypeOptions.Images,
                allowsEditing:true,
                aspect:[1,1],
                quality:0.1
            })
            if(!data.cancelled){
                await FileSystem.readAsStringAsync(data.uri, { encoding: 'base64' }).then(data1=>{
                    if(data1.length> MaxPicture){
                        Alert.alert("Bildet er for stort","Vennligst velg et mindre bilde..")
                    }else{
                        setBilde("data:image/png;base64,"+data1);
                        setLoading(false)
                    }

                });
            }
        }else{
            Alert.alert("Vi trenger å få tilgang til galleri for å fortsette..")
        }
    }

    const renderViewPagerPage = (data: any) => {
        return (
            <View key={data} style={styles.page}>
                <Text>{data}</Text>
            </View>
        );
    };



    const renderLabel = ({position, label, currentPosition,}: {
        position: number;
        stepStatus: string;
        label: string;
        currentPosition: number;
    }) => {
        return (
            <Text
                style={
                    position === currentPosition
                        ? styles.stepLabelSelected
                        : styles.stepLabel
                }
            >
                {label}
            </Text>
        );
    };

    /**
     * @this page1 is the first page in our stepper,
     * It contains info about from to address and city
     */
    function page1(){
        return(

            <ScrollView>
                <Text style={styles.title}>  Fra by: <Text style={{color:'red'}}>  *</Text></Text>
                <View style={{padding:3}}>
                    <TextInput
                        value={fra_by}
                        placeholder="Byen du skal sende pakken fra"
                        style={styles.texInputStyle}
                        onChangeText={text => setFra_by(text)}
                    />
                </View>

                <Text style={styles.title}>  Fra adresse: <Text style={{color:'red'}}>  *</Text></Text>
                <View>
                    <TextInput
                        value={fra_adresse}
                        placeholder="En gyldig adresse i Norge"
                        style={styles.texInputStyle}
                        onChangeText={text => setFra_adresse(text)}
                    />
                </View>
                <Text>{"\n"}</Text>

                <Text style={styles.title}>  Til by: <Text style={{color:'red'}}>  *</Text> </Text>
                <View style={{padding:4}}>
                    <TextInput
                        value={til_by}
                        placeholder="Byen du skal sende pakken til"
                        style={styles.texInputStyle}
                        onChangeText={text => setTil_by(text)}
                    />
                </View>

                <Text style={styles.title}> Til adresse: <Text style={{color:'red'}}>  *</Text></Text>
                <View>
                    <TextInput
                        value={til_adresse}
                        placeholder="En gyldig adresse i Norge"
                        style={styles.texInputStyle}
                        onChangeText={text => setTil_adresse(text)}
                    />
                </View>


            </ScrollView>

        )
    }

    /**
     * @this page2 is the second page in our stepper,
     * It contains info about the means of transport suitable for transporting the package,
     * and suggested price the owner want to pay
     */
    function page2(){
        return(
            <ScrollView>
                <Text style={myStyles.title}>Velg transportmiddel som passer til pakken din: <Text style={{color:'red'}}>  *</Text></Text>
                <DropDownPicker
                    items={[
                        {label: 'Sykkel eller personbil', value: 1},
                        {label: 'Varebil', value: 0},
                    ]}
                    containerStyle={{height: 40}}
                    onChangeItem={item =>{
                        if(item.value==1){
                            setSykkel_personbil(1);
                            setVarebil(0);
                        }
                        else {
                            setVarebil(1);
                            setSykkel_personbil(0);
                        }
                    }}
                    style={styles.texInputStyle}
                />

                <View style={{ flexDirection: "row" , justifyContent: 'space-between', marginRight:50, marginTop:20  }}>
                    <Text style={{fontSize: 16, paddingTop: 18, paddingBottom: 10, fontWeight: "600"}} >Foreslå en pris: <Text style={{color:'red'}}>  *</Text></Text>
                    <TextInput
                        style={{ height:35, borderColor: '#ccc', borderWidth: 1, width:70,color:'gray', backgroundColor: '#FAF7F6',marginTop:12}}
                        onChangeText={text =>{setPris(text)}}
                        keyboardType={'numeric'}
                        accessibilityLabel={"number"}
                        value={ pris}
                    />
                    <Text style={{fontSize: 16, fontWeight: "600", paddingTop: 18, paddingBottom: 10}} >,- NOK</Text>
                </View>
                <Text style={{fontSize: 11, fontWeight: "300", paddingTop: 3, paddingBottom: 10, color:'grey'}} >* prisen kan endres etter avtale med pendler</Text>
            </ScrollView>
        )
    }

    /**
     * @this page3 is the third page in our stepper,
     * It contains info about wished transport date,
     * and more description of the package (picture and text)
     */
    function page3(){
        return(
            <ScrollView>
                <Text style={myStyles.title}>Ønsket transport dato:</Text>
                <View>
                    <Button onPress={() =>{setShowDato(true)}} title="Velg en dato" />
                </View>
                {showDato && (<DateTimePicker
                    value={dato}

                    mode={'date'}
                    display="default"
                    onChange={onChangeDate}
                    style={styles.datePicker}
                />)}

                <Text style={myStyles.title}>Legg til et bilde:</Text>
                <View>

                    <Button title="Velg et bilde fra galleri"  onPress={()=>pickFromGallery()}>Hent bilde fra telefonens galleri</Button>

                </View>

                <Text style={myStyles.title}>Ønsker du å legge til mer informasjon:</Text>
                <TextInput
                    value={beskrivelse}
                    placeholder="Mer informasjon.."
                    editable
                    multiline
                    style={{borderWidth:1, borderColor:'#808080', borderRadius:5,  color:'#808080',paddingLeft:5, padding:25}}
                    onChangeText={text => setbeskrivelse(text)}
                />
            </ScrollView>
        )
    }

    /**
     * @this page4  Contains a brief summary of all pages before upload
     */
    function page4(){
        return(
            <ScrollView>
                {isCityInNorway(fra_by.trim())&&isCityInNorway(til_by.trim()) &&isAddressInNorway(fra_adresse.trim())&&isAddressInNorway(til_adresse.trim())
                    ?<Text style={myStyles.title}>
                        pakken din sendes {"\n"} fra:  <Text style={{color:'#5884c4'}}>{fra_by} " {fra_adresse} "</Text>
                        {"\n"} til: <Text style={{color:'#5884c4'}}>{til_by} " {til_adresse} "</Text>
                    </Text>
                    :<Text style={{color:"#cb4f93", fontSize:14}}>Vennligst sjekk om du har fylt informasjonen riktig.</Text>
                }

                {tidspunkt!==""
                    ?<Text style={myStyles.title}>Ønsket transport dato:  <Text style={{color:'#5884c4'}}>{tidspunkt}</Text></Text>
                    :null
                }

                {pris!==""
                    ?<Text style={myStyles.title}>Du foreslår å betale <Text style={{color:'#5884c4'}}>{pris}</Text> ,- for levering av pakken</Text>
                    :null
                }
                <View style={{flex:1}}>
                    {bilde!==""
                        ?<Image source={{uri: bilde }} style={{height:200, width:200, marginHorizontal:70, marginBottom:20}}/>
                        :null
                    }
                </View>


                {beskrivelse?<Text style={myStyles.title}>Du skrev følgende beskrivelse: <Text style={{color:'#5884c4'}}>{"\n"}{beskrivelse}</Text></Text>:null}

                <View style={{flex:1, position: 'relative'}}>
                    <TouchableOpacity
                        onPress={() => registerOppdrag()}
                        style={styles.buttonStyle}>
                        <Text
                            style={styles.ButtonText} >
                            Registrer oppdraget
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }


    return (
        <SafeAreaView style={myStyles.container} >
            <NavbarHeader/>
            <View style={styles.container}>
                <View style={{alignItems:"center", marginBottom:10}}>
                    <Text style={{ paddingVertical:5, textAlign:'center', fontWeight:"900", borderWidth:1 ,borderColor:'black', width:200, borderRadius:3, color:'#027e4a', fontSize:16}}>Legg til oppdrag</Text>
                </View>
                <View style={styles.stepIndicator}>
                    <StepIndicator
                        customStyles={firstIndicatorStyles}
                        currentPosition={currentPage}
                        stepCount={4}
                        labels={['Adresse', 'Pris og Transportmiddel', 'Informasjon', 'Sammendrag']}
                        renderLabel={renderLabel}
                        onPress={onStepPress}
                    />
                </View>
                {currentPage==0
                    ? (page1())
                    :currentPage==0
                }
                {currentPage==1
                    ?(page2())
                    :currentPage==0
                }
                {currentPage==2
                    ?(page3())
                    :currentPage==0
                }
                {currentPage==3
                    ?(page4())
                    :currentPage==0
                }

                <Swiper
                    showsButtons={true}
                    paginationStyle={{padding:10}}
                    style={{ flexGrow: 1 }}
                    loop={false}
                    index={currentPage}
                    autoplay={false}
                    onIndexChanged={(page) => {
                        setCurrentPage(page);
                    }}
                >
                    {PAGES.map((page) => renderViewPagerPage(page))}
                </Swiper>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    stepIndicator: {
        marginBottom:15
    },
    page: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    stepLabel: {
        fontSize: 11,
        textAlign: 'center',
        fontWeight: '500',
        color: '#999999',
    },
    stepLabelSelected: {
        fontSize: 11,
        textAlign: 'center',
        fontWeight: '500',
        color: '#4aae4f',
    },
    texInputStyle:{
        //inserted text style
        padding: 8,
        borderWidth: 1,
        borderColor: '#ccc',
        backgroundColor: '#FAF7F7',
    },
    itemDropDown:{
        //single dropdown item style
        padding: 5,
        marginTop: 2,
        backgroundColor: '#FAF9F8',
        borderColor: '#bbb',
        borderWidth: 1,
    },
    itemContainerDropDown:{
        //items container style you can pass maxHeight
        //to restrict the items dropdown height
        maxHeight: '600%',
    },
    datePicker:{
        flex:1,
        marginTop:2,
        paddingBottom:15,
        color:'gray',
        backgroundColor:'gray',
        fontSize: 16,
        fontWeight: 'normal'
    },
    buttonStyle:{
        width:200,
        marginLeft:70,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#3ecba7",
    },
    ButtonText:{
        fontSize: 15,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    title: {
        fontSize: 15,
        fontWeight: "300",
        paddingTop: 7
    },
    jaButton: {
        width:50,
        height:40,
        backgroundColor: "#a4d4a5",
        borderRadius: 10,
        paddingVertical: 10.5,
        paddingHorizontal: 14,
    },
    neiButton: {
        width:50,
        height:40,
        backgroundColor: "#ADD8E6",
        borderRadius: 10,
        paddingVertical: 10.5,
        paddingHorizontal: 14
    }
});

