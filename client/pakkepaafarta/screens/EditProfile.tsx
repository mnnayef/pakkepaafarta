
import * as React from 'react';
import {
    Alert, Image,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {useEffect, useState} from "react";
import jwt_decode from 'jwt-decode';
import 'localstorage-polyfill';
import * as SecureStore from 'expo-secure-store';
import {Bruker, brukerService} from "../services/brukerService";

import NavbarHeader from "../navigation/NavbarHeader";
import {useCallback} from "react";
import {useNavigation} from "@react-navigation/native";
import {Buffer} from "buffer";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import * as FileSystem from "expo-file-system";


/**
 *
 * @param props
 * @constructor
 * @screen EditProfile page allows the user to edit personal info,
 * such as profile picture, phone number...
 */
export default function EditProfile(props: {userData?: any;}) {
    const navigation = useNavigation();
    const [userData, setUserData] = useState<Bruker>();
    const [emailInput, setEmailInput] = useState("");
    const [phoneInput,setPhoneInput] = useState(0);

    const [bilde, setBilde] = useState('');
    const [loading, setLoading] = useState(true);
    const defaultImage= require("../assets/images/code.jpg")

    // Input format validations
    const emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const tlfFormat = /^(0047|\+47|47)?[2-9]\d{7}$/;
    const MaxPicture=221857;



    useEffect(() => {
        handleFetchCatFacts();
    }, []);


    const handleFetchCatFacts = useCallback(async () => {
        const authToken= await SecureStore.getItemAsync('x-auth-token');
        let token=jwt_decode(authToken)
        brukerService.read().then(data =>{
            brukerService.getUserAllInfoById(token.id).then(data => {
                if (data !== undefined && data[0] !== undefined) {
                    setUserData(data)

                    if(data[0].bilde!=null &&data[0].bilde!=""){
                        let bufferOriginal = Buffer.from(JSON.parse(JSON.stringify(data[0].bilde).toString()));
                        setBilde(bufferOriginal.toString('utf8'))
                    }
                    //(data[0])
                    setEmailInput(data[0].email)
                    setPhoneInput(data[0].mobile)

                }
            });
        })
    },[]);

    const pickFromGallery=async()=>{
        const{granted} =await Permissions.askAsync(Permissions.CAMERA_ROLL)
        if(granted){
            let data = await ImagePicker.launchImageLibraryAsync({
                mediaTypes:ImagePicker.MediaTypeOptions.Images,
                allowsEditing:true,
                aspect:[1,1],
                quality:0.1
            })
            if(!data.cancelled){
                await FileSystem.readAsStringAsync(data.uri, { encoding: 'base64' }).then(data1=>{
                    if(data1.length> MaxPicture){
                        Alert.alert("Bildet er for stort","Vennligst velg et mindre bilde..")
                    }else{
                        setBilde("data:image/png;base64,"+data1);
                        setLoading(true)
                    }
                });
            }
        }else{
            Alert.alert("Vi trenger å få tilgang til galleri for å fortsette..")
        }
    }

    /**
     * @this save attempts to save the changes of user info
     */
    const save = async () => {
        const authToken= await SecureStore.getItemAsync('x-auth-token');
        let token=jwt_decode(authToken)
        let brukerId = token.id;

        if (!emailInput.trim().match(emailFormat)){
            Alert.alert("Ugyldig e-postadresse","Prøv å skrive en gyldig e-postadresse på nytt");
            return;
        }
        if ( phoneInput !== undefined && !phoneInput.toString().match(tlfFormat)) {
            Alert.alert("Ugyldig Telefonnummer!", "Eksempel på riktig nummer er: 47185123");
            return;
        }
        // Update user info
        let res = await brukerService.updateUser(emailInput,+phoneInput,brukerId, bilde);

        if ((res && res.status === 401) || res instanceof Error) {

            Alert.alert("Noe feil skjedde, sjekk internett tilkoblingen");
        } else if (res) {
            navigation.navigate("Profile")
        }
    };

    return (
        <SafeAreaView style={styles.container}>

            <NavbarHeader/>
            <TouchableOpacity
                onPress={()=>pickFromGallery()}
            >
                <View style={styles.imageAndNameView}>
                    {bilde!==""
                        ?<Image source={{uri: bilde }} style={styles.profileImage}/>
                        :<Image source={defaultImage} style={styles.profileImage}/>
                    }
                </View>
                <Text style={{ color: "black", textAlign:"center", backgroundColor:"#e4f0f0"}}>Trykk på bildet for å redigere det</Text>
            </TouchableOpacity>


            <View style={styles.linksWrapper}>
                <View style={styles.linkView}>
                    <Text style={styles.linkStyling}>Navn:</Text>
                    {userData !== undefined
                        ? <TextInput
                            editable={false}
                            style={[styles.inputStyling, { color:"grey"} ]}
                            defaultValue={userData[0].navn}
                        > </TextInput>
                        : <TextInput style={{fontSize:20}}>No user</TextInput> }
                </View>

                <View style={styles.linkView}>
                    <Text style={styles.linkStyling}>E-post adresse:</Text>
                    {userData !== undefined
                        ? <TextInput
                            style={styles.inputStyling}
                            defaultValue={userData[0].email.toString()}
                            onChangeText={(text) => setEmailInput(text)}
                        > </TextInput>
                        : <TextInput style={{fontSize:20}}>No user</TextInput> }
                </View>

                <View style={styles.linkView}>
                    <Text style={styles.linkStyling}> Mobile:</Text>
                    {userData !== undefined
                        ? <TextInput
                            keyboardType={'numeric'}
                            style={styles.inputStyling}
                            defaultValue={userData[0].mobile.toString()}
                            onChangeText={(text1) => setPhoneInput(parseInt(text1))}
                        > </TextInput>
                        : <TextInput style={{fontSize:20}}>No user</TextInput> }
                </View>
                <View style={{alignItems:"center"}}>
                    <TouchableOpacity
                        onPress={save}
                        style={[styles.deleteButton,{backgroundColor: "#97d360",width: "40%"}]}>
                        <Text style={styles.linkStyling}>
                            Lagre
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    linksWrapper:{
        flex:3,
        backgroundColor:"#e4f0f0",
        marginLeft:5,
    },

    linkView:{
        height: 50,
        width: "95%",
        margin: 10,
        borderColor:"black",
        borderBottomWidth:3,
        overflow: "hidden",
        flexDirection : "column",
        justifyContent: "space-between",

    },
    linkStyling:{

        color: "black"
    },
    inputStyling:{
        fontSize:15,

    },
    deleteButton:{
        width:240,
        elevation: 4,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"red",
        top:20,
        alignItems: 'center',
        flexDirection : "row",
        justifyContent: "center"

    },
    imageAndNameView:{
        backgroundColor:"#e4f0f0",
        alignItems:"center",
        paddingTop: 4,
        overflow: "hidden",
        borderColor: "grey",
        borderStyle: "dotted"
    },
    profileImage:{
        width:110,
        height:110,
        borderRadius:45,
        overflow:"hidden",
        marginTop:10
    }
});

