
import {
    Alert,
    Button,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    View
} from 'react-native';
import  React, {useState}  from 'react';
import NavbarHeader from "../navigation/NavbarHeader";
import { brukerService} from "../services/brukerService";
import { useNavigation } from '@react-navigation/native';
import {emailService} from "../services/emailService";

/**
 *
 * @param props
 * @constructor
 * @screen ForgotPassword page is used to generate a new password,
 * and further send it by email to users to log in in case the password is forgotten
 */
export default function ForgotPassword(props: any) {
    const navigation = useNavigation();
    const [emailInput, setEmailInput] = useState("");
    const emailFormat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;


    // Check inputs and try to reset password
    const sendNewPassord = async (email: string) => {

        // Check if inputs are empty
        if (emailInput.trim() === "" || emailInput.trim() === undefined) {
            Alert.alert("e-postadressen er påkrevd for å kunne sende deg nytt passord");
            return;
        }
        if (!emailInput.trim().match(emailFormat)){
            Alert.alert("Ugyldig e-postadresse","Prøv å skrive en gyldig e-postadresse på nytt");
            return;
        }

        // Find user info by its mail
        let res = await brukerService.getUserAllInfoByEMail(emailInput);

        // Generating new password to login with it
        let newpass = emailService.generatePassword()
        // If find user try to send the generated password and set it in db
        if(res) {
            let mailtext = "Hei\nDu kan bruke passordet under for å logge deg inn:\n\n"+newpass+"\n\nMvh Pakke på farta"
            brukerService.setNewPassword(emailInput,newpass);
            emailService.sendEmail(emailInput.trim(),mailtext);
            navigation.navigate("Login")
            Alert.alert("Vennligst sjekk e-postadressen din","et generert passord ble sendt")
        } else {
            Alert.alert("Ugyldig e-postadressen","e-postadressen finnes ikke i systemet")
        }
    };


    return (
        <SafeAreaView style={mystyle.container}>
            <NavbarHeader />
            <View  style={{alignItems: "center"}}>
                <Text
                    style={{marginTop:50, fontSize:25,marginBottom:10}}>
                    E-post adresse
                </Text>

                <View
                    style={mystyle.action}>
                    <TextInput
                        value={emailInput}
                        textContentType={"emailAddress"}
                        accessibilityLabel={"E-post adresse"}
                        placeholder="skriv inn din aktiv e-postadresse.."
                        autoCompleteType={"email"}
                        style={mystyle.textInput}
                        onChangeText={text => setEmailInput(text)}
                    />
                </View>

                <View style={{marginTop:30,width:"60%"}}>
                    <Button
                        accessibilityLabel="Send generated password"
                        onPress={() => sendNewPassord(emailInput)}
                        title="Send e-post"
                        color={"#3ecba7"}
                    />
                </View>

            </View>
        </SafeAreaView>
    );
}

const mystyle = StyleSheet.create({
    container:{
        flex:0.6,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        margin:5
    },

    buttonsContainer:{
        flex:0.7,
        backgroundColor: "red",
        flexDirection: "column",
        alignItems: "center"
    },
    buttonStyle:{
        width:240,
        elevation: 4,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#3ecba7",
        top:20
    },
    ButtonText:{
        fontSize: 22,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    textInput: {
        width: "80%",
        marginTop:2,
        paddingBottom:10,
        color:'gray'
    },
    action: {
        flexDirection:'row',
        borderBottomWidth:0.3,
        borderBottomColor:'#222'
    },

});
