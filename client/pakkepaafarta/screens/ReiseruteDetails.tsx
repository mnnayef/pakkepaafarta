

import {
    Alert,
    Image,
    SafeAreaView, ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import React, { useEffect, useState} from "react";
import {Reiserute, reiseruteService} from "../services/reiseruteService";
import { brukerService} from "../services/brukerService";
import {useIsFocused, useNavigation} from "@react-navigation/native";
import Spinner from 'react-native-loading-spinner-overlay';
import jwt_decode from 'jwt-decode';

import {Entypo} from "@expo/vector-icons";
import styles from "../componentes/MyStyles";
import * as SecureStore from "expo-secure-store";
import {ChatRute, chatReiseruteService} from "../services/chatReiseruteService";
import {chatOppdragService} from "../services/chatOppdragService";
import {chat} from "./OppdragDetails";

let obj = {
    reiserute_id: -1,
    tidspunkt: "",
    fra_by: "",
    til_by: "",
    bruker_pendler: -1,
    varebil: -1,
    sykkel_personbil: -1,
    pris: -1,
    informasjon: "",
    stopp_by1 : "",
    stopp_by2 : "",
    stopp_by3 : "",
};


/**
 *
 * @param props
 * @param route
 * @constructor
 * @screen ReiseruteDetails used to show details of commission to all users of it,
 *
 * In this page the users can send a message to the commute (pendler),
 * to discuss picking up package for them.
 */
const ReiseruteDetails =({props,route}) => {

    const isFocused = useIsFocused();
    // Gets parameter from MineOppdragScreen
    const reiseruteId = parseInt(route.params.ReiseruteId);
    const[reiserute, setReiserute]= useState<Reiserute>(obj);
    const[pendlerNavn, setPendlerNavn]=useState("");
    const[pendlerVurdering, setPendlerVurdering]=useState(-1);
    const[pendlerTelefon, setPendlerTelefon]=useState(-1);
    const [chatData, setChatData] = useState<ChatRute>(chat);
    const [chat_id, setChat_id] = useState(-1);
    const [loading, setLoading] = useState(true);
    const navigation = useNavigation();


    useEffect(() => {
        setLoading(true);
        setReiserute(obj)
        handleFetch().then(()=>{})
    }, [props, isFocused]);

    const handleFetch = async () => {
        setLoading(true);

        await reiseruteService.getReiserute(reiseruteId).then(
            data => {
                if (data[0] === undefined) {
                    return;
                }
                if (data) {
                    if (data[0] !== undefined) {
                        setReiserute(data[0]);
                        brukerService.getUserInfoById(data[0].bruker_pendler).then(user=>{
                            setPendlerNavn(user[0].navn);
                            setPendlerVurdering(user[0].vurdering);
                            setPendlerTelefon(user[0].mobile)
                        });
                        setLoading(false)
                    }
                }
            }
        )
    };

    function navigateToLogin () {
        navigation.navigate("Login");
        setLoading(false);
        return;
    }

    /**
     * @this handleSendMeldingCklicked used to either create a new chat and navigate him to it,
     * or just navigate him to an existing chat related to this route if this a case
     */
    const handleSendMeldingCklicked = async  () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken) {
            Alert.alert(
                "Krever innlogging",
                "Vil du logge deg inn for å sende melding til pendleren?",
                [
                    {text: 'Ja', onPress: () => navigateToLogin()},
                    {text: 'Nei', onPress: () => navigation.navigate("Home"), style: 'cancel'},
                ],
                { cancelable: true }
            )
        }
        if(authToken) {
            let toki = jwt_decode(authToken);
            const chatObject:ChatRute={
                rute_id: reiserute.reiserute_id,
                bruker_1: toki.id,
                bruker_2: reiserute.bruker_pendler,
                status:0,
                bruker_updated_status:toki.id
            };
            if (toki.id > 0) {
                setLoading(true);
                chatOppdragService.getSpecificChat(-1,reiserute.reiserute_id, toki.id, reiserute.bruker_pendler,authToken).then( data => {
                    if (data === undefined) {
                        return;
                    }
                    if ( data) {
                        if(data[0] !== undefined) {
                            setChatData(data[0]);
                            navigation.navigate("Chat",{chatId:data[0].chat_id, pendler:reiserute.bruker_pendler})
                        }
                        else {
                            chatReiseruteService.addChat(chatObject,authToken).then( result=> {
                                if (result.affectedRows>0) {
                                    chatOppdragService.maxChatId(authToken).then(res => {
                                        if (res && res[0] !== undefined) {
                                            setChat_id(res[0].chat_id);
                                            navigation.navigate("Chat",{chatId:res[0].chat_id, pendler:reiserute.bruker_pendler})
                                        }
                                    })
                                }
                            })
                        }
                    }
                    setLoading(false)
                })
            }
        }
    };

    function RenderReiseruteDetails(){
        if (loading) {
            return (
                <Spinner
                    visible={loading}
                    textContent={'Loading...'}
                    textStyle={{color: '#FFF'}}
                />
            )
        }
        return (
            <SafeAreaView style={{backgroundColor:"white",flex:1}}>
                <View style={styleReiseruteDetails.image}>
                    <Image source={require("../assets/images/bckImage.jpg")}/>
                </View>

                <View style={styleReiseruteDetails.prisView}>
                    <Text style={styleReiseruteDetails.prisText}>   Foreslått pris er: </Text>
                    <Text style={[styleReiseruteDetails.prisText,{color:"#000000",left:30}]}> {reiserute.pris}</Text>
                    <Text style={[styleReiseruteDetails.prisText,{left:-25}]}> ,-KR</Text>
                </View>

                <View style={styleReiseruteDetails.addressContainer}>
                    <ScrollView>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            <Text style={{color:"#000000"}}> Fra: {reiserute.fra_by} </Text>
                            <Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by1!==""
                                ?<Text style={{color:"#000000"}}> {reiserute.stopp_by1} </Text>
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by1!==""
                                ?<Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by2!==""
                                ?<Text style={{color:"#000000"}}> {reiserute.stopp_by2} </Text>
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by2!==""
                                ?<Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by3!==""
                                ?<Text style={{color:"#000000"}}> {reiserute.stopp_by3} </Text>
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by3!==""
                                ?<Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            <Text> Til: {reiserute.til_by} </Text>
                        </View>
                    </ScrollView>
                    <View style={styleReiseruteDetails.pendlerWrapper}>
                        <Text style={{color:"#84d4d4"}}>Pendler:<Text style={{color:"black"}}> {"\n"+pendlerNavn}</Text></Text>
                        <Text style={{color:"#84d4d4"}}>Telefon:<Text style={{color:"black"}}> {"\n"+pendlerTelefon}</Text></Text>
                        {(pendlerVurdering)&&<Text style={{color:"#84d4d4"}}>Vurdering:<Text style={{color:"black"}}> {"\n"+pendlerVurdering}</Text></Text>}
                    </View>
                </View>

                <View style={styleReiseruteDetails.infoWrapper}>
                    <Text style={styleReiseruteDetails.infoTitle} > Beskrivelse</Text>
                    <Text style={styleReiseruteDetails.info}>{reiserute.informasjon}</Text>
                </View>

                <View style={styleReiseruteDetails.datoView}>
                    <Text style={{fontSize:15, color:"#84d4d4"}}>     Ønsket transport dato: </Text>

                </View>

                <View style={{ alignItems:"center"}}>
                    <TouchableOpacity
                        style={[styles.buttonStyle, {backgroundColor: "#5fd4c0",width: "35%"}]}
                        onPress={() => handleSendMeldingCklicked()}
                    >

                        <Text style={{fontSize:15,color:"#222222"}}>
                            Send melding
                        </Text>

                    </TouchableOpacity>
                </View>


            </SafeAreaView>
        )
    }
    return(
        <RenderReiseruteDetails/>

    )
};

const styleReiseruteDetails = StyleSheet.create({
    marginHeader:{
        marginBottom:2
    },
    image:{
        width:"100%",
        overflow:"hidden",
        flex: 0.25,
    },
    addressContainer:{
        flex:0.29,
        overflow:"hidden",
        flexDirection:"row"
    },
    addressWrapper:{
        flexDirection : "column",
        width:"60%"
    },
    pendlerWrapper:{
        paddingLeft:4,
        borderLeftColor:"#469b8c",
        flexDirection : "column",
        width:"40%"
    },
    adressView:{
        backgroundColor:"white",
        justifyContent: "space-between"
    },
    prisView:{
        backgroundColor: "white",
        flex: 0.08,
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        justifyContent: "space-between"
    },
    prisText:{
        fontSize:20,
        color: "#84d4d4",
        marginBottom:2
    },
    KR:{
        fontSize: 20,

    },
    icon:{
        fontSize:40,
        color:"#84d4d4",
        left:20
    },
    infoWrapper:{
        backgroundColor: "white",
        flex: 0.24,
    },
    info:{
        fontSize:15,
        color: "#808080",
        width:"90%",
        left:20,
        marginTop: 4,
        marginBottom:8

    },
    infoTitle:{
        fontSize:20,
        color:"#84d4d4",
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        width:"50%",
        left:15
    },
    datoView:{
        flex:0.05,
        marginTop:1,
        marginBottom: 1,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        width:"100%",
        justifyContent: "space-between"
    },
});

export default ReiseruteDetails;