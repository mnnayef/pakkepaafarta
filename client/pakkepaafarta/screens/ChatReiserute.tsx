
import React, { useState, useCallback, useEffect } from 'react'
import { GiftedChat, IMessage } from 'react-native-gifted-chat'
import {
    Alert,
    Platform,
    StatusBar,
    StyleSheet,
    View,
} from "react-native";
import NavbarHeader from "../navigation/NavbarHeader";
import {meldingService,Melding} from "../services/meldingService";
import Spinner from 'react-native-loading-spinner-overlay';
import * as SecureStore from "expo-secure-store";
import jwt_decode from 'jwt-decode';
import {useIsFocused ,useFocusEffect  } from "@react-navigation/native";
import {chatOppdragService} from "../services/chatOppdragService";


/**
 *
 * @param route
 * @constructor
 * @screen ChatScreen page to allow users to communicate with each other,
 */
export default function ChatScreen({route}) {
    const [messages, setMessages] = useState<IMessage[]>([]);
    const [user_id, setUser_id] = useState(0);
    const [token, setToken] = useState("");
    const [loading, setLoading] = useState(true);
    const [loadingUpdate, setLoadingUpdate] = useState(true);

    let interval;

    /**
     * @this useFocusEffect used to run get all messages on focus,
     * and clean it up when the screen becomes unfocused
     */
    useFocusEffect(
        useCallback(() => {
            fetchMessages()
            // Do something when the screen is focused
            return () => {
                clearInterval(interval)
                setMessages([])
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, [route])
    )

    /**
     * @this fetchMessages used to get all messages given a chat id
     */
    const fetchMessages = async () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');

        if(!authToken){
            setUser_id(0);
            return
        }
        let toki = jwt_decode(authToken)
        setToken(authToken)
        setUser_id(toki.id)
        interval= setInterval(() => {

            setLoadingUpdate(true)
            meldingService.getAllMeldingForChat(parseInt(route.params.chatId),authToken).then(data => {
                setLoadingUpdate(true)

                if( data[0] === undefined) {
                    setLoading(false)
                    return
                }

                if (data) {
                    if(data.length==0){
                        setMessages([])
                    }
                    for(let i=0;i<data.length;i++){
                        messages[i]= {text:"", user:{_id:-1}, createdAt:-1,_id:-1};
                    }
                    for(let i=0;i<data.length;i++){
                        messages[i]._id=data[i].mld_id
                        messages[i].user._id=data[i].sender_id
                        messages[i].createdAt=data[i].sendt_at
                        messages[i].text=data[i].mld
                    }
                    setLoading(false)
                    setLoadingUpdate(false)
                }
            })
        }, 1000);  //runs every 100 m second
    };

    /**
     * @this onSend used to send a message to db to view it in chat screen
     * @param tex Identifies the message of interest
     */
    const onSend =  async  (tex) => {
        let meldingObj:Melding = {
            mld_id: -1,
            chat_nr: parseInt(route.params.chatId),
            sender_id: tex[0].user._id,
            mld: tex[0].text,
            status:0
        }
        meldingService.addMessage(meldingObj,token).then(result => {
            if (result.affectedRows<=0) {
                Alert.alert("Nettverksfeil", "Kunne ikkke sende meldingen.")
            }else{
                chatOppdragService.updateStatus(parseInt(route.params.chatId), tex[0].user._id,0, token)
            }
        }).catch(error=>Alert.alert(error))
    }

    /**
     * Spinner loading while data in being fetching
     */
    if(loading){
        return (
            <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                //Text with the Spinner
                textContent={'Loading...'}
                textStyle={chatRieseruteStyle.spinnerTextStyle}
            />
        )
    }

    /**
     * GiftedChat is UI for chat implemented from react-native-gifted-chat,
     */
    return (
        <View style={chatRieseruteStyle.container}>
            <NavbarHeader/>
            <GiftedChat
                messages={messages}
                user={{
                    _id: user_id,
                }}
                onSend={text=>onSend(text)}
            />
        </View>


    )
}
const chatRieseruteStyle = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    textInput: {
        height:30,
        borderBottomWidth:0.6,
        borderTopWidth:0.6,
        backgroundColor:"white",
        width:"85%"
    },
    spinnerTextStyle: {
        color: '#FFF',
    }
});

