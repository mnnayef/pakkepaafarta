
import React from 'react';
import {
    ImageBackground,
    View,
    Image,
    Text,
    SafeAreaView,
    TouchableOpacity,
} from "react-native";
import styles from '../componentes/MyStyles'
import { useNavigation } from '@react-navigation/native';
import NavbarHeader from "../navigation/NavbarHeader";

/**
 *
 * @constructor
 * @screen WelcomeScreen is the landing screen in our application
 *
 * From this screen we navigate the user easily to many functions/screens such as:
 * 1. Login/ myProfile "depends if user already logged in or not"
 * 2. Add commission (oppdrag) / route (reiserute)
 * 3. Menu page which contains more quick navigation links
 */
const WelcomeScreen =( ) => {
    const navigation = useNavigation();

    return (
        <SafeAreaView style={styles.container}>

            <NavbarHeader/>

            <View style={styles.textView}>
                <Image
                    style={styles.tittlePakkePaaFarta}
                    source={require("../assets/images/textLogo2.png")}/>
            </View>

            <ImageBackground
                resizeMode={"contain"}
                style={styles.imageView}
                source={require("../assets/images/bckImage.jpg")}
            />

            <View style={styles.buttonsContainer}>

                <TouchableOpacity
                    style={styles.buttonStyle}
                    onPress={() => navigation.navigate('FindPendler')}
                >
                    <Text style={styles.ButtonText}>
                        send en pakke
                    </Text>

                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.buttonStyle, {backgroundColor: "#4ec1c1", top:50}]}
                    onPress={() => navigation.navigate('FindOppdrag')}
                >

                    <Text style={styles.ButtonText}>
                        Bring en pakke
                    </Text>

                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.LoginButton, {backgroundColor: "#89c8c8", top:80}]}
                    onPress={() => navigation.navigate('Profile')}
                >

                    <Text style={styles.ButtonText}>
                        Logg inn
                    </Text>

                </TouchableOpacity>

            </View>

        </SafeAreaView>
    );
}


export default WelcomeScreen;