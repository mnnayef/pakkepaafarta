
import React, { useEffect, useState} from "react";
import {
    Platform,
    ScrollView,
    StatusBar,
    StyleSheet,
    View
} from 'react-native';
import NavbarHeader from "../navigation/NavbarHeader";
import jwt_decode from 'jwt-decode';
import * as SecureStore from "expo-secure-store";
import {Oppdrag, oppdragService} from "../services/oppdragService";
import { useIsFocused} from "@react-navigation/native";
import Spinner from 'react-native-loading-spinner-overlay';
import {ListItem, Avatar} from 'react-native-elements'
import TouchableScale from 'react-native-touchable-scale';
import {Buffer} from "buffer";
import DataNotFound from "../componentes/dataNotFound";


/**
 *
 * @param props
 * @constructor
 * @screen MineOppdrag used to show list of all commissions the user has,
 * We use ListItem, Avatar from react-native-elements to render this list of commissions
 *
 * The user cal also see a full info about a commission by clicking on one of the items
 */
const MineOppdrag =(props) => {
    const isFocused = useIsFocused();
    const [oppdragList, setOppdragList] = useState<Oppdrag[]>([]);
    const [loading, setLoading] = useState(true);
    const {navigate} = props.navigation;

    useEffect(() => {
        setLoading(true);
        getUserOppdrag().then(()=>{
        }).catch(error=>console.log(error))
    }, [props, isFocused]);

    const getUserOppdrag = async () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        let token = jwt_decode(authToken);
        setLoading(true);
        oppdragService.getOppdragForUser(parseInt(token.id)).then(async data => {
            if (data[0] === undefined){
                setOppdragList([]);
                setLoading(false);
                return;
            }
            if(data){
                setLoading(true);
                if(data[0].bruker_eier!==token.id){
                    setOppdragList([])
                }

                for(let i=0;i<data.length;i++){
                    if(data[i]!==undefined){
                        if(oppdragList[i]===undefined) {
                            oppdragList[i]=data[i];
                        }else
                        if(oppdragList[i].oppdrag_id!==data[i].oppdrag_id){
                            oppdragList[i]=data[i];
                        }

                        if(data[i].bilde!==null &&data[i].bilde!=="") {

                            let bufferOriginal = Buffer.from(JSON.parse(JSON.stringify(data[i].bilde).toString()));
                            oppdragList[i].bilde = bufferOriginal.toString('utf8')
                        }
                    }
                }
            }
            setLoading(false);
        });
    };


    function RenderOppdragList(){
        if (loading) {
            return (
                <Spinner
                    visible={loading}
                    textContent={'Loading...'}
                    textStyle={mineOppdragStyles.spinnerTextStyle}
                />
            )
        }
        if( oppdragList.length<1) {
            return <DataNotFound/>
        }
        return (
            <ScrollView style={mineOppdragStyles.container}>
                <View style={mineOppdragStyles.marginHeader}>
                    <NavbarHeader/>
                </View>

                {
                    oppdragList.map((item, i) => (
                        <ListItem
                            // sending oppdrag id parameter with navigate to render details screen
                            onPress={ () => navigate('MineOppdragDetails',{minOppdragID:item.oppdrag_id})}
                            Component={TouchableScale}
                            key={i}
                            bottomDivider={true}
                            style={mineOppdragStyles.itemStyling}>
                            {(item.bilde.length>0 )
                                ?<Avatar source={{uri: item.bilde}} size="large"/>
                                :<Avatar source={require("../assets/images/bckImage.jpg")} size="large"/>
                            }

                            <ListItem.Content >
                                <ListItem.Title >{item.fra_by} {"--------------->"} {item.til_by}</ListItem.Title>
                                <ListItem.Subtitle style={{color:"#5fd4c0"}}>Pris: {item.pris}  ,-KR</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>
                    ))
                }
            </ScrollView>
        )
    }

    return(
        <RenderOppdragList/>
    )
};


const mineOppdragStyles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        paddingBottom: 50
    },
    marginHeader:{
        marginBottom:10
    },
    textINput:{
        margin:40
    },
    spinnerTextStyle: {
        color: '#FFF',
    },
    itemStyling:{
        paddingTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    }
});

export default MineOppdrag;