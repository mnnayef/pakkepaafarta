

import React, {useCallback, useEffect, useState} from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    SafeAreaView,
    Platform,
    StatusBar,
    Alert,
    TouchableOpacity,
} from "react-native";
import jwt_decode from 'jwt-decode';
import 'localstorage-polyfill';
import * as SecureStore from 'expo-secure-store';
import {Bruker} from "../services/brukerService";
import NavbarHeader from "../navigation/NavbarHeader";
import { useIsFocused} from '@react-navigation/native';
import {brukerService} from "../services/brukerService";
import { Buffer } from 'buffer'
import {AntDesign} from "@expo/vector-icons";
import Spinner from 'react-native-loading-spinner-overlay';


/**
 *
 * @param props
 * @constructor
 * @screen MyProfile used to render all info connected to the user
 *
 * In this page the user can do following functions:
 * 1. Edit the profile ( pic, email...etc)
 * 2. Quick navigate to pages such as ( my chats,commissions...etc)
 * 3. Log out

 */
const MyProfile =(props: { navigation: any; }) => {
    const { navigation } = props;
    const [userData, setUserData] = useState<Bruker>();
    const [bilde, setBilde] = useState('');

    const [loading, setLoading] = useState(true);
    const isFocused = useIsFocused();
    const defaultImage= require("../assets/images/code.jpg");


    useEffect(() => {
        setLoading(true);
        handleFetch().then(()=>{
        }).catch(error=>console.log(error))
    }, [props, isFocused]);

    function logout() {
        const authToken= SecureStore.getItemAsync('x-auth-token');

        if(authToken){
            SecureStore.deleteItemAsync("x-auth-token")
                .then(() => {
                    setUserData(undefined);
                    setBilde('');
                    navigation.navigate('Login')
                }).catch((error) => {
                Alert.alert("SecureStore: An error occurred while deleting the item...", error);
                });
        }else navigation.navigate('Login');
    }

    const handleFetch = useCallback(async () => {

        const authToken= await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate('Login');
            setLoading(false);
            return;
        }
        let token=jwt_decode(authToken);
        brukerService.read().then(() => {
            brukerService.getUserAllInfoById(token.id).then(data => {
                setLoading(true);
                if (data !== undefined && data[0] !== undefined) {
                    setUserData(data);
                    if(data[0].bilde!=null &&data[0].bilde!=""){
                        let bufferOriginal = Buffer.from(JSON.parse(JSON.stringify(data[0].bilde).toString()));
                        setBilde(bufferOriginal.toString('utf8'))
                    }
                }
            }).then(()=>setLoading(false)).catch(error=>console.log(error))
        })
    },[]);

    function Greeting() {
        if (loading) {
            return (
                <Spinner
                    visible={loading}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
            )
        }


        return (
            <SafeAreaView style={styles.container}>

                <NavbarHeader />

                <View style={styles.imageAndNameView}>
                    {bilde!==""
                        ?<Image source={{uri: bilde }} style={styles.profileImage}/>
                        :<Image source={defaultImage} style={styles.profileImage}/>
                    }


                    {userData !== undefined
                        ? <Text style={{fontSize:20, fontWeight:"800", paddingVertical:10, color:"#03235b"}}>{userData[0].navn}</Text>
                        : <Text style={{fontSize:20}}>No user</Text> }

                </View>

                <View style={styles.linksWrapper}>

                    <TouchableOpacity
                        style={styles.linkView}
                        onPress={() => navigation.navigate('EditProfile')}
                    >
                        <Text style={styles.linkStyling}>
                            Rediger profil
                        </Text>
                        <AntDesign name="right" size={24} />
                    </TouchableOpacity>


                    <TouchableOpacity
                        style={styles.linkView}
                        onPress={() => navigation.navigate('MineReiseruter')}
                    >
                        <Text style={styles.linkStyling}>
                            Mine reiseruter
                        </Text>
                        <AntDesign name="right" size={24} />
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.linkView}
                        onPress={() => navigation.navigate('MineOppdrag')}
                    >
                        <Text style={styles.linkStyling}>
                            Mine oppdrag
                        </Text>
                        <AntDesign name="right" size={24} />
                    </TouchableOpacity>
                    <View style={{alignItems: "center"}}>
                        <TouchableOpacity style={styles.logOutButton}  onPress={logout}>
                            <Text style={styles.linkStyling}>
                                Logg ut
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems: "center"}}>
                        <TouchableOpacity style={styles.deleteButton}>
                            <Text style={styles.linkStyling}>
                                Slett konto
                            </Text>
                        </TouchableOpacity>
                    </View>

                </View>



            </SafeAreaView>
        )

    }

    return (
        <Greeting/>
    );
};

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    spinnerTextStyle: {
        color: '#FFF',
    },
    imageAndNameView:{

        backgroundColor:"#e4f0f0",
        alignItems:"center",
        paddingTop: 4,
        overflow: "hidden",
        borderBottomWidth: 1,
        borderColor: "grey",
        borderStyle: "dotted"
    },
    linksWrapper:{
        flex:3,
        backgroundColor:"#e4f0f0",
    },
    profileImage:{
        width:110,
        height:110,
        borderRadius:100,
        overflow:"hidden",
        marginTop:10
    },
    linkView:{
        height: 40,
        width: "95%",
        margin: 10,
        marginTop: 30,
        borderColor:"black",
        borderBottomWidth:1,
        overflow: "hidden",
        flexDirection : "row",
        justifyContent: "space-between",
    }
    ,
    linkStyling:{
        fontSize:20,
        color: "#0c0d0c"
    },
    deleteButton:{
        width:240,
        elevation: 20,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#e34c4c",
        top:50,
        alignItems: 'center',
        flexDirection : "row",
        justifyContent: "center"

    },
    logOutButton:{
        width:240,
        elevation: 10,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#3bb4b6",
        top:30,
        marginBottom:30,
        alignItems: 'center',
        flexDirection : "row",
        justifyContent: "center"

    }
});


export default MyProfile;