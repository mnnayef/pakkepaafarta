
import * as React from 'react';
import {
    StyleSheet,
    View,
    Text,
    SafeAreaView,
    Platform,
    StatusBar,
    TouchableOpacity,
    ScrollView, TextInput, Alert, Button
} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import Swiper from 'react-native-swiper';
import NavbarHeader from "../navigation/NavbarHeader";
import myStyles from "../componentes/MyStyles";
import {useIsFocused, useNavigation, useFocusEffect} from "@react-navigation/native";
import {useCallback, useEffect, useState} from "react";
import * as SecureStore from "expo-secure-store";
const citiesData = require('./no.json');
import {Reiserute, reiseruteService} from '../services/reiseruteService'
import jwt_decode from 'jwt-decode';
import DropDownPicker from "react-native-dropdown-picker";
import DateTimePicker from "@react-native-community/datetimepicker";
const PAGES = ['Side 1', 'Side 2', 'Side 3','Side 4'];

const firstIndicatorStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 4,
    separatorFinishedColor: '#4aae4f',
    separatorUnFinishedColor: '#a4d4a5',
    stepIndicatorFinishedColor: '#4aae4f',
    stepIndicatorUnFinishedColor: '#a4d4a5',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 15,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
    labelColor: '#666666',
    labelSize: 12,
    currentStepLabelColor: '#4aae4f',
};

/**
 * @param props
 * @param route
 * @constructor
 * @screen EditReiserute page allows the user to edit an existing route in database
 */
export default function EditReiserute({props,route}) {
    const reiserute_id = parseInt(route.params.minReiseruteEdit);
    const [currentPage, setCurrentPage] = React.useState<number>(0);
    const [loading, setLoading] = useState(false);
    const [showMellomByer, setShowMellomByer] = useState(false);
    const [showValgMellomByer, setShowValgMellomByer] = useState(true);
    const navigation = useNavigation();
    const [showDato, setShowDato] = useState(false);
    const [showTid, setShowTid] = useState(false);
    const[month,setMonth]=useState(0);
    const[dato,setDato]=useState(new Date());
    const[tid,setTid]=useState(new Date());

    //Reiserute infor
    const [bruker_pendler, setBruker_pendler] = useState(0);
    const [reiseRuteID, setReiseruteID] = useState(parseInt(route.params.minReiseruteEdit));
    const [fra_by, setFra_by] = useState('');
    const [mellom_by1, setMellom_by1] = useState('');
    const [mellom_by2, setMellom_by2] = useState('');
    const [mellom_by3, setMellom_by3] = useState('');
    const [til_by, setTil_by] = useState("");
    const [tidspunkt, setTidspunkt] = useState("");
    const [varebil, setVarebil] = useState(0);
    const [sykkel_personbil, setSykkel_personbil] = useState(0);
    const [pris, setPris] = useState("");
    const [token, setToken] = useState("");
    const [beskrivelse,setbeskrivelse] = useState('');

    var items = [
        {
            id: 1,
            name: 'Oslo',
        }
    ]
    const fillArrays = () => {
        for(let i=0; i<citiesData.length;i++){
            let city= {} as typeof items[0];
            city.id=i ;
            city.name= citiesData[i].city;
            items[i]=city;
        }
        return items;
    }
    fillArrays()

    useEffect(() => {
        setLoading(true)
        handleFetch().then(()=>{}).catch(error=>console.log(error))
    },[useIsFocused()]);


    /**
     * @this handleFetch used to get reiserute-object to be edited
     */
    const handleFetch = useCallback(async () => {
        const authToken= await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate('Login')
            setLoading(false)
            return
        }
        setToken(authToken)
        let toki=jwt_decode(authToken)
        setBruker_pendler(toki.id)
        await reiseruteService.getReiserute(reiserute_id).then(
            data => {
                if (data[0] === undefined) {
                    return;
                }
                if (data) {
                    if (data[0] !== undefined) {
                        onChangeDateEdit(data[0].tidspunkt)
                        setReiseruteID(data[0].reiserute_id)
                        setFra_by(data[0].fra_by)
                        setTil_by(data[0].til_by)
                        setMellom_by1(data[0].stopp_by1)
                        setMellom_by2(data[0].stopp_by2)
                        setMellom_by3(data[0].stopp_by3)
                        setPris(data[0].pris)
                        setbeskrivelse(data[0].informasjon)
                        setSykkel_personbil(data[0].sykkel_personbil)
                        setVarebil(data[0].varebil)
                        setLoading(false)
                    }
                }
            }
        )
        return () => {

        };

    },[route]);

    const onChangeDateEdit = ( selectedDate) => {
        setTidspunkt(selectedDate.split("-")[0].toString()+"-"+selectedDate.split("-")[1].toString()+"-"+selectedDate.split("-")[2].toString().split("T")[0])
    };
    //Stepper
    const onStepPress = (position: number) => {
        setCurrentPage(position);
    };

    /**
     * A method to check weather the city is in norway or not
     * @param city: Identifies the city of interest
     */
    function isCityInNorway(city:string){
        for(let i=0;i<items.length;i++){
            if(city.toUpperCase()==items[i].name.toString().toUpperCase()) return true
        }
        return false
    }

    /**
     * Method to pick the new date of the route
     * @param event
     * @param selectedDate Gets the selected date from the user
     */
    const onChangeDate = (event, selectedDate) => {
        setShowDato(Platform.OS === 'ios');
        setMonth(selectedDate.getMonth())
        setTidspunkt(selectedDate.getFullYear().toString()+"-"+addZero(selectedDate.getMonth()+1)+"-"+addZero(selectedDate.getDate().toString())+" "+addZero(tid.getHours().toString()) +":"+addZero(tid.getUTCMinutes().toString())+":00")
    };

    //When returned time is only one digit it helps to get the right format to store to the database
    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    const onChangeTime = (event, selectedTime) => {
        setShowTid(Platform.OS === 'ios');

        //setMonth(dato.getMonth()+1)
        setTidspunkt(dato.getFullYear().toString()+"-"+addZero(month+1)+"-"+addZero(dato.getDate().toString())+" "+addZero(selectedTime.getHours().toString()) +":"+addZero(selectedTime.getUTCMinutes().toString())+":00")
    };
    const registerReiserute= ()=>{
        if (!isCityInNorway(fra_by) ||
            !isCityInNorway(til_by) ||
            (mellom_by3!="" &&! isCityInNorway(mellom_by3))||
            (mellom_by1!="" &&! isCityInNorway(mellom_by1))||
            (mellom_by2!="" &&! isCityInNorway(mellom_by2))){
            Alert.alert("Vennligst velg en by i Norge");
            return;
        }
        if(fra_by.trim()==""||til_by==""){
            Alert.alert("Vennligst velg avreiseby og ankomstby")
            return;
        }
        if(sykkel_personbil==0&&varebil==0){
            Alert.alert("Vennligst velg en transportmiddel")
            return;
        }
        const reiseruteObject:Reiserute={
            bruker_pendler:bruker_pendler,
            fra_by:fra_by,
            tidspunkt:tidspunkt,
            informasjon:beskrivelse,
            stopp_by1:mellom_by1,
            stopp_by2:mellom_by2,
            stopp_by3:mellom_by3,
            reiserute_id:reiseRuteID,
            pris:parseInt(pris),
            sykkel_personbil:sykkel_personbil,
            varebil:varebil,
            til_by:til_by
        }
        reiseruteService.updateReiserute(reiseruteObject,token).then(res=>{
            if(res.affectedRows>0){
                Alert.alert("Reiseruten din er redigert")
                handleFetch().then(()=>{
                    navigation.navigate("Home")
                }).catch(error=>console.log(error))

            }else Alert.alert("En feil oppsto..", "vennligst ta kontakt med oss..")

        })

    }

    const renderViewPagerPage = (data: any) => {
        return (
            <View key={data} style={styles.page}>
                <Text>{data}</Text>
            </View>
        );
    };



    const renderLabel = ({position, label, currentPosition,}: {
        position: number;
        stepStatus: string;
        label: string;
        currentPosition: number;
    }) => {
        return (
            <Text
                style={
                    position === currentPosition
                        ? styles.stepLabelSelected
                        : styles.stepLabel
                }
            >
                {label}
            </Text>
        );
    };
    const showMellomBy=()=> {
        if (!showMellomByer) return null;
        if(showMellomByer){
            return(
                <View>
                    <Text style={styles.title}>  Mellomby 1: <Text style={{color:'grey', fontSize:12 }}>    "ikke obligatorisk"</Text></Text>

                    <View style={{padding:4}}>
                        <TextInput
                            value={mellom_by1}
                            placeholder="Stopp 1.."
                            style={styles.texInputStyle}
                            onChangeText={text => setMellom_by1(text)}
                        />
                    </View>
                    <Text style={styles.title}>  Mellomby 2: <Text style={{color:'grey', fontSize:12 }}>    "ikke obligatorisk"</Text> </Text>
                    <View style={{padding:4}}>
                        <TextInput
                            value={mellom_by2}
                            placeholder="Stopp 2.."
                            style={styles.texInputStyle}
                            onChangeText={text => setMellom_by2(text)}
                        />
                    </View>
                    <Text style={styles.title}>  Mellomby 3: <Text style={{color:'grey', fontSize:12 }}>    "ikke obligatorisk"</Text> </Text>
                    <View style={{padding:4}}>
                        <TextInput
                            value={mellom_by3}
                            placeholder="Stopp 3.."
                            style={styles.texInputStyle}
                            onChangeText={text => setMellom_by3(text)}
                        />
                    </View>
                </View>

            )
        }
    }

    //the choice of showing between cities or not
    const showValgMellomBy=()=>{
        if(!showValgMellomByer) return null;

        if(showValgMellomByer){
            return(
                <View >
                    <Text style={{padding:3, paddingBottom:15, paddingTop:15}}>  Kommer du til å stoppe i flere byer mellom avreiseby og ankomstby?</Text>
                    <View style={{ flexDirection: "row" ,marginLeft: 40, justifyContent: 'space-between', marginRight:50  }}>
                        <TouchableOpacity style={styles.jaButton} onPress={()=>{setShowMellomByer(true); setShowValgMellomByer(false)}}><Text>Ja</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.neiButton} onPress={()=>setShowValgMellomByer(false)}><Text>Nei</Text></TouchableOpacity>
                    </View>

                </View>)
        }
    }


    function page1(){
        return(

            <ScrollView>
                <Text style={styles.title}>  Avreiseby: <Text style={{color:'red'}}>  *</Text></Text>
                <View style={{padding:3}}>
                    <TextInput
                        value={fra_by}
                        placeholder="Byen du begynner reisen din"
                        style={styles.texInputStyle}
                        onChangeText={text => setFra_by(text)}
                    />
                </View>

                <View>
                    {showValgMellomByer
                        ? (showValgMellomBy())
                        : null
                    }
                </View>
                <View>
                    {showMellomByer
                        ? (showMellomBy())
                        : null
                    }
                </View>


                <Text style={styles.title}>  Ankomstby: <Text style={{color:'red'}}>  *</Text> </Text>
                <View style={{padding:4}}>
                    <TextInput
                        value={til_by}
                        placeholder="Siste by du øsnker å stoppe i"
                        style={styles.texInputStyle}
                        onChangeText={text => setTil_by(text)}
                    />
                </View>

            </ScrollView>

        )
    }

    function page2(){
        return(
            <ScrollView>
                <Text style={myStyles.title}>transportmiddel: <Text style={{color:'red'}}>  *</Text></Text>
                <DropDownPicker
                    items={[
                        {label: 'Sykkel eller personbil', value: 1},
                        {label: 'Varebil', value: 0},
                    ]}
                    containerStyle={{height: 40}}
                    onChangeItem={item =>{
                        if(item.value==1){
                            setSykkel_personbil(1);
                            setVarebil(0);
                        }
                        else {
                            setVarebil(1);
                            setSykkel_personbil(0);
                        }
                    }}
                    style={styles.texInputStyle}
                    defaultValue={sykkel_personbil==0?0:1}
                />

                <View style={{ flexDirection: "row" , justifyContent: 'space-between', marginRight:50, marginTop:20  }}>
                    <Text style={{fontSize: 16, paddingTop: 18, paddingBottom: 10, fontWeight: "600"}} >Foreslå en pris:</Text>
                    <TextInput
                        style={{ height:35, borderColor: '#ccc', borderWidth: 1, width:70,color:'gray', backgroundColor: '#FAF7F6',marginTop:12}}
                        onChangeText={text =>{setPris(text)}}
                        keyboardType={'numeric'}
                        accessibilityLabel={"number"}
                        value={ pris.toString()}
                    />
                    <Text style={{fontSize: 16, fontWeight: "600", paddingTop: 18, paddingBottom: 10}} >,- NOK</Text>
                </View>
                <Text style={{fontSize: 11, fontWeight: "300", paddingTop: 3, paddingBottom: 10, color:'grey'}} >* prisen gjelder for hver mellomreiser (hvis de finnes)</Text>
            </ScrollView>
        )
    }

    //page 3 in swip window
    function page3(){
        return(
            <ScrollView>
                <Text style={myStyles.title}>Ønsket transport dato:</Text>
                <View>
                    <Button onPress={() =>{setShowDato(true)}} title="Velg en dato" />
                </View>
                {showDato && (<DateTimePicker
                    value={dato}

                    mode={'date'}
                    display="default"
                    onChange={onChangeDate}
                    style={styles.datePicker}
                />)}

                <Text style={myStyles.title}>Transport tid: <Text style={{color:'grey', fontSize:12 }}>    "ikke obligatorisk"</Text></Text>
                <View>
                    <Button onPress={() =>{setShowTid(true)}} title="Velg tid" />
                </View>
                {showTid && (<DateTimePicker
                    value={tid}
                    mode={'time'}
                    is24Hour={true}
                    display="default"
                    onChange={onChangeTime}
                    style={styles.datePicker}
                />)}

                <Text style={myStyles.title}>Ønsker du å legge til mer informasjon:</Text>
                <TextInput
                    value={beskrivelse}
                    placeholder="Mer informasjon.."
                    editable
                    multiline
                    style={{borderWidth:1, borderColor:'#808080', borderRadius:5,  color:'#808080',paddingLeft:5, padding:25}}
                    onChangeText={text => setbeskrivelse(text)}
                />
            </ScrollView>
        )
    }
    function page4(){
        return(
            <ScrollView>
                {isCityInNorway(fra_by)&&isCityInNorway(til_by)
                    ?<Text style={myStyles.title}>Ruten din er fra: <Text style={{color:'#5884c4'}}>{fra_by}</Text>  til <Text style={{color:'#5884c4'}}>{til_by}</Text></Text>
                    :<Text style={{color:"#cb4f93", fontSize:14}}>Vennligst sjekk om byene du skrev inn er byer i Norge</Text>
                }
                {mellom_by1
                    ? <Text style={myStyles.title}> Du har følgende stoppesteder:  <Text style={{color:'#4aae4f'}}>{mellom_by1} {mellom_by2?<Text style={{color:'#03bc70'}}>, {mellom_by2}</Text>:null} {mellom_by3?<Text style={{color:'#03bc70'}}>, {mellom_by3}</Text>:null}</Text></Text>
                    :null}

                {tidspunkt!==""
                    ?<Text style={myStyles.title}>Reisen din begynner:  <Text style={{color:'#5884c4'}}>{tidspunkt}</Text></Text>
                    :null
                }
                {pris!==""
                    ?<Text style={myStyles.title}>Du ønsker å få <Text style={{color:'#5884c4'}}>{pris}</Text> ,- per levering</Text>
                    :null
                }

                {beskrivelse?<Text style={myStyles.title}>Du skrev følgende beskrivelse: <Text style={{color:'#5884c4'}}>{"\n"}{beskrivelse}</Text></Text>:null}

                <View>
                    <TouchableOpacity
                        onPress={() => registerReiserute()}
                        style={styles.buttonStyle}>
                        <Text
                            style={styles.ButtonText} >
                            Rediger reiseruten
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }


    return (
        <SafeAreaView style={myStyles.container} >
            <NavbarHeader/>
            <View style={styles.container}>
                <View style={{alignItems:"center", marginBottom:10}}>
                    <Text style={{ paddingVertical:5, textAlign:'center', fontWeight:"900", borderWidth:1 ,borderColor:'black', width:200, borderRadius:3, color:'#027e4a', fontSize:16}}>Rediger reiserute</Text>
                </View>
                <View style={styles.stepIndicator}>
                    <StepIndicator
                        customStyles={firstIndicatorStyles}
                        currentPosition={currentPage}
                        stepCount={4}
                        labels={['Reiserute', 'Pris og Transportmiddel', 'Informasjon', 'Sammendrag']}
                        renderLabel={renderLabel}
                        onPress={onStepPress}
                    />
                </View>
                {currentPage==0
                    ? (page1())
                    :currentPage==0
                }
                {currentPage==1
                    ?(page2())
                    :currentPage==0
                }
                {currentPage==2
                    ?(page3())
                    :currentPage==0
                }
                {currentPage==3
                    ?(page4())
                    :currentPage==0
                }

                <Swiper
                    showsButtons={true}
                    paginationStyle={{padding:10}}
                    style={{ flexGrow: 1 }}
                    loop={false}
                    index={currentPage}
                    autoplay={false}
                    onIndexChanged={(page) => {
                        setCurrentPage(page);
                    }}
                >
                    {PAGES.map((page) => renderViewPagerPage(page))}
                </Swiper>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    stepIndicator: {
        marginBottom:15
    },
    page: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    stepLabel: {
        fontSize: 11,
        textAlign: 'center',
        fontWeight: '500',
        color: '#999999',
    },
    stepLabelSelected: {
        fontSize: 11,
        textAlign: 'center',
        fontWeight: '500',
        color: '#4aae4f',
    },
    texInputStyle:{
        //inserted text style
        padding: 8,
        borderWidth: 1,
        borderColor: '#ccc',
        backgroundColor: '#FAF7F7',
    },
    itemTextDropDown:{
        //text style of a single dropdown item
        color: '#222',
    },
    datePicker:{
        flex:1,
        marginTop:2,
        paddingBottom:15,
        color:'gray',
        backgroundColor:'gray',
        fontSize: 16,
        fontWeight: 'normal'
    },
    prisFelt:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },


    textView:{
        flex:3,
        backgroundColor:"white",
        alignItems:"center",
        paddingTop: 4
    },
    tittlePakkePaaFarta:{
        paddingTop:5,
        bottom:-13
    },
    imageView:{
        flex:2,
    },
    buttonsContainer:{
        flex:0.7,
        backgroundColor: "red",
        flexDirection: "column",
        alignItems: "center"
    },
    buttonStyle:{
        position: 'relative',
        width:200,
        marginLeft:70,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#3ecba7",
    },
    ButtonText:{
        fontSize: 15,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    title: {
        fontSize: 15,
        fontWeight: "300",
        paddingTop: 7
    },
    textInput: {
        flex:1,
        marginTop:2,
        paddingBottom:15,
        color:'gray'
    },
    jaButton: {
        width:50,
        height:40,
        backgroundColor: "#a4d4a5",
        borderRadius: 10,
        paddingVertical: 10.5,
        paddingHorizontal: 14,
    },
    neiButton: {
        width:60,
        height:40,
        backgroundColor: "#ADD8E6",
        borderRadius: 10,
        paddingVertical: 10.5,
        paddingHorizontal: 14
    }
});

