

import {
    Alert,
    Image, Platform,
    SafeAreaView, StatusBar, StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import React, { useEffect, useState} from "react";
import {Oppdrag, oppdragService} from "../services/oppdragService";
import {useIsFocused, useNavigation} from "@react-navigation/native";
import NavbarHeader from "../navigation/NavbarHeader";
import Spinner from 'react-native-loading-spinner-overlay';
import {Entypo} from "@expo/vector-icons";
import styles from "../componentes/MyStyles";
import {Buffer} from "buffer";
import jwt_decode from 'jwt-decode';
import * as SecureStore from "expo-secure-store";
import {chatOppdragService,ChatOppdrag} from "../services/chatOppdragService"

export let obj = {
    oppdrag_id: 0,
    fra_by: "",
    til_by: "",
    tidspunkt: "",
    bruker_eier: 0,
    varebil: 0,
    sykkel_personbil: 0,
    beskrivelse: "",
    fra_adresse: "",
    til_adresse: "",
    pris: 0,
    bilde: ""
};
export let chat = {
    chat_id: 0,
    oppdr_id: 0,
    rute_id:0,
    bruker_1: 0,
    bruker_2: 0,
    status:0,
    bruker_updated_status:0
};

/**
 *
 * @param props
 * @param route
 * @constructor
 * @screen OppdragDetails used to show details of commission to all users of it,
 *
 * In this page the users can send a message to the owner of the package to discuss picking it up.
 */
const OppdragDetails =({props,route}) => {
    const navigation = useNavigation();
    const isFocused = useIsFocused();
    const oppdragId = parseInt(route.params.OppdragID); // Gets parameter from MineOppdragScreen
    const[oppdrag, setOppdrag]= useState<Oppdrag>(obj);
    const [bilde, setBilde] = useState('');
    const defaultImage= require("../assets/images/bckImage.jpg");
    const [user_id, setUser_id] = useState(0);
    const [token, setToken] = useState("");
    const [loading, setLoading] = useState(true);
    const [chatData, setChatData] = useState<ChatOppdrag>(chat);
    const [chat_id, setChat_id] = useState(-1);



    useEffect(() => {
        setLoading(true);
        getUserOppdrag().then(()=>{})
    }, [props, isFocused]);

    function navigateToLogin () {
        navigation.navigate("Login");
        setLoading(false);
        return;
    }
    // Check if chat exists given rute id, bruker1 id and bruker2 id  navigate to it, if not create one
    const handleSendMeldingCklicked = async  () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken) {
            Alert.alert(
                "Krever innlogging",
                "Vil du logge deg inn for å sende melding til pakke eieren?",
                [
                    {text: 'Ja', onPress: () => navigateToLogin()},
                    {text: 'Nei', onPress: () => console.log(""), style: 'cancel'},
                ],
                { cancelable: true }
            )
        }
        if(authToken) {
            let toki = jwt_decode(authToken);
            setToken(authToken);
            setUser_id(toki.id);
            const chatObject:ChatOppdrag={
                chat_id: -1,
                oppdr_id: oppdrag.oppdrag_id,
                bruker_1: toki.id,
                bruker_2: oppdrag.bruker_eier,
                status:0,
                bruker_updated_status:toki.id
            };
            if (toki.id > 0) {
                setLoading(true);
                chatOppdragService.getSpecificChat(oppdragId,-1, toki.id, oppdrag.bruker_eier,authToken).then( data => {
                    if (data === undefined) {
                        return;
                    }
                    if ( data) {
                        if(data[0] !== undefined) {
                            setChatData(data[0]);
                            navigation.navigate("Chat",{chatId:data[0].chat_id, eier:oppdrag.bruker_eier})
                        }
                        else {
                            chatOppdragService.addChat(chatObject,token).then( result=> {
                                if (result.affectedRows>0) {
                                    chatOppdragService.maxChatId(token).then(res => {
                                        if (res && res[0] !== undefined) {
                                            setChat_id(res[0].chat_id);
                                            navigation.navigate("Chat",{chatId:res[0].chat_id, eier:oppdrag.bruker_eier})
                                        }
                                    })
                                }
                            })
                        }
                    }
                    setLoading(false)
                })
            }
        }
    };


    const getUserOppdrag = async () => {
        setBilde("");
        await oppdragService.getOppdrag(oppdragId).then(
            data => {
                if (data[0] === undefined) {
                    return;
                }
                if (data) {
                    if (data[0] !== undefined) {
                        setOppdrag(data[0]);
                        if(data[0].bilde!=null&&data[0].bilde!=""){
                            let bufferOriginal = Buffer.from(JSON.parse(JSON.stringify(data[0].bilde).toString()));
                            setBilde(bufferOriginal.toString('utf8'))
                        }

                        setLoading(false)
                    }
                }
            }
        )
    };


    function renderOppdrag(){
        if (loading) {
            return (
                <Spinner
                    visible={loading}
                    textContent={'Loading...'}
                    textStyle={styleOppdragDetail.spinnerTextStyle}
                />
            )
        }
        return(
            <View style={{backgroundColor:"white",flex:1}}>
                <View style={styleOppdragDetail.image}>
                    {(bilde!=="" &&bilde!==null)
                        ?<Image source={{uri:bilde}} style={{height:300,width:"100%"}}/>
                        :<Image source={defaultImage} style={{height:300,width:"100%"}}/>
                    }

                </View>

                <View style={styleOppdragDetail.prisView}>
                    <Text style={styleOppdragDetail.prisText}>   Foreslått pris er: </Text>
                    <Text style={[styleOppdragDetail.prisText,{color:"#000000",left:30}]}> {oppdrag.pris}</Text>
                    <Text style={[styleOppdragDetail.prisText,{left:-25}]}> ,-KR</Text>
                </View>

                <View style={styleOppdragDetail.addressContainer}>
                    <View style={styleOppdragDetail.addressWrapper}>
                        <Text style={{color:"#000000"}}>      Fra: {oppdrag.fra_by + "  / "  + oppdrag.fra_adresse} </Text>
                        <Entypo name="flow-line" style={styleOppdragDetail.icon} />
                    </View>

                    <View style={styleOppdragDetail.adressView}>
                        <Text>      Til: {oppdrag.til_by + "  / "  + oppdrag.til_adresse} </Text>
                    </View>
                </View>
                <View style={styleOppdragDetail.infoWrapper}>
                    <Text style={styleOppdragDetail.infoTitle} > Beskrivelse</Text>
                    <Text style={styleOppdragDetail.info}>{oppdrag.beskrivelse}</Text>
                </View>

                <View style={styleOppdragDetail.datoView}>
                    <Text style={{fontSize:15, color:"#84d4d4"}}>     Ønsket transport dato: </Text>
                    <Text style={{fontSize:15,left:-20}}>{oppdrag.tidspunkt.split("T")[0]+"  klokka: "+
                    oppdrag.tidspunkt.split("T")[1].split(":")[0]+oppdrag.tidspunkt.split("T")[1].split(":")[1]
                    }
                    </Text>
                </View>

                <View style={{ alignItems:"center"}}>
                    <TouchableOpacity
                        style={[styles.buttonStyle, {backgroundColor: "#5fd4c0",width: "35%"}]}
                        onPress={() => handleSendMeldingCklicked()}
                    >

                        <Text style={{fontSize:15,color:"#222222"}}>
                            Send melding
                        </Text>

                    </TouchableOpacity>
                </View>


            </View>
        )
    }
    return(
        <SafeAreaView style={styleOppdragDetail.container}>
            <View style={styleOppdragDetail.marginHeader}>
                <NavbarHeader/>
            </View>

            {renderOppdrag()}
        </SafeAreaView>
    )
};
const styleOppdragDetail = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,

    },
    marginHeader:{
        marginBottom:2
    },

    spinnerTextStyle: {
        color: '#FFF',
    },
    image:{
        width:"100%",
        overflow:"hidden",
        flex: 0.3,
    },
    addressContainer:{
        justifyContent: "center",
        flex:0.15,

    },
    addressWrapper:{
        flexDirection : "column",
        justifyContent: "space-between",

    },
    adressView:{
        backgroundColor:"white",
        flexDirection : "row",
        justifyContent: "space-between"
    },
    prisView:{
        backgroundColor: "white",
        flex: 0.08,
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        justifyContent: "space-between"
    },
    prisText:{
        fontSize:20,
        color: "#84d4d4",
        marginBottom:2
    },
    KR:{
        fontSize: 20,

    },
    icon:{
        fontSize:40,
        color:"#84d4d4",
        left:40
    },
    infoWrapper:{
        backgroundColor: "white",
        flex: 0.24,
    },
    info:{
        fontSize:15,
        color: "#808080",
        width:"90%",
        left:20,
        marginTop: 4,
        marginBottom:8

    },
    infoTitle:{
        fontSize:20,
        color:"#84d4d4",
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        width:"50%",
        left:15
    },
    datoView:{
        flex:0.05,
        marginTop:1,
        marginBottom: 1,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        width:"100%",
        justifyContent: "space-between"
    },
});
export default OppdragDetails;
