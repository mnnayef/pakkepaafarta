

import {
    Alert,
    Image,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import React, {useCallback, useEffect, useState} from "react";
import {Oppdrag, oppdragService} from "../services/oppdragService";
import {useIsFocused} from "@react-navigation/native";
import NavbarHeader from "../navigation/NavbarHeader";
import Spinner from 'react-native-loading-spinner-overlay';
import {Entypo} from "@expo/vector-icons";
import styles from "../componentes/MyStyles";
import {Buffer} from "buffer";
import jwt_decode from 'jwt-decode';
import * as SecureStore from "expo-secure-store";

import {obj} from "./OppdragDetails";

/**
 *
 * @param props
 * @param route
 * @param navigation
 * @constructor
 * @screen MineOppdragDetails used to show details of commission to the owner of it,
 *
 * In this page the user can do the following functions:
 * 1. Edit the commission
 * 2. Delete it from the database
 * 3. Update its status as invalid in case the user has agreed with someone to pick it up
 */
const MineOppdragDetails =({props,route,navigation}) => {
    const isFocused = useIsFocused();
    const oppdragId = parseInt(route.params.minOppdragID); // Gets parameter from MineOppdragScreen
    const[oppdrag, setOppdrag]= useState<Oppdrag>(obj)
    const [bilde, setBilde] = useState('');
    const[status, setStatus]=useState("");
    const [token, setToken] = useState("");
    const [loading, setLoading] = useState(true);

    /**
     * @this useEffect method used to get oppdrag-object given its id
     * The id is passed as a parameter through route navigation from screens,
     * that navigate to this screen
     */
    useEffect(() => {
        getUserOppdrag().then(()=>{
        })
        handleFetch()
    }, [props, isFocused]);

    const getUserOppdrag = async () => {
        setBilde("")
        await oppdragService.getOppdrag(oppdragId).then(
            data => {
                if (data[0] === undefined) {
                    return;
                }
                if (data) {
                    if (data[0] !== undefined) {
                        setOppdrag(data[0]);
                        setStatus(data[0].status)
                        if(data[0].bilde!=null&&data[0].bilde!=""){
                            let bufferOriginal = Buffer.from(JSON.parse(JSON.stringify(data[0].bilde).toString()));
                            setBilde(bufferOriginal.toString('utf8'))
                        }
                        setLoading(false)
                    }
                }
            }
        )
    }
    const updateStatus = async (status:string) => {
        setStatus(status)
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate("Login")
            return
        }
        oppdragService.updateStatus(oppdragId, status, authToken)
    }

    const handleFetch = useCallback(async () => {
        const authToken= await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate('Login')
            setLoading(false)
            return
        }
        setToken(authToken)
    },[]);

    const deleteOppdrag = async (oppdragNumber: number) => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken) {
            navigation.navigate("Login")
        }
        let token = jwt_decode(authToken)
        oppdragService.read().then( () => {
            oppdragService.deleteOppdrag(oppdragNumber).then(res => {
                if (res.affectedRows > 0) {
                    setLoading(false)
                    Alert.alert("Oppdraget ditt er slettet")
                    navigation.navigate("Profile")
                } else Alert.alert("En feil oppsto..", "vennligst prøv på nytt..")
            });
        })
    };
    function rencerOppdrag(){
        if (loading) {
            return (
                <Spinner
                    visible={loading}
                    textContent={'Loading...'}
                    textStyle={styleOppdragDetail.spinnerTextStyle}
                />
            )
        }
        return(
            <View style={{backgroundColor:"white",flex:1}}>
                <View style={styleOppdragDetail.image}>
                    {bilde!==""
                        ?<Image source={{uri:bilde}} style={{height:250,width:"100%"}}/>
                        :<Image source={require("../assets/images/code.jpg")}/>
                    }

                </View>

                <View style={styleOppdragDetail.prisView}>
                    <Text style={styleOppdragDetail.prisText}>   Foreslått pris er: </Text>
                    <Text style={[styleOppdragDetail.prisText,{color:"#000000",left:30}]}> {oppdrag.pris}</Text>
                    <Text style={[styleOppdragDetail.prisText,{left:-25}]}> ,-KR</Text>
                </View>

                <View style={styleOppdragDetail.addressContainer}>
                    <View style={styleOppdragDetail.addressWrapper}>
                        <Text style={{color:"#000000"}}>      Fra adresse: {oppdrag.fra_by + "  / "  + oppdrag.fra_adresse} </Text>
                        <Entypo name="flow-line" style={styleOppdragDetail.icon} />
                    </View>

                    <View style={styleOppdragDetail.adressView}>
                        <Text>      Til adresse: {oppdrag.til_by + "  / "  + oppdrag.til_adresse} </Text>
                    </View>
                </View>
                <View style={styleOppdragDetail.infoWrapper}>
                    <Text style={styleOppdragDetail.infoTitle} > Beskrivelse</Text>
                    <Text style={styleOppdragDetail.info}>{oppdrag.beskrivelse}</Text>
                </View>

                <View style={styleOppdragDetail.datoView}>
                    <Text style={{fontSize:15, color:"#84d4d4"}}>     Ønsket transport dato: </Text>
                    <Text style={{fontSize:15,left:-20}}>{oppdrag.tidspunkt.split("T")[0]}  {oppdrag.tidspunkt.split("T")[1].split(":")[0]}:{oppdrag.tidspunkt.split("T")[1].split(":")[1]}
                    </Text>
                </View>

                <View style={{flexDirection:"row",justifyContent: "space-between"}}>

                    <TouchableOpacity
                        style={[styles.buttonStyle, {backgroundColor: "#5fd4c0",width: "35%"}]}
                        onPress={() => navigation.navigate('EditOppdrag',{oppdragEdit:oppdrag.oppdrag_id})}
                    >

                        <Text style={{fontSize:15,color:"#222222"}}>
                            Rediger
                        </Text>

                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[styles.buttonStyle, {backgroundColor: "#db533e",width: "35%"}]}
                        onPress={() => Alert.alert(
                            "Slett oppdragg",
                            "Vil du slette oppdraget fra systemet??",
                            [
                                {text: 'Ja', onPress: () => deleteOppdrag(oppdragId)},
                                {text: 'Nei', onPress: () => console.log('No Pressed'), style: 'cancel'},
                            ],
                            { cancelable: false }
                        )}
                    >

                        <Text style={{fontSize:15,color:"#222222"}}>
                            Slett
                        </Text>

                    </TouchableOpacity>

                </View>
                {status=="gyldig"&& <View style={{ alignItems:"center"}}>
                    <TouchableOpacity
                        style={[styles.buttonStyle, {backgroundColor: "#d4b4b4",width: "40%", marginTop:15}]}
                        onPress={() => updateStatus("ugyldig")}
                    >

                        <Text style={{fontSize:15,color:"#222222"}}>
                            Utilgjengeliggjøre
                        </Text>

                    </TouchableOpacity>
                </View>}
                {status=="ugyldig"&& <View style={{ alignItems:"center"}}>
                    <TouchableOpacity
                        style={[styles.buttonStyle, {backgroundColor: "#b4e5c5",width: "40%", marginTop:15}]}
                        onPress={() => updateStatus("gyldig")}
                    >

                        <Text style={{fontSize:15,color:"#222222"}}>
                            Tilgjengeliggjøre
                        </Text>

                    </TouchableOpacity>
                </View>}


            </View>
        )
    }
    return(
        <SafeAreaView style={styleOppdragDetail.container}>
            <View style={styleOppdragDetail.marginHeader}>
                <NavbarHeader/>
            </View>

            {rencerOppdrag()}
        </SafeAreaView>
    )
};


export const styleOppdragDetail = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,

    },
    marginHeader:{
        marginBottom:2
    },

    spinnerTextStyle: {
        color: '#FFF',
    },
    image:{
        width:"100%",
        overflow:"hidden",
        flex: 0.3,
    },
    addressContainer:{
        justifyContent: "center",
        flex:0.15,

    },
    addressWrapper:{
        flexDirection : "column",
        justifyContent: "space-between",

    },
    adressView:{
        backgroundColor:"white",
        flexDirection : "row",
        justifyContent: "space-between"
    },
    prisView:{
        backgroundColor: "white",
        flex: 0.08,
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        justifyContent: "space-between"
    },
    prisText:{
        fontSize:20,
        color: "#84d4d4",
        marginBottom:2
    },
    KR:{
        fontSize: 20,

    },
    icon:{
        fontSize:40,
        color:"#84d4d4",
        left:40
    },
    infoWrapper:{
        backgroundColor: "white",
        flex: 0.24,
    },
    info:{
        fontSize:15,
        color: "#808080",
        width:"90%",
        left:20,
        marginTop: 4,
        marginBottom:8

    },
    infoTitle:{
        fontSize:20,
        color:"#84d4d4",
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        width:"50%",
        left:15
    },
    datoView:{
        flex:0.05,
        marginTop:1,
        marginBottom: 1,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        width:"100%",
        justifyContent: "space-between"
    },
});

export default MineOppdragDetails;