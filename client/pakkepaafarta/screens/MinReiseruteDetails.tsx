
import {
    Image,
    Platform,
    SafeAreaView, ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import React, { useEffect, useState} from "react";
import {Reiserute, reiseruteService} from "../services/reiseruteService";
import { brukerService} from "../services/brukerService";
import {useIsFocused, useNavigation} from "@react-navigation/native";
import Spinner from 'react-native-loading-spinner-overlay';
import { Entypo} from "@expo/vector-icons";
import styles from "../componentes/MyStyles";
import * as SecureStore from "expo-secure-store";

let obj = {
    reiserute_id: -1,
    tidspunkt: "",
    fra_by: "",
    til_by: "",
    bruker_pendler: -1,
    varebil: -1,
    sykkel_personbil: -1,
    pris: -1,
    informasjon: "",
    stopp_by1 : "",
    stopp_by2 : "",
    stopp_by3 : "",
};

/**
 *
 * @param props
 * @param route
 * @constructor
 * @screen MinReiseruteDetails used to show details of route to the user that own it,
 *
 * In this page the user can do the following functions:
 * 1. Edit the route
 * 2. Delete it from the database
 * 3. Update its status for example as invalid in case the user will not travel on that itinerary for a period of time
 */
const MinReiseruteDetails =({props,route}) => {

    const isFocused = useIsFocused();
    const reiseruteId = parseInt(route.params.minReiseruteId); // Gets parameter from MineOppdragScreen
    const[reiserute, setReiserute]= useState<Reiserute>(obj);
    const[pendlerNavn, setPendlerNavn]=useState("");
    const[status, setStatus]=useState("");
    const[pendlerVurdering, setPendlerVurdering]=useState(-1);
    const[pendlerTelefon, setPendlerTelefon]=useState(-1);
    const [loading, setLoading] = useState(true);
    const navigation = useNavigation();

    /**
     * @this useEffect method used to get reiserute-object given its id
     * The id is passed as a parameter through route navigation from screens,
     * that navigate to this screen
     */
    useEffect(() => {
        setLoading(true);
        setReiserute(obj);
        handleFetch().then(()=>{
        })
    }, [props, isFocused]);

    // Gets the route from database
    const handleFetch = async () => {
        setLoading(true);

        await reiseruteService.getReiserute(reiseruteId).then(
            data => {
                if (data[0] === undefined) {
                    return;
                }
                if (data) {
                    if (data[0] !== undefined) {
                        setReiserute(data[0]);
                        setStatus(data[0].status);
                        brukerService.getUserInfoById(data[0].bruker_pendler).then(user=>{
                            setPendlerNavn(user[0].navn);
                            setPendlerVurdering(user[0].vurdering);
                            setPendlerTelefon(user[0].mobile);
                        });
                        setLoading(false);
                    }
                }
            }
        )
    };

    const updateStatus = async (status:string) => {
        setStatus(status);
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            navigation.navigate("Login");
            return
        }
        await reiseruteService.updateStatus(reiseruteId, status, authToken);
    };

    function RenderReiseruteDetails(){
        if (loading) {
            return (
                <Spinner
                    //visibility of Overlay Loading Spinner
                    visible={loading}
                    //Text with the Spinner
                    textContent={'Loading...'}
                    //Text style of the Spinner Text
                    textStyle={{color: '#FFF'}}
                />
            )
        }
        return (
            <SafeAreaView style={{backgroundColor:"white",flex:1}}>
                <View style={styleReiseruteDetails.image}>
                    <Image source={require("../assets/images/bckImage.jpg")}/>
                </View>

                <View style={styleReiseruteDetails.prisView}>
                    <Text style={styleReiseruteDetails.prisText}>   Foreslått pris er: </Text>
                    <Text style={[styleReiseruteDetails.prisText,{color:"#000000",left:30}]}> {reiserute.pris}</Text>
                    <Text style={[styleReiseruteDetails.prisText,{left:-25}]}> ,-KR</Text>
                </View>

                <View style={styleReiseruteDetails.addressContainer}>
                    <ScrollView>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            <Text style={{color:"#000000"}}>      Fra: {reiserute.fra_by} </Text>
                            <Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by1!==""
                                ?<Text style={{color:"#000000"}}>     {reiserute.stopp_by1} </Text>
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by1!==""
                                ?<Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by2!==""
                                ?<Text style={{color:"#000000"}}>     {reiserute.stopp_by2} </Text>
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by2!==""
                                ?<Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by3!==""
                                ?<Text style={{color:"#000000"}}>     {reiserute.stopp_by3} </Text>
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.addressWrapper}>
                            {reiserute.stopp_by3!==""
                                ?<Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                                :null}
                        </View>
                        <View style={styleReiseruteDetails.adressView}>
                            <Text>      Til: {reiserute.til_by} </Text>
                            <Entypo name="flow-line" style={styleReiseruteDetails.icon} />
                        </View>
                    </ScrollView>

                </View>
                <View style={styleReiseruteDetails.infoWrapper}>
                    <Text style={styleReiseruteDetails.infoTitle} > Beskrivelse</Text>
                    <Text style={styleReiseruteDetails.info}>{reiserute.informasjon}</Text>
                </View>

                <View style={styleReiseruteDetails.datoView}>
                    <Text style={{fontSize:15, color:"#84d4d4"}}>     Ønsket transport dato: </Text>
                    <Text style={{fontSize:15,left:-20}}>{reiserute.tidspunkt.split("T")[0]}  {reiserute.tidspunkt.split("T")[1].split(":")[0]}:{reiserute.tidspunkt.split("T")[1].split(":")[1]}
                    </Text>
                </View>


                <View style={{ flexDirection:"row"}}>
                    <View style={{ alignItems:"center"}}>
                        <TouchableOpacity
                            style={[styles.buttonStyle, {backgroundColor: "#5fd4c0",width: "70%"}]}
                            onPress={() => navigation.navigate('EditReiserute',{minReiseruteEdit:reiserute.reiserute_id})}
                        >

                            <Text style={{fontSize:15,color:"#222222"}}>
                                Rediger
                            </Text>

                        </TouchableOpacity>
                    </View>

                    {status=="gyldig"&& <View style={{ alignItems:"center"}}>
                        <TouchableOpacity
                            style={[styles.buttonStyle, {backgroundColor: "#d4b4b4",width: "70%"}]}
                            onPress={() => updateStatus("ugyldig")}
                        >

                            <Text style={{fontSize:15,color:"#222222"}}>
                                Utilgjengeliggjøre
                            </Text>

                        </TouchableOpacity>
                    </View>}
                    {status=="ugyldig"&& <View style={{ alignItems:"center"}}>
                        <TouchableOpacity
                            style={[styles.buttonStyle, {backgroundColor: "#b4e5c5",width: "70%"}]}
                            onPress={() => updateStatus("gyldig")}
                        >

                            <Text style={{fontSize:15,color:"#222222"}}>
                                Tilgjengeliggjøre
                            </Text>

                        </TouchableOpacity>
                    </View>}

                </View>
            </SafeAreaView>
        )
    }
    return(
        <RenderReiseruteDetails/>

    )
};

const styleReiseruteDetails = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,

    },
    marginHeader:{
        marginBottom:2
    },

    spinnerTextStyle: {
        color: '#FFF',
    },
    image:{
        width:"100%",
        overflow:"hidden",
        flex: 0.25,
    },
    addressContainer:{
        flex:0.29,
        overflow:"hidden"
    },
    addressWrapper:{
        flexDirection : "column",
        justifyContent: "space-between",

    },
    adressView:{
        backgroundColor:"white",
        flexDirection : "row",
        justifyContent: "space-between"
    },
    prisView:{
        backgroundColor: "white",
        flex: 0.08,
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        justifyContent: "space-between"
    },
    prisText:{
        fontSize:20,
        color: "#84d4d4",
        marginBottom:2
    },
    KR:{
        fontSize: 20,

    },
    icon:{
        fontSize:40,
        color:"#84d4d4",
        left:40
    },
    infoWrapper:{
        backgroundColor: "white",
        flex: 0.24,
    },
    info:{
        fontSize:15,
        color: "#808080",
        width:"90%",
        left:20,
        marginTop: 4,
        marginBottom:8

    },
    infoTitle:{
        fontSize:20,
        color:"#84d4d4",
        marginTop:2,
        marginBottom: 4,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        width:"50%",
        left:15
    },
    datoView:{
        flex:0.05,
        marginTop:1,
        marginBottom: 1,
        borderBottomWidth: 1,
        borderColor: "#84d4d4",
        borderStyle: "solid",
        flexDirection : "row",
        width:"100%",
        justifyContent: "space-between"
    },
});

export default MinReiseruteDetails;