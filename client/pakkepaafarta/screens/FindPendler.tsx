
import React, { useState} from 'react';
import {
    Alert,
    Button,
    Platform,
    SafeAreaView, ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    View
} from 'react-native';
import NavbarHeader from "../navigation/NavbarHeader";
import styles from '../componentes/MyStyles'
import {Reiserute, reiseruteService } from "../services/reiseruteService";
import { useNavigation} from '@react-navigation/native';
import { ListItem} from "react-native-elements";
import TouchableScale from "react-native-touchable-scale";
import MyStyles from "../componentes/MyStyles";

/**
 * @param props
 * @constructor
 * @screen FindPendler page used to show all registered routes
 * so that other users can see them and possibly ask the commute to pick up their packages
 *
 * - When item clicked on, we navigate the user to the details screen of this route (item)
 * In this class we use:
 * - ListItem from react-native-elements to render a list of routes
 * - TouchableScale from react-native-touchable-scale to give the user feedback,
 * when item focused
 */
export default function FindPendler(props: any) {
    const navigation = useNavigation();
    const [fra_by, setFra_by] = useState("");
    const [reiseruteListe, setReiseruteListe] = useState<Reiserute[]>([]);
    const [til_by, setTil_by] = useState("");
    const [showList, setShowList] = useState(false);
    const [loading, setLoading] = useState(true);
    const [loadingUpdate, setLoadingUpdate] = useState(false);


    /**
     * @this search method used to check inputs,
     * and search for routes (reiseruter) that suit them
     */
    const search = async () => {
        setShowList(false);
        // Check if inputs are empty
        if (fra_by.trim() === "" || til_by.trim() === "") {
            Alert.alert("Ett eller flere felter er tomme");
            return;
        }

        await reiseruteService.getReiseruteFraTil(fra_by,til_by).then(async res=>{
            if(!res) Alert.alert("Netverk feil", "sjekk internett tilkoblingen");

            // Network or other errors
            if (res instanceof Error) {
                Alert.alert("En feil oppsto", "sjekk internett tilkoblingen");
            }

            else if(res) {
                for(let i=0;i<res.length;i++){
                    if(res[i].status=="gyldig"){
                        reiseruteListe[i]=res[i];
                        setLoadingUpdate(true);
                    }
                }
                setLoading(false);
                setShowList(true);
            }
        })

    };

    /**
     * @this showReiserute method used when search button clicked
     */
    function showReiserute(){
        return(
            reiseruteListe.sort((a, b) => a.pris-b.pris)

                .map((item, i) => (
                    <ListItem
                        // sending oppdrag id parameter with navigate to render details screen
                        onPress={ () =>navigation.navigate('ReiseruteDetails',{ReiseruteId:item.reiserute_id})}
                        Component={TouchableScale}
                        key={i}
                        bottomDivider={true}
                        style={MyStyles.itemStyling}>
                        <ListItem.Content >
                            <ListItem.Title>{item.fra_by} {"------------>"} {item.til_by}</ListItem.Title>
                            <ListItem.Subtitle>Pris:<Text style={{color:"#5fd4c0"}}> {item.pris}</Text></ListItem.Subtitle>
                            {item.sykkel_personbil==1&&<ListItem.Subtitle>Transportmiddel:<Text style={{color:"#5fd4c0"}}> Sykkel eller personlig bil</Text></ListItem.Subtitle>}
                            {item.varebil==1&&<ListItem.Subtitle>Transportmiddel: <Text style={{color:"#5fd4c0"}}>Varebil</Text></ListItem.Subtitle>}
                            <ListItem.Subtitle>Tidspunkt:<Text style={{color:"#5fd4c0"}}>  {item.tidspunkt.split("T")[0]}  {item.tidspunkt.split("T")[1].split(":")[0]}:{item.tidspunkt.split("T")[1].split(":")[1]}</Text></ListItem.Subtitle>

                        </ListItem.Content>
                    </ListItem>
                ))
        )
    }

    return (
        <SafeAreaView style={mystyle.container}>
            <NavbarHeader />
            <ScrollView  removeClippedSubviews={true} style={{ flex: 1 }}>
                <Text
                    style={[styles.title,{
                        marginTop:50
                    }]}>
                    Pakken sendes Fra by
                </Text>

                <View style={styles.action}>
                    <TextInput
                        value={fra_by}
                        placeholder="   Eks: Trondheim, Oslo... osv"
                        style={styles.textInput}
                        onChangeText={text => setFra_by(text)}
                    />
                </View>

                <Text style={[styles.title,{
                    marginTop:20
                }]}>Pakken sendes til by:</Text>

                <View style={styles.action}>
                    <TextInput
                        value={til_by}
                        placeholder="   Eks: Trondheim, Oslo... osv"
                        style={styles.textInput}
                        textContentType={"addressCity"}
                        onChangeText={text1 => setTil_by(text1)}
                    />
                </View>
                <Text style={{color:'grey', fontSize:12, marginVertical:15 }}>* Fra by og til by kan være det samme.</Text>
                <View >
                    <Button
                        onPress={() => search()}
                        title="SØK"
                        color={"#3ecba7"}
                    />
                </View>
                {showList&&!loading
                    ?showReiserute()
                    :null
                }
            </ScrollView>
        </SafeAreaView>
    );
}

const mystyle = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        margin:5
    },

    buttonsContainer:{
        flex:0.7,
        backgroundColor: "red",
        flexDirection: "column",
        alignItems: "center"
    },
    buttonStyle:{
        width:240,
        elevation: 4,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#3ecba7",
        top:20
    },
    itemStyling:{
        paddingTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    },
    ButtonText:{
        fontSize: 22,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    textInput: {
        flex:1,
        marginTop:2,
        paddingBottom:15,
        color:'gray'
    },
    action: {
        flexDirection:'row',
        borderBottomWidth:1,
        borderBottomColor:'#f2f2f2'
    },
    linkContainer:{
        marginTop: 20,
        flexDirection: "column",
        alignItems: "center",
        alignContent:"center"

    },
    link:{
        width:240,
        top:20,

    }
});
