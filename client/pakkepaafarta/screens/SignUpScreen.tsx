
import * as React from 'react';
import {
    Alert,
    Button,
    Dimensions,
    Platform,
    SafeAreaView, StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {useState} from "react";
import PhoneInput from 'react-native-phone-number-input';
import {passwordValidation} from "../componentes/utlis";
import styles from '../componentes/MyStyles'
import NavbarHeader from "../navigation/NavbarHeader";
import { Feather} from "@expo/vector-icons";
import {brukerService} from "../services/brukerService";
import { useNavigation } from '@react-navigation/native';
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import * as FileSystem from 'expo-file-system';

// Get the height of device to make the screen more native
const screenHeight = Math.round(Dimensions.get('window').height)

/**
 *
 * @param props
 * @constructor
 * @screen SignUpScreen page used to add a new user to database
 */
export default function SignUpScreen(props: {userData?: any;}) {
    const navigation = useNavigation();
    // sign up dialog component and variables
    const [nameInput, setNameInput] = useState('');
    const [emailInput, setEmailInput] = useState("");
    const [passwordInput, setPasswordInput] = useState("");
    const [confirmPasswordInput, setConfirmPasswordInput] = useState("");
    const [formattedValue, setFormattedValue] = useState('');
    const [phoneInput,setPhoneInput] = useState('');
    const [bilde, setBilde] = useState("");
    const MaxPicture=221857;

    // Input format validations
    const emailFormat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    const tlfFormat = /^(0047|\+47|47)?[2-9]\d{7}$/;

    // Used to display error on empty input when submitting
    const [submit, setSubmit] = useState(false);

    // Show loading while waiting async API call
    const [loading, setLoading] = useState(false);

    /**
     * @this pickFromGallery used to let the user oppload a picture from gallery,
     * to use it as profile picture
     */
    const pickFromGallery=async()=>{
        const{granted} =await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if(granted){
            let data = await ImagePicker.launchImageLibraryAsync({
                mediaTypes:ImagePicker.MediaTypeOptions.Images,
                allowsEditing:true,
                aspect:[1,1],
                quality:0.1
            });
            if(!data.cancelled){
                await FileSystem.readAsStringAsync(data.uri, { encoding: 'base64' }).then(data1=>{
                    if(data1.length> MaxPicture){
                        Alert.alert("Bildet er for stort","Vennligst velg et mindre bilde..")
                    }else{
                        setBilde("data:image/png;base64,"+data1);
                        setLoading(true)
                    }
                });
            }
        }else{
            Alert.alert("Vi trenger å få tilgang til galleri for å fortsette..")
        }
    };


    /**
     * @this register attempts to add create a new user in the database
     */
     const register = async () => {

        setSubmit(true);

        if (
            nameInput.trim() === "" ||
            emailInput.trim() === "" ||
            passwordInput.trim() === "" ||
            confirmPasswordInput.trim()===""
        ){
            Alert.alert("Alle input feltene påkrevd å fylle");
            return;
        }
        if (!emailInput.trim().match(emailFormat)){
            Alert.alert("Ugyldig e-postadresse","Prøv å skrive en gyldig e-postadresse på nytt");
            return;
        }
        if (!passwordValidation(passwordInput.trim())){
            Alert.alert("Passord format!",
                "Passordet må inneholde minst 1 Stor bokstav,1 liten vokstav,et tall og et tegn f.eks(!). ");
            return;
        }
        if ( passwordInput.trim() !== confirmPasswordInput.trim()) {
            Alert.alert("Feil passord", "Vennligst sjekk at du har skrevet samme passord");
            return;
        }
        if ( phoneInput !== undefined && !phoneInput.trim().toString().match(tlfFormat)) {
            Alert.alert("Ugyldig Telefonnummer!", "Eksempel på riktig nummer er: 47185123");
            return;
        }

        setLoading(true);

        let res = await brukerService.registerBruker(nameInput, emailInput, parseInt(phoneInput), passwordInput, bilde);
        console.log(res)

        if(res instanceof Error ) {
            Alert.alert("Server error","Vennligst prøv på nytt");
            setSubmit(false);
            setLoading(false);
        }

        // Status code 409 indicates that the email is already registered
        if (res && res.status === 409  ) {
            console.log(res)
            Alert.alert("email eller telefonnummeret er allerede registrert");

            setSubmit(false);
            setLoading(false);
        }
         if ((res && res.status===500) ) {
             Alert.alert("email eller telefonnummeret er allerede registrert");
             setSubmit(false);
             setLoading(false);
         }

        if (res && res.status !== 500) {
            Alert.alert("Brukeren ble registreert");
            navigation.navigate('Login');

        }
        // Status code 401 indicates that something went wrong
        else if ((res && res.status === 401) ) {
            setSubmit(false);
            setLoading(false);
            Alert.alert("Noe feil skjedde, sjekk internett tilkoblingen");
        }
    };


    return (
        <SafeAreaView style={signStyle.container}>

            <NavbarHeader/>

            <View style={signStyle.SignUpInputForm}>

                <Text style={signStyle.title}> Ditt navn?</Text>
                <View style={signStyle.input}>
                    <TextInput
                        value={nameInput}
                        textContentType={"emailAddress"}
                        accessibilityLabel={"Navn"}
                        placeholder="   Ditt full navn.."
                        style={signStyle.textInput}
                        onChangeText={text => setNameInput(text)}
                    />
                </View>

                <Text style={signStyle.title}>Din e-post adresse</Text>

                <View style={signStyle.input}>

                    <TextInput
                        textContentType={"emailAddress"}
                        accessibilityLabel={"E-post adresse"}
                        value={emailInput}
                        placeholder="   Eks: bruker@gmail.com"
                        style={signStyle.textInput}
                        onChangeText={text => setEmailInput(text)}
                    />
                </View>

                <Text style={signStyle.title}>Ditt mobile nummer</Text>
                <PhoneInput
                    containerStyle={{backgroundColor:"white",marginTop:5}}
                    defaultValue={phoneInput}
                    defaultCode="NO"
                    onChangeText={(number) => {
                        setPhoneInput(number);
                    }}
                    onChangeFormattedText={(text) => {
                        setFormattedValue(text);
                    }}

                />

                <Text style={signStyle.title}>Passord</Text>
                <View style={signStyle.input}>
                    <TextInput
                        secureTextEntry
                        accessibilityLabel={"  Passord"}
                        placeholder="   Minimum lengde er 6.."
                        value={passwordInput}
                        style={signStyle.textInput}
                        onChangeText={text1 => setPasswordInput(text1)}
                    />
                </View>

                <Text style={signStyle.title}>Bekreft passord</Text>
                <View style={signStyle.input}>
                    <TextInput
                        secureTextEntry
                        accessibilityLabel={"Gjenta passordet"}
                        placeholder="   Gjenta passordet.."
                        value={confirmPasswordInput}
                        style={signStyle.textInput}
                        onChangeText={text1 => setConfirmPasswordInput(text1)}
                    />
                </View>
                <View style={{ paddingTop:7}}>
                    <Button title="Velg et bilde fra galleri"  onPress={()=>pickFromGallery()}>Gallery</Button>
                </View>

                <View style={signStyle.buttonsContainer}>
                    <TouchableOpacity
                        onPress={() =>  register() }
                        style={styles.buttonStyle}>
                        <Text style={styles.ButtonText}>
                            Registrer
                        </Text>
                        <Feather name="user-plus" size={24} color="white" style={{marginLeft:8}} />
                    </TouchableOpacity>
                </View>

                <View style={{alignItems:"center",marginBottom:20}}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Login')}
                        >
                        <Text
                            style={{ color:"blue", fontSize:15}} >
                            Allerede bruker? Logg inn
                        </Text>
                    </TouchableOpacity>

                </View>
            </View>
        </SafeAreaView>
    );
}


const signStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 1,
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold',
        paddingTop: screenHeight<=692 ? 4 : 13,
        paddingBottom: 10
    },
    input: {
        flexDirection:'row',
        borderBottomWidth:1,
        borderBottomColor:'#f2f2f2',

    },
    textInput: {
        borderWidth:1,
        borderColor:'gray',
        borderRadius:7,
        flex:1,
        marginTop:2,
        paddingVertical:7,
        color:'gray',
        left: 1,

    },
    SignUpInputForm:{
        flex:4,
        backgroundColor: "white"
    },
    buttonsContainer:{
        flex:1.4,
        backgroundColor: "white",
        flexDirection: "column",
        alignItems: "center"
    }
});