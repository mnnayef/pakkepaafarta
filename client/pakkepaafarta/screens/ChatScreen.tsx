
import React, { useState, useCallback, useEffect } from 'react'
import { GiftedChat, IMessage } from 'react-native-gifted-chat'
import {
    Alert,
    Text,
    Platform,
    StatusBar,
    StyleSheet,
    View,
    TouchableOpacity,
} from "react-native";
import NavbarHeader from "../navigation/NavbarHeader";
import {meldingService,Melding} from "../services/meldingService";
import Spinner from 'react-native-loading-spinner-overlay';
import * as SecureStore from "expo-secure-store";
import jwt_decode from 'jwt-decode';
import {useIsFocused ,useFocusEffect, useNavigation  } from "@react-navigation/native";
import {chatOppdragService} from "../services/chatOppdragService";
import {oppdragService} from "../services/oppdragService";
import {reiseruteService} from "../services/reiseruteService";
import styles from "../componentes/MyStyles";


/**
 *
 * @param route
 * @constructor
 * @screen ChatScreen page to allow users to communicate with each other,
 */
export default function ChatScreen({route}) {
    const navigation = useNavigation();
    const [messages, setMessages] = useState<IMessage[]>([]);
    const[chat_id, setChat_id]  = useState(parseInt(route.params.chatId)) ;
    const oppdrag_eier =  parseInt(route.params.eier);
    const [user_id, setUser_id] = useState(0);
    const [token, setToken] = useState("");
    const [loading, setLoading] = useState(true);
    const [loadingUpdate, setLoadingUpdate] = useState(true);
    const isFocused = useIsFocused();

    let interval;


    useFocusEffect(
        useCallback(() => {
            fetchMessages()
            // Do something when the screen is focused
            return () => {
                clearInterval(interval)
                setMessages([])
            };
        }, [route])
    )

    const fetchMessages = async () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        if(!authToken){
            setUser_id(0);
            return
        }
        let toki = jwt_decode(authToken)
        setToken(authToken)
        setUser_id(toki.id)
        interval= setInterval(() => {

            setLoadingUpdate(true)
            meldingService.getAllMeldingForChat(parseInt(route.params.chatId),authToken).then(data => {
                setLoadingUpdate(true)

                if( data[0] === undefined) {
                    setLoading(false)
                }

                if (data) {
                    if(data.length==0) setMessages([])
                    for(let i=0;i<data.length;i++){
                        messages[i]= {text:"", user:{_id:-1}, createdAt:-1,_id:-1};
                    }
                    for(let i=0;i<data.length;i++){
                        messages[i]._id=data[i].mld_id
                        messages[i].user._id=data[i].sender_id
                        messages[i].createdAt=data[i].sendt_at
                        messages[i].text=data[i].mld
                    }
                    setLoading(false)
                    setLoadingUpdate(false)
                }
            })
        }, 2000);  //runs every 100 m second
    }

    const navigateToDetails =  async  () => {
        await chatOppdragService.getChat(parseInt(route.params.chatId),token).then(
            data => {
                if (data[0] === undefined) {
                    return;
                }
                if (data) {
                    if (data[0] !== undefined) {
                        if(data[0].oppdr_id!==null){
                            oppdragService.getOppdrag(data[0].oppdr_id).then(
                                data => {
                                    if (data[0] === undefined) {
                                        return;
                                    }
                                    if (data) {
                                        if (data[0] !== undefined) {
                                            if(data[0].bruker_eier==jwt_decode(token).id){
                                                navigation.navigate('MineOppdragDetails',{minOppdragID:data[0].oppdrag_id})
                                            }else{
                                                navigation.navigate('OppdragDetails',{minOppdragID:data[0].oppdrag_id})
                                            }
                                            setLoading(false)
                                        }
                                    }
                                }
                            )
                        }else if(data[0].rute_id!==null){
                            reiseruteService.getReiserute(data[0].rute_id).then(
                                data => {
                                    if (data[0] === undefined) {
                                        return;
                                    }
                                    if (data) {
                                        if (data[0] !== undefined) {
                                            if(data[0].bruker_pendler==jwt_decode(token).id){
                                                navigation.navigate('MinReiseruteDetails',{minReiseruteId:data[0].reiserute_id})
                                            }else{
                                                navigation.navigate('ReiseruteDetails',{ReiseruteId:data[0].reiserute_id})
                                            }
                                            setLoading(false)
                                        }
                                    }
                                }
                            )
                        }else{
                            Alert.alert("Noe gikk galt!")
                        }

                    }
                }
            }
        )

    }



    const onSend =  async  (tex) => {
        let meldingObj:Melding = {
            mld_id: -1,
            chat_nr: parseInt(route.params.chatId),
            sender_id: tex[0].user._id,
            mld: tex[0].text,
            status:0
        }
        meldingService.addMessage(meldingObj,token).then(result => {
            if (result.affectedRows<=0) {
                Alert.alert("Netverkfeil", "Kunne ikkke sende meldingen.")
            }else{
                chatOppdragService.updateStatus(parseInt(route.params.chatId), tex[0].user._id, 0, token)
            }
        }).catch(error=>Alert.alert(error))
    }

    if(loading){
        return (
            <Spinner
                visible={loading}
                textContent={'Loading...'}
                textStyle={chatStyle.spinnerTextStyle}
            />
        )
    }


    return (
        <View style={chatStyle.container}>
            <NavbarHeader/>
            <View style={{ alignItems:"center"}}>
                <TouchableOpacity
                    style={[styles.buttonStyle, {backgroundColor: "#5fd4c0",width: "35%"}]}
                    onPress={() => navigateToDetails()}
                >

                    <Text style={{fontSize:15,color:"#222222"}}>
                        Vis detaljer
                    </Text>

                </TouchableOpacity>
            </View>

            <GiftedChat
                messages={messages}
                user={{
                    _id: user_id,
                }}
                onSend={text=>onSend(text)}
            />
        </View>


    )
}
const chatStyle = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    textInput: {
        height:30,
        borderBottomWidth:0.6,
        borderTopWidth:0.6,
        backgroundColor:"white",
        width:"85%"
    },
    spinnerTextStyle: {
        color: '#FFF',
    }
});

