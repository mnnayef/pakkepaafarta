
import axios from "axios";
import Service from "./Services";
import {Alert, AsyncStorage} from 'react-native';
import * as SecureStore from 'expo-secure-store';

export interface Bruker{
    bruker_id: number;
    navn: string,
    email: string,
    mobile: number,
    vurdering: number,
    bilde:string
}

/**
 * @serviceClass BrukerService used to connect between client and server to interact with users,
 * such as setToken when logged in, sign up user, delete account...
 */
class BrukerService extends Service{
    // Check validation to token
    token;
    read = async () => {
        try {
            this.token = await SecureStore.getItemAsync("x-auth-token");
        } catch (e) {
            console.log(e);
        }
    };

    /**
     * checkToken() used to set token to user when logged in
     */
    checkToken() {
        return axios({
            method: "post",
            url: this.path+"/login/token",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => {
                if (response.data.jwt !== undefined) {
                    SecureStore.setItemAsync('x-auth-token',response.data.jwt);
                }
                return response;
            })
            .catch(error => error.response);
    }

    /**
     * This method is used to register a new user
     * @param navn The name of the user when trying to register
     * @param email The email of the user when trying to register
     * @param mobile The mobile number of the user when trying to register
     * @param password The password of the user when trying to register
     * @param bilde The picture of the user when trying to register (option)
     * @return response based on the result of the the database
     */
    registerBruker(
        navn: string,
        email: string,
        mobile: number,
        password: string,
        bilde:any,
    ) {
        let postData = {
            navn: navn,
            email: email,
            password: password,
            mobile: mobile,
            bilde:bilde
        };
        const headers = {
            "content-Type": "application/json;charset=utf-8"
        };
        return axios
            .post(this.path+"/bruker/register",JSON.stringify(postData), {
                headers: headers
            })
            .then(response => {
                if(response){
                    if (response.status === 409 || response.status===500) {
                    } else if (response.data.jwt !== undefined) {
                        SecureStore.setItemAsync('x-auth-token',response.data.jwt);
                    }
                    return response;
                }else {
                }
            })
            .catch(error => error.response);
    };

    /**
     * This method used to log in the user
     * @param email
     * @param password The password when the user trying to log in "not used directly, but used to compare hash and salt of it"
     * @return response based on the result of the the database
     */
    login(email: string, password: string) {
        const headers = {
            "Content-Type": "application/json; charset=utf-8"
        };
        var postData = {
            email: email,
            password: password
        };
        return axios
            .post(this.path+"/bruker/login", postData, {
                headers: headers
            })
            .then(async response => {
                if (response.status === 204) {
                    Alert.alert("Du har ikke tilgang","Er du sikker på at brukeren finnes")
                }
                if (response.data.jwt !== undefined) {
                    SecureStore.setItemAsync('x-auth-token',response.data.jwt);
                }
                return response;
            })
            .catch(error => error);
    }

    /**
     * getUserAllInfoByEMail() used to get a singular user given his email,if exist
     * Example where the method is used is (forgotten password screen)
     * @param email The mail of the interact user
     * @return response based on the result of the the database
     */
    getUserAllInfoByEMail(email: string) {

        return axios({
            method: "get",
            url: this.path + "/bruker/email/info/" + email,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => console.log(error));
    }

    /**
     * getUserAllInfoById() used to get a singular user given his id
     * Example where the method is used is (profile, Edit profile)
     * @param id The id of the user in the database
     * @return response based on the result of the the database
     */
    getUserAllInfoById(id: number) {
        return axios({
            method: "get",
            url: this.path + "/bruker/" +id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": this.token
            }
        })
            .then(response => response.data)
            .catch(error => console.log(error));
    }

    /**
     * getUserInfoById() used to get a singular user given his id, without unneccessary data
     * Example where the method is used is (mine oppdrag, reiserute screen)
     * @param id The id of the user in the database
     * @return response based on the result of the the database
     */
    getUserInfoById(id: number) {
        return axios({
            method: "get",
            url: this.path + "/bruker/no-image/" +id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": this.token
            }
        })
            .then(response => response.data)
            .catch(error => console.log(error));
    }

    /**
     * setNewPassword() used to set a new password, if mail exists in the database
     * @param email The email is used, for example, to send a generated random password to user
     * @param password The new password, can be generated password in forget password case
     * @return response based on the result from the database
     */
    setNewPassword(email:string, password:string ) {
        return axios({
            method: "put",
            url: this.path + "/bruker/reset_password/" + email,
            data: {password},
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            }
        })
            .then(response => {
                if (response.status === 409) {
                    return response;
                }
                return response;
            })
            .catch(error => console.log(error));
    }

    /**
     * updateUser() used to allow the user to edit his info after signing up
     * @param email
     * @param phoneNumber
     * @param bruker_id
     * @param bilde
     * @return response based on the result from the database
     */
    updateUser(email:string, phoneNumber:number ,bruker_id:number, bilde) {
        return axios({
            method: "put",
            url: this.path + "/bruker/" + bruker_id,
            data: {email:email,mobile: phoneNumber, bilde: bilde},
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": this.token
            }
        })
            .then(response => {
                if (response.status === 409) {
                    return response;
                }
                return response;
            })
            .catch(error => console.log(error));
    }
}

export let brukerService = new BrukerService();