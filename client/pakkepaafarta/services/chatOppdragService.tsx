
import axios from "axios"
import Service from "./Services";


/**
 * @interface ChatOppdrag used to declare props of the Chat-object and their datatype
 */
export interface ChatOppdrag {
    chat_id: number;
    oppdr_id: number;
    bruker_1: number;
    bruker_2: number;
    status: number;
    bruker_updated_status:number;
}

/**
 * @serviceClass ChatOppdragService used to connect between client and server to interact with chats.
 * This class is used to handle all functions when a user want discuss a commission (oppdrag) with the commission-owner
 * such as add a new chat room between two users, list all chats to a singular user
 */
class ChatOppdragService extends Service {
    /**
     * addChat creates a new chat between two users in database
     * @param chat The chat object to be sent to the database
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    addChat(chat: ChatOppdrag, token: string){
        return axios({
            method: "post",
            url: this.path + "/chat/",
            data: chat,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(err => err)
    }

    /**
     * getChatByUserId() gets a list of chat to the interact user in the datbase
     * @param bruker_id Identifies the user of interest in database
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    getChatByUserId(bruker_id:number,  token:string){
        return axios({
            method: "get",
            url: this.path + "/chat/"+bruker_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getChat() gets a singular chat, used to get all messages related of the chat (chatScreen)
     * @param chatId Identifies the chat of interest in database
     * @param token Used to verify if the user is authenticated to get access to the database
     */
    getChat(chatId:number,  token:string){
        return axios({
            method: "get",
            url: this.path + "/chat/samtale/"+chatId,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getSpecificChat() used to check if message already exists when user presses send message,
     * related to a specific commission/route.
     * @param oppdrag_id Identifies the commission of interest in database
     * @param reiserute_id Identifies the route of interest in database
     * @param bruker_id1 Identifies the 1.user of interest in database
     * @param bruker_id2 Identifies the 2.user of interest in database
     * @param token
     */
    getSpecificChat(oppdrag_id:number, reiserute_id:number,bruker_id1: number, bruker_id2: number, token: string){
        return axios({
            method: "get",
            url: this.path + "/chat/specific/"+oppdrag_id+"/"+reiserute_id+"/"+bruker_id1+"/"+bruker_id2,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response =>response.data)
            .catch(error => error);

    }

    /**
     * maxChatId() gets maxId to navigate to it when it has just been made
     * @param token
     */
    maxChatId(token:string){
        return axios({
            method: "get",
            url: this.path + "/maxchatId",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getUnreadMessages() used to set a notify the user if there are unread messages
     * @param bruker_id Identifies the user of interest in database
     * @param token
     */
    getUnreadMessages(bruker_id:string, token:string){
        return axios({
            method: "get",
            url: this.path + "/chat/unread/messages/"+bruker_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * updateStatus() used to change status of unread messages when the user open the chat
     * @param chat_id Identifies the chat of interest in database
     * @param bruker Identifies the user of interest in database
     * @param status
     * @param token
     */
    updateStatus(chat_id:number, bruker:number,status:number, token){
        return axios({
            method: "put",
            url: this.path + "/chat/"+chat_id+"/"+bruker+"/"+status,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }
}


export let chatOppdragService = new ChatOppdragService();