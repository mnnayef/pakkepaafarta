
import axios from "axios";
import Service from "./Services";
import * as SecureStore from "expo-secure-store";


export interface  Oppdrag{
    oppdrag_id: number;
    fra_by: string;
    til_by: string;
    tidspunkt: string;
    bruker_eier: number;
    varebil: number;
    sykkel_personbil: number;
    beskrivelse: string;
    fra_adresse: string;
    til_adresse: string;
    pris: number;
    bilde: string;
}

/**
 * @serviceClass OppdragService used to interact with commission(oppdrag),
 * such get,add,update, delete...
 */
class OppdragService extends Service{
    token;
    /**
     * @this read used to get the key of the token for the user
     */
    read = async () => {
        try {
            this.token = await SecureStore.getItemAsync("x-auth-token");
        } catch (e) {
            console.log(e);
        }
    };

    /**
     * addOppdrag() used to add a new commission to the database
     * @param oppdrag The commission object to be sent to the database
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    addOppdrag(oppdrag:Oppdrag, token:string) {
        return axios({
            method: "post",
            url: this.path + "/oppdrag/",
            data: oppdrag,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    };

    /**
     * getOppdrag used to get a singular commission given its id
     * @param oppdrag_id Identifies the commission of interest in database
     */
    getOppdrag(oppdrag_id:number){
        return axios({
            method: "get",
            url: this.path + "/oppdrag/"+oppdrag_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync("x-auth-token")
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getOppdragForUser() used to get a list of commissions to the user
     * @param bruker_eier Identifies the user of interest in database
     * @return response based on the result from the database
     */
    getOppdragForUser(bruker_eier:number){
        return axios({
            method: "get",
            url: this.path + "/oppdrag/bruker/"+bruker_eier,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync("x-auth-token")
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getAllOppdragWithoutPic() used to get all commissions from db - ,
     * to list them on map based on their address
     */
    getAllOppdragWithoutPic(){
        return axios({
            method: "get",
            url: this.path + "/oppdrag/oppdragOnMap",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * updateOppdrag() used to update an existing commission in db
     * @param oppdrag Identifies the commission-object of interest to be updated in database
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    updateOppdrag(oppdrag:Oppdrag, token:string){
        return axios({
            method: "put",
            url: this.path + "/oppdrag/update/"+oppdrag.oppdrag_id,
            data:oppdrag,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * updateStatus() used to set a commission as unavailable in case
     * the owner has agreed with someone to pick it up
     * @param oppdrag_id Identifies the commission of interest in database
     * @param status to be sett as unavailable
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    updateStatus(oppdrag_id:number, status:string, token:string){
        return axios({
            method: "put",
            url: this.path + "/oppdrag/update/"+oppdrag_id+"/"+status,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * deleteOppdrag() used to allow the user to delete an existing commission from db
     * @param oppdrag_id Identifies the commission of interest in database
     * @return response based on the result from the database
     */
    deleteOppdrag(oppdrag_id:number){
        return axios({
            method: "delete",
            url: this.path + "/oppdrag/"+oppdrag_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": this.token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

}
export let oppdragService = new OppdragService();