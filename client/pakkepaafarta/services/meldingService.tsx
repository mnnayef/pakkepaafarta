
import axios from "axios"
import Service from "./Services";

export interface Melding  {
    mld_id: number;
    chat_nr: number;
    sender_id: number;
    mld: string;
    status:number;
}

/**
 * @serviceClass MeldingService used to interact with messages(melding) between users
 */
class MeldingService extends Service {
    /**
     *
     * @param melding The message-object to be sent to the database
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    addMessage(melding: Melding, token: string){
        return axios({
            method: "post",
            url: this.path + "/melding/",
            data: melding,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(err => err)
    }

    // Gets all messages given chat id
    /**
     * getAllMeldingForChat() used to get all messages given chat id
     * @param chat_nr Identifies the chat of interest in database
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    getAllMeldingForChat(chat_nr:number, token: string){
        return axios({
            method: "get",
            url: this.path + "/melding/"+chat_nr,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data )
            .catch(error => error);
    }

    /**
     *
     * @param melding_id Identifies the message of interest in database
     * @param status used to update message status (read, unread)
     * @param token Used to verify if the user is authenticated to get access to the database
     */
    updateStatus(melding_id:number,status:number, token){
        return axios({
            method: "put",
            url: this.path + "/melding/"+melding_id+"/"+status,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }
}


export let meldingService = new MeldingService();