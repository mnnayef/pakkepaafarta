
import axios from "axios";
import Service from "./Services";
import * as SecureStore from 'expo-secure-store';


export interface Reiserute{
    reiserute_id: number;
    tidspunkt: string;
    fra_by: string;
    til_by: string;
    bruker_pendler: number;
    varebil: number;
    sykkel_personbil: number;
    pris: number;
    informasjon: string;
    stopp_by1 : string;
    stopp_by2 : string;
    stopp_by3 : string
}

/**
 * @serviceClass ReiseruteService used to interact with route(reiserute),
 * such get,add,update, delete...
*/
class ReiseruteService extends Service{

    /**
     * addReiserute() used to add a new route to the database
     * @param reiserute The route object to be sent to the database
     * @param toki Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    addReiserute(reiserute:Reiserute, toki:string) {
        return axios({
            method: "post",
            url: this.path + "/reiserute/",
            data: reiserute,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": toki
            }
        })
            .then(response => response.data)
            .catch(error => error);
    };

    /**
     * getReiserute used to get a singular route given its id
     * @param reiserute_id Identifies the route of interest in database
     */
    getReiserute(reiserute_id:number){
        return axios({
            method: "get",
            url: this.path + "/reiserute/"+reiserute_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getReiseruteForUser() used to get a list of route to the user from the database
     * @param bruker_pendler Identifies the user of interest in database
     * @return response based on the result from the database
     */
    getReiseruteForUser(bruker_pendler:number){
        return axios({
            method: "get",
            url: this.path + "/bruker/reiserute/"+bruker_pendler,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getReiseruteFraTil() used to list all routes when users search about route given from and to address
     * @param fra the pick up address
     * @param til the destination
     * from and to addresses can be the stop points on the way to the destination
     */
    getReiseruteFraTil(fra:string, til:string){
        return axios({
            method: "get",
            url: this.path + "/reiserute/"+fra+"/"+til,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
            .then(response =>response.data)
            .catch(error => error);
    }

    /**
     * updateReiserute() used to update an existing route in db
     * @param reiserute Identifies the route-object of interest to be updated in database
     * @param toki Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    updateReiserute(reiserute:Reiserute , toki:string){
        return axios({
            method: "put",
            url: this.path + "/reiserute/update/",
            data:reiserute,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": toki
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * updateStatus() used to set a route as unavailable/available in case
     * the user does not commute / want to take a break
     * @param oppdrag_id Identifies the commission of interest in database
     * @param status to be sett as unavailable
     * @param token Used to verify if the user is authenticated to get access to the database
     * @return response based on the result from the database
     */
    updateStatus(reiserute_id:number, status:string , toki:string){
            return axios({
                method: "put",
                url: this.path + "/reiserute/update/"+reiserute_id+"/"+status,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "x-auth-token": toki
                }
            })
            .then(response => response.data)
            .catch(error => error);

    }

    /**
     * deleteReiserute() used to allow the user to delete an existing route from db
     * @param reiserute_id Identifies the route of interest in database
     * @return response based on the result from the database
     */
    deleteReiserute(reiserute_id:number){
        return axios({
            method: "delete",
            url: this.path + "/reiserute/"+reiserute_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

}
export let reiseruteService = new ReiseruteService();