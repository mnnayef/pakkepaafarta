
/**
 *  @servie Service used to set the main path of our application
 * The path varies, and is is the IP address of the connected network running the app
 */
export default class Service{
    path = "http://192.168.0.166:9000/v0";
}
