
import Service from "./Services";
import axios from "axios";


/**
 * @serviceClass EmailService used to interact with send mail to users,
 * for example send a generated password to the user
 */
class EmailService extends Service {
    /**
     *
     * @param email The e-mail address of the recipient
     * @param message The message contains the generated password
     */
    sendEmail(email: string, message: string) {
        const headers = {
            "Content-Type": "application/json; charset=utf-8",
        };
        var postData = {
            email: email,
            message: message
        };

        return axios
            .post(this.path + "/send_email/", postData, {
                headers: headers
            })
            .then(response => {
                return response;
            })
            .catch(error => console.log(error));
    }

    /**
     * generatePassword() used to create a random generated password
     */
    generatePassword() {
        let password = '';
        let upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let lowerChars = "abcdefghijklmnopqrstuvwxyz";


        password += lowerChars.charAt(Math.floor(Math.random() * lowerChars.length))
        password += Math.floor(1000 + Math.random() * 9000);
        password += upperChars.charAt(Math.floor(Math.random() * upperChars.length))

        return password;  // Length 6 with lowercase, 4 digits number and Uppercase eks: p1830H
    }

}
export let emailService = new EmailService();
