

import axios from "axios"
import Service from "./Services";
/**
 * @class ChatRute used to declare props of the Chat-object and their datatype
 */
export interface ChatRute {
    rute_id: number;
    bruker_1: number;
    bruker_2: number;
    status:number;
    bruker_updated_status:number;
}

/**
 * @serviceClass ChatRuteService used to add a chat related to routes (reiserute)
 * All another needed functions as get chat for user, get are used from class ChatOppdragService
 */
class ChatRuteService extends Service {
    addChat(chatReiserute: ChatRute, token: string){
        return axios({
            method: "post",
            url: this.path + "/chat/",
            data: chatReiserute,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": token
            }
        })
            .then(response => response.data)
            .catch(err => err)
    }
}


export let chatReiseruteService = new ChatRuteService();