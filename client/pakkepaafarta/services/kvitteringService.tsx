
import axios from "axios";
import Service from "./Services";
import * as SecureStore from "expo-secure-store";


interface Kvittering{
    kvittering_id: number;
    bruker_pendler: number;
    bruker_eier: number;
    pris: number;
    oppdrag_id: number;
    reiserute_id: number;
    tidspunkt: string;
}

/**
 * @serviceClass KvitteringService is used to interact with receipts (kvittering)
 * receipts is meant to be sent when two users agree on a commission (Oppdrag)
 * Note: The class is not in use but we have decided to implement it for further development
 */
class KvitteringService extends Service{
    /**
     * addKvittering used to add receipt in database
     * @param kvittering The receipt-object it self to be send with the request
     * @return response based on the result from the database
     */
    addKvittering(kvittering:Kvittering) {
        return axios({
            method: "post",
            url: this.path + "/kvittering/",
            data: kvittering,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    };

    /**
     * getKvittering() can be used to get a singular receipt
     * @param kvittering_id Identifies the receipt of interest in database
     */
    getKvittering(kvittering_id:number){
        return axios({
            method: "get",
            url: this.path + "/kvittering/"+kvittering_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getKvitteringerBruker() can be used to list the all agreements that the user has
     * @param bruker_id Identifies the user of interest in database
     * @return response based on the result from the database
     */
    getKvitteringerBruker(bruker_id:number){
        return axios({
            method: "get",
            url: this.path + "/kvittering/bruker/"+bruker_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * getKvitteringPendlerEier() can be used to get a singular agreement between two users
     * @param bruker_pendler Identifies the user who picks up the package
     * @param bruker_eier Identifies the owner of the package(commission) in database
     */
    getKvitteringPendlerEier(bruker_pendler:number, bruker_eier:number){
        return axios({
            method: "get",
            url: this.path + "/kvittering/"+bruker_eier+"/"+bruker_pendler,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * updateKvittering() can be used to allow the users to update the receipt in case the agreement has been changed
     * @param kvittering The receipt-object to be updated
     */
    updateKvittering(kvittering:Kvittering){
        return axios({
            method: "put",
            url: this.path + "/kvittering/"+kvittering.kvittering_id,
            data:kvittering,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

    /**
     * deleteKvittering() can be used allow the user to delete a singular receipt
     * @param kvittering_id Identifies the receipt of interest in database
     * @return response based on the result from the database
     */
    deleteKvittering(kvittering_id:number){
        return axios({
            method: "delete",
            url: this.path + "/kvittering/"+kvittering_id,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "x-auth-token": SecureStore.getItemAsync('x-auth-token')
            }
        })
            .then(response => response.data)
            .catch(error => error);
    }

}
export let kvitteringService = new KvitteringService();