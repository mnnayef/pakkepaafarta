
import {
    StyleSheet,
    View,
    Text,
    SafeAreaView,
    Platform,
    StatusBar,
} from "react-native";
import React from "react";
import NavbarHeader from "../navigation/NavbarHeader";

/**
 *
 * @constructor
 * @component DataNotFound is used to render a simple view,
 * to tell the user there are no data to render it,if that is the case
 */
const DataNotFound =() => {
    return (
        <SafeAreaView style={dataStyle.container}>
            <NavbarHeader/>
            <View style={dataStyle.tittle}>
                <Text style={dataStyle.textStyle}>
                    Vi fant ingen data i systemet vårt
                </Text>
            </View>
        </SafeAreaView>
    );
}

const dataStyle = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,

    },
    tittle:{
        flex: 2,
        fontWeight:"bold",
        alignItems:"center",
        justifyContent:"center"
    },
    textStyle:{
        paddingVertical:5,
        textAlign:'center',
        fontWeight:"bold",
        borderWidth:1 ,
        borderColor:'black',
        width:"60%",
        borderRadius:3,
        color:'#027e4a',
        fontSize:25
    }
});

export default DataNotFound;