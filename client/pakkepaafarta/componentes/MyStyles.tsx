
import {Platform, StatusBar, StyleSheet} from 'react-native';

export default StyleSheet.create({
    // Style used in all screen
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 1
    },
    itemStyling:{
        paddingTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold',
        paddingTop: 13,
        paddingBottom: 10,
        left: 5
    },
    action: {
        flexDirection:'row',
        borderBottomWidth:1,
        borderBottomColor:'#f2f2f2',
        left:2
    },
    textInput: {
        borderWidth:1,
        borderColor:'gray',
        borderRadius:7,
        flex:1,
        marginTop:2,
        paddingVertical:7,
        color:'gray'
    },
    SignUpInputForm:{
        flex:4,
        backgroundColor: "white"
    },
    // Specific Style to Welcome Screen
    profileImageView:{
        flex:0.45,
        backgroundColor:"white",
        alignItems:"center",
        paddingTop: 4
    },
    tittlePakkePaaFarta:{
        paddingTop:5,
        bottom:-13
    },
    textView:{
        alignItems: "center"
    },
    imageView:{
        flex:2,
    },
    buttonsContainer:{
        flex:1.4,
        backgroundColor: "white",
        flexDirection: "column",
        alignItems: "center"
    },
    buttonStyle:{
        width:240,
        elevation: 4,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#3ecba7",
        top:20,
        alignItems: 'center',
        flexDirection : "row",
        justifyContent: "center"

    },
    ButtonText:{
        fontSize: 22,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    },
    LoginButton:{
        width:180,
        elevation: 4,
        borderRadius: 14,
        paddingVertical: 10,
        backgroundColor:"#68b5a0",
        top:20,
        alignItems: 'center',
        flexDirection : "row",
        justifyContent: "center"

    }
});
