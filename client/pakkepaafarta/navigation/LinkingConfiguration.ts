import * as Linking from 'expo-linking';


/**
 * @function used for linking configuration between roots, screens and tabs
 */
export default {
    prefixes: [Linking.makeUrl('/')],
    config: {
        screens: {
            Root: {
                screens: {
                    TabOne: {
                        screens: {
                            TabOneScreen: 'one',
                        },
                    },
                    TabTwo: {
                        screens: {
                            TabTwoScreen: 'two',
                        },
                    },
                },
            },
            NotFound: '*',
        },
    },
};
