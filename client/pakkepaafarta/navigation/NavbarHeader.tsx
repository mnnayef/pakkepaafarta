
import React, {useEffect, useState} from 'react';
import { StyleSheet, View, Image, TouchableOpacity} from "react-native";
import { Entypo } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import * as SecureStore from "expo-secure-store";
import jwt_decode from 'jwt-decode';

/**
 *
 * @constructor
 * @Component NavbarHeader is the top navigation header, and it is used in all screens
 * NavbarHeader navigate the user easily to home,profile,login and menu screens
 */
const NavbarHeader =() =>  {
    const navigation = useNavigation();
    /**
     * @this checkLoggin used to navigate user to profile/login screen,
     * depend if he is logged in or not
     */
    const checkLoggin = async () => {
        const authToken = await SecureStore.getItemAsync('x-auth-token');
        // User not logged in
        if(!authToken){
            navigation.navigate("Login")
            return
        }
        navigation.navigate("Profile")
    }

    return (
        <View  style={styles.navbarView}>

            <TouchableOpacity
                onPress={() => navigation.navigate('Home')}
            >
                <Image style={{left:13}} source={require("../assets/images/logo2.png")}/>
            </TouchableOpacity>


            <FontAwesome
                onPress={() => checkLoggin()}
                style={{right:-70, paddingTop:6}} name="user-circle" size={32} color="black"
            />

            <Entypo
                onPress={() => navigation.navigate("Menu")}
                style={{left:-13}} name="menu" size={45} color="black"
            />

        </View>

    );
}

const styles = StyleSheet.create({
    navbarView:{
        paddingBottom:2,
        borderStyle:'dashed',
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomColor: "black",
        borderBottomWidth: StyleSheet.hairlineWidth
    }
});

export default NavbarHeader;