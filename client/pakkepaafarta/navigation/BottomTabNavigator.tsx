
import { Entypo } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constans/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {BottomTabParamList, TabLoginParamList} from '../types';
import LogInScreen from "../screens/LoginScreen";
import SignUpScreen from "../screens/SignUpScreen";


const BottomTab = createBottomTabNavigator<BottomTabParamList>();
const Tab = createBottomTabNavigator();
/**
 * @function BottomTabNavigator is the bottom navigator of our app,
 * we do not use it anymore after implementing the topNavigator (NavBarHeader)
 */
export default function BottomTabNavigator() {
    const colorScheme = useColorScheme();

    return (
        <BottomTab.Navigator
            initialRouteName="Login"
            tabBarOptions={{ activeTintColor: Colors[colorScheme].tint } }>
            <BottomTab.Screen
                name="Login"
                component={TabLogIn}
                options={{
                    tabBarIcon: ({ color }) => <TabBarIcon name="login" color={'black'} />,
                }}
            />
            <BottomTab.Screen
                name="SignUp"
                component={SignUpScreen}
                options={{
                    tabBarIcon: ({ color }) => <AntDesign name="adduser" size={30} color="black" />,
                }}
            />
        </BottomTab.Navigator>
    );
}

function TabBarIcon(props: { name: string; color: string }) {
    return <Entypo size={30} style={{ marginBottom: -3 }} {...props} />;
}

const TabLogInStack = createStackNavigator<TabLoginParamList>();

function TabLogIn() {
    return (
        <TabLogInStack.Navigator>
            <TabLogInStack.Screen
                name="LoginScreen"
                component={LogInScreen}
                options={{ headerTitle: 'Login to pendler' }}
            />
        </TabLogInStack.Navigator>
    );
}
