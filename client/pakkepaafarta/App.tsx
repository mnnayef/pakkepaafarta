


// Libraries
import React from 'react';
import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import {NavigationContainer} from '@react-navigation/native'

/**
 * All used screens and components in our application
 */
import WelcomeScreen from "./screens/WelcomeScreen";
import SignUpScreen from "./screens/SignUpScreen";
import LogInScreen from "./screens/LoginScreen";
import AddReiseRute from "./screens/AddReiseRute";
import AddOppdrag from "./screens/AddOppdrag";
import MyProfile from "./screens/MyProfile";
import {createDrawerNavigator} from "@react-navigation/drawer";
import EditProfile from "./screens/EditProfile";
import ForgotPassword from "./screens/ForgotPassword";
import FindOppdrag from "./screens/FindOppdrag";
import FindPendler from "./screens/FindPendler";
import MineOppdrag from "./screens/MineOppdrag";
import MineReiseruter from "./screens/MineReiseruter";
import OppdragDetails from "./screens/OppdragDetails";
import MinReiseruteDetails from "./screens/MinReiseruteDetails";
import MineOppdragDetails from "./screens/MineOpdragDetails";
import EditOppdrag from "./screens/EditOppdrag";
import EditReiserute from "./screens/EditReiserute";
import ReiseruteDetails from "./screens/ReiseruteDetails";
import MenuScreen from "./screens/MenuScreen";
import ChatScreen from "./screens/ChatScreen";
import ChatReiserute from "./screens/ChatReiserute";
import ChatList from "./screens/ChatList";

const Drawer = createDrawerNavigator();

/**
 *
 * @constructor
 * @Component App is the main rote in client-side of our application
 * The initialRoute of the application is WelcomeScreen
 * This file contains all our route, Screen that we render in the app
 */
export default function App() {
    const isLoadingComplete = useCachedResources();
    const colorScheme = useColorScheme();

    if (!isLoadingComplete) {
        return null;

    }
    else {
        return (

            <NavigationContainer >

              <Drawer.Navigator
                  initialRouteName="Home"
                  drawerStyle={{backgroundColor: "white"}}
                  drawerContentOptions={{activeBackgroundColor:"#3ecba7",activeTintColor:"black"}}

              >
                <Drawer.Screen name="Home" component={WelcomeScreen}   />
                <Drawer.Screen name="Login" component={LogInScreen} />
                <Drawer.Screen name="Registrer" component={SignUpScreen} />
                <Drawer.Screen name="AddTripRoute" component={AddReiseRute} />
                <Drawer.Screen name="Profile" component={MyProfile} />
                <Drawer.Screen name="AddCommission" component={AddOppdrag} />
                <Drawer.Screen name="EditProfile" component={EditProfile} />
                <Drawer.Screen name="ForgetPassword" component={ForgotPassword} />
                <Drawer.Screen name="MineOppdrag" component={MineOppdrag} />
                <Drawer.Screen name="MineReiseruter" component={MineReiseruter} />
                <Drawer.Screen name="Chat" component={ChatScreen} />
                <Drawer.Screen name="ChatReiserute" component={ChatReiserute} />
                <Drawer.Screen name="ChatList" component={ChatList} />
                <Drawer.Screen name="Menu" component={MenuScreen} />
                <Drawer.Screen name="EditOppdrag" component={EditOppdrag} />
                <Drawer.Screen name="EditReiserute" component={EditReiserute} />
                <Drawer.Screen name="OppdragDetails" component={OppdragDetails} />
                <Drawer.Screen name="MineOppdragDetails" component={MineOppdragDetails} />
                <Drawer.Screen name="MinReiseruteDetails" component={MinReiseruteDetails} />
                <Drawer.Screen name="ReiseruteDetails" component={ReiseruteDetails} />
                <Drawer.Screen name="FindOppdrag" component={FindOppdrag} />
                <Drawer.Screen name="FindPendler" component={FindPendler} />
              </Drawer.Navigator>
            </NavigationContainer>
    );
  }
}


