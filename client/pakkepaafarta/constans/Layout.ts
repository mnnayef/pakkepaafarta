/**
 * This file used to implement native app,
 * by checking device width and height
 */
import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
/**
 * @values  This file used to implement native app,
 * by checking device width and height
 */
export default {
    window: {
        width,
        height,
    },
    isSmallDevice: width < 375,
};
